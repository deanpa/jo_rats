#!/usr/bin/env python

import os
import numpy as np
import pickle
import preProcessing
import mcmcFX


######################
# Main function
def main(params):
    print('########################')
    print('########################')
    print('Initial run: ', params.firstRun)
    print('Use checked data: ', params.useCheckedData)
    print('Model ID: ', params.modelID)

    print('ngibbs', params.ngibbs)
    print('Thin rate', params.thinrate)
    print('Burnin', params.burnin)      

    print('Total iterations', params.totalIterations)
    print('Checkpoint interval', params.interval)
    print('########################')
    print('########################')


    ## if first mcmc run, initiate parameters; else read in basicdata
    if params.firstRun:
        ## create an instance of the basicdata class in initiate model.
        basicdata = preProcessing.BasicData(params)        
        print('########### FINISHED PREPROCESSING')
    else:
        ## unpickle basicdata results from previous mcmc run.
        fileobj = open(params.basicdataFname, 'rb')
        basicdata = pickle.load(fileobj)
        fileobj.close()

    ## RUN MCMC        
    if params.useCheckedData:
        fileobj = open(params.outCheckingFname, 'rb')
        checkingdata = pickle.load(fileobj)
        fileobj.close()
        print('outCheckingFname: ', params.outCheckingFname)
        mcmcobj = mcmcFX.MCMC(params, basicdata, checkingdata)
    else:
        mcmcobj = mcmcFX.MCMC(params, basicdata)



    # make Class of mcmc results and pickle to directory
    gibbsresults = preProcessing.GibbsResults(params, basicdata, mcmcobj)


    ## pickle basic data from present run to be used to initiate new runs
    fileobj = open(params.basicdataFname, 'wb')
    pickle.dump(basicdata, fileobj)
    fileobj.close()

    ## pickle mcmc results for post processing in postProcessing.py
    fileobj = open(params.gibbsFname, 'wb')
    pickle.dump(gibbsresults, fileobj)
    fileobj.close()




    # Save a checkpoint, but not if this is the first or the last step
    # include simplified MCMC data
    if (( (mcmcobj.g % params.checkpointfreq == 0) or (mcmcobj.g == (mcmcobj.stoppingStep - 1))) 
            and (mcmcobj.g != mcmcobj.startingStep) and (mcmcobj.g != (mcmcobj.maxsteps - 1)) ):
        mcmcobj.startingStep = mcmcobj.g + 1
        print('writeChecking = True')

        # Write the checking data to a separate PKL
        checkingdata = preProcessing.CheckingData(mcmcobj)
        fileobj = open(params.outCheckingFname, 'wb')
        pickle.dump(checkingdata, fileobj)      
        fileobj.close()




if __name__ == '__main__':
    main()


