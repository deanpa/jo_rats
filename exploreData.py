#!/usr/bin/env python

import os
import pickle
import numpy as np
import datetime
from scipy import stats
#from numba import njit
import pylab as P



def decodeBytes(byteArray, nArray):
    """
    ## loop to decode byte to string
    """
    strArray = str(byteArray[0], 'utf-8')
    for i in range(1, nArray):
        strArr_i = str(byteArray[i], 'utf-8')
        strArray = np.append(strArray, strArr_i)
    return(strArray)


### Define global functions: ###
def logit(x):
    return np.log(x) - np.log(1 - x)


def inv_logit(x):
    return np.exp(x) / (1 + np.exp(x))

def getWeek(inArray):
    n = len(inArray)
    outArray = np.empty(n, dtype = int)
    for i in range(n):
        date_i = inArray[i]
        outArray[i] = date_i.isocalendar()[1]
#        print('date', date_i, 'wk', outArray[i])    
    return(outArray)


def distFX(x1, y1, x2, y2):
    """
    calc distance matrix between 2 sets of points
    resulting matrix dimensions = (len(x1), len(x2))
    """
    deltaX = np.add.outer(x1, -x2)
    deltaX_sq = deltaX**2
    deltaY = np.add.outer(y1, -y2)
    deltaY_sq = deltaY**2
    dist_2D = np.sqrt(deltaX_sq + deltaY_sq)
    return dist_2D


def formatDate(inArray):
    n = len(inArray)
    outArray = np.empty(n, dtype = datetime.date)
    mondayArray = np.empty(n, dtype = datetime.date)
    for i in range(n):
        date_i = inArray[i]
        if date_i != '':
            di = datetime.datetime.strptime(date_i, '%d/%m/%Y').date()
            outArray[i] = di
            mondayArray[i] = (di - datetime.timedelta(days=di.weekday()))

###            outArray[i] = datetime.datetime.strptime(date_i, '%d/%m/%y').date()
        else:
            outArray[i] = datetime.date(1999, 1, 1).date()
#        print('date', di, 'monday', mondayArray[i])
    return(outArray, mondayArray)

# Function to find the closest date
def find_closest_date(target_date, date_array):
    closest_date = min(date_array, key=lambda x: abs(x - target_date))
    return closest_date


class Params(object):
    def __init__(self):
        # set paths to scripts and data
        self.inputDataPath = os.path.join(os.getenv('ALABASTORPROJDIR', default = '.'), 
            'Data')
        self.outputDataPath = os.path.join(os.getenv('ALABASTORPROJDIR', default = '.'), 
            'Results/Detection')
        self.inputDensityFname = os.path.join(self.inputDataPath, '20220215_densities.csv')
#        self.inputSnapFname = os.path.join(self.inputDataPath, 'SnapTrapData.csv')
        self.inputTTFname = os.path.join(self.inputDataPath, 'TT_Data2.csv')
        self.inputDensityTTFname = os.path.join(self.inputDataPath, 'denTest.csv')
#        self.inputCameraFname = os.path.join(self.inputDataPath, 'Camera3Night.csv')
#        self.inputEnvironFname = os.path.join(self.inputDataPath, 'environVarDevices.csv')

        self.ttRateDensityFname =  os.path.join(self.outputDataPath,'TTRateDensity.png')
        self.ttRateLineFname =  os.path.join(self.outputDataPath,'TTRate_Line.png')
        self.pDetectFname =  os.path.join(self.outputDataPath,'pDetect.png')
        self.pDetect0Fname =  os.path.join(self.outputDataPath,'pDetect0.png')
        self.pDetectLgSigmaFname =  os.path.join(self.outputDataPath,'pDetectLgSigma.png')
        self.denSigmaFname =  os.path.join(self.outputDataPath,'denSigma.png')
        self.simTTFname = os.path.join(self.outputDataPath,'simTT.png')





class Explore(object):
    def __init__(self, params):
        self.params = params   

        ## RUN FUNCTIONS
        self.readDensity()
        self.readTrackingTunnel()
#        self.getLocationData()
        self.readTT_Density()
        self.getTrackingRate()
        self.plotTT()
        self.plotG0()
        self.plotG0_small()
        self.plotDenSigma()
        self.simTTData()


    def readDensity(self):
        """
        ## READ IN DENSITY DATA
        """
        densityDat = np.genfromtxt(self.params.inputDensityFname,  delimiter=',', 
            names=True, dtype=['S10', 'S10', 'S10', 'i8', 'S10', 'i8', 'S10', 'f8', 'f8', 
            'f8', 'f8', 'f8', 'f8', 'f8', 'f8','f8', 'f8', 'f8', 'f8'])

        self.denGrid = densityDat['Grid']
        self.nDensity = len(self.denGrid)
        self.denGrid = decodeBytes(self.denGrid, self.nDensity)
        self.uDenGrid = np.unique(self.denGrid)
        self.nUDenGrid = len(self.uDenGrid)
        self.denDate = densityDat['date']
        self.denDate = decodeBytes(self.denDate, self.nDensity)
#        self.denElevClass = densityDat['elevation']
#        self.denElevClass = decodeBytes(self.denElevClass, self.nDensity)

        ## FORMAT DENSITY DATES AND GET SIN, COS OF WEEK OF YEAR
        self.getDensityDates()
        self.rawD = densityDat['D'] * 100.0
        self.D = self.rawD.copy() 
        self.sigma = densityDat['sigma']
        self.denMask = ~np.isnan(self.D)
        self.D = self.D[self.denMask]
        self.sigma = self.sigma[self.denMask]
        print('len', np.shape(self.D), np.shape(self.sigma))


#        ## SESSION
#        self.denSession = densityDat['Session']
#        self.denSession = decodeBytes(self.denSession, self.nDensity)
        ## ELEVATION AND MONTHS SINCE MAST
        self.denElev = densityDat['Height']
#        self.monthsMast = densityDat['MonthsSinceMast']
        self.denSites = np.array(['highA', 'highB', 'lowA', 'lowB', 'midA', 'midB'])
        self.nDenSites = len(self.denSites)

    def readEnviron(self):
        ## READ IN ENVIRONMENTAL DATA 
        environDat = np.genfromtxt(self.params.inputEnvironFname,  delimiter=',', 
            names=True, dtype=['S32', 'f8', 'f8', 'f8', 'S32'])
        self.environDeviceID = environDat['DeviceID']
        self.nEnviron = len(self.environDeviceID)
        self.environDeviceID = decodeBytes(self.environDeviceID, self.nEnviron)
        self.envEast = environDat['Easting']
        self.envNorth = environDat['Northing']
        self.envElev = environDat['Elev_vect']
        self.envType = environDat['detectType']
        self.envType = decodeBytes(self.envType, self.nEnviron)


    def readTrackingTunnel(self):
        ## READ IN TRACKING TUNNEL DATA
        TTDat = np.genfromtxt(self.params.inputTTFname,  delimiter=',', 
            names=True, dtype=['S32', 'S10', 'S10', 'S10', 'S32', 'S32', 'S32', 
            'f8', 'i8', 'f8', 'i8', 'i8', 'i8', 'i8', 'S10', 'S10'])
        self.TTID = TTDat['TunnelID']
        self.nTT = len(self.TTID)

        self.mSess = TTDat['MonitorSession']
        self.mSess = decodeBytes(self.mSess, self.nTT)

        trt1080 = TTDat['1080']
        trt1080 = decodeBytes(trt1080, self.nTT)
        maskNo1080 = trt1080 == 'No' 

        ttKeepMask = (self.mSess != 'Post 1080 survey') & maskNo1080
        self.mSess = self.mSess[ttKeepMask]
        print('unique mSess', np.unique(self.mSess))

        self.ttElev = TTDat['Elevation']
        self.ttElev = decodeBytes(self.ttElev, self.nTT)[ttKeepMask]
        print('unique ttElev', np.unique(self.ttElev))

        self.ttStatus = TTDat['TunnelStatus'][ttKeepMask]
        self.ttRat = TTDat['Rat'][ttKeepMask]

#        print('rat', self.ttRat)
#        print('status', self.ttStatus, self.ttStatus.shape)

    def readTT_Density(self):
        ## READ IN TRACKING TUNNEL DATA
        TTDensityDat = np.genfromtxt(self.params.inputDensityTTFname,  delimiter=',', 
            names=True, dtype=['S10', 'S32', 'f8'])
        self.densityTTElev = TTDensityDat['elevation']
        self.nTTDensity = len(self.densityTTElev)

        self.densityTTElev = decodeBytes(self.densityTTElev, self.nTTDensity)
        self.ttSess = TTDensityDat['session']
        print('ttSess', self.ttSess)
        self.ttSess = decodeBytes(self.ttSess, self.nTTDensity)
        self.ttDensity = TTDensityDat['density']

        print('unique', np.unique(self.ttSess), np.unique(self.densityTTElev))
        print('np.shape', np.shape(self.mSess), np.shape(self.ttSess)) 


    def getTrackingRate(self):
        self.ttRate = np.zeros(self.nTTDensity)
        for i in range(self.nTTDensity):
            sessMask = self.mSess == self.ttSess[i]
            elevMask = self.ttElev == self.densityTTElev[i]
            
#            if i == 0:
#                print('sessMask', sessMask, 'elevmask', self.ttElev)

            sessElevMask = sessMask & elevMask
            status_i = np.nansum(self.ttStatus[sessElevMask])

#            print('status mask', self.ttSess[i], self.densityTTElev[i])

            rat_i = np.nansum(self.ttRat[sessElevMask])
            ttRate_i = rat_i / status_i
            self.ttRate[i] = ttRate_i
            print('sess', self.ttSess[i], 'elev', self.densityTTElev[i],'rat_i', rat_i, 
                'status', status_i, 
                'ttRate', self.ttRate[i], 'den', self.ttDensity[i])


#            print('self.ttSess[i]', self.ttSess[i], 'self.densityTTElev[i]', self.densityTTElev[i])
#            print('unique ttElev', np.unique(self.ttElev))
#            print('unique mSess', np.unique(self.mSess))


       
    def getDensityDates(self):
        ## GET WEEKS OF YEAR FOR DENSITY AT SECR

        print('denDate', self.denDate)
        (self.denDate, self.denMonday) = formatDate(self.denDate)
        self.denWeek = getWeek(self.denDate)
        ## UNIQUE DENSITY DATES
        self.uDates = np.unique(self.denMonday)
        self.nDenSessions = len(self.uDates)
        print('udates', self.uDates, 'Type', type(self.uDates))

    def plotTT(self):
        P.figure(figsize=(6,6))
        P.scatter(self.ttDensity, self.ttRate, color='k')
        denArr = np.arange(0, 14.5, .1)
        maxR = 0.94
        Tau = .07
        r = 1.0 - np.exp(-(denArr**2) * Tau)
        pTT = r * np.exp(1.0 - r/maxR)
        P.plot(denArr, pTT, color = 'r', linewidth = 2)
        P.xlabel('Density ($ha^{-1}$)', fontsize = 14)
        P.ylabel('Tracking rate', fontsize = 14)
        P.savefig(self.params.ttRateLineFname, format='png', dpi = 120)
        P.show()

        P.figure(figsize=(6,6))
        P.scatter(self.ttDensity, self.ttRate, color='k')
        P.xlabel('Density ($ha^{-1}$)', fontsize = 14)
        P.ylabel('Tracking rate', fontsize = 14)
        P.savefig(self.params.ttRateDensityFname, format='png', dpi = 120)
        P.show()

    def plotG0(self):
        self.sigmaPara = 30.0
        self.hrRadius = self.sigmaPara*3.0
        self.g0 = 0.1
        self.dist = np.arange(0.0, 60.0 * 3, .5)
        self.pDetect = self.g0 * np.exp(-self.dist**2 / 2.0 / self.sigmaPara**2)
        P.figure(figsize=(6,6))
        P.plot(self.dist, self.pDetect, color='k', linewidth = 4)
        P.xlabel('Distance from device to HR centre (m)', fontsize = 14)
        P.ylabel('Probability of detection', fontsize = 14)
        P.ylim(-0.004, 0.104)
        P.xlim(-5.0, 60.0 * 3.0 + 1)
        P.savefig(self.params.pDetectFname, format='png', dpi = 120)
        P.show()


    def plotG0_small(self):
        self.sigmaPara = 30.0
        self.hrRadius = self.sigmaPara*3.0
        self.g0 = 0.1
        self.dist = np.arange(0.0, self.hrRadius, .5)
        self.pDetect = self.g0 * np.exp(-self.dist**2 / 2.0 / self.sigmaPara**2)
        P.figure(figsize=(6,6))
        P.plot(self.dist, self.pDetect, color='k', linewidth = 4)
        P.xlabel('Distance from device to HR centre (m)', fontsize = 14)
        P.ylabel('Probability of detection', fontsize = 14)
#        P.ylim(-0.004, 0.104)
#        P.xlim(-5.0, 60.0 * 3.0 + 1)
        P.savefig(self.params.pDetect0Fname, format='png', dpi = 120)
        P.show()

    def plotDenSigma(self):
        P.figure(figsize=(6,6))
        print('len', np.shape(self.D), np.shape(self.sigma))
        
        P.scatter(self.D, self.sigma, color='k')
        P.xlabel('Density ($ha^{-1}$)', fontsize = 14)
        P.ylabel(r'$\sigma$ (m)',   fontsize = 14)
#        P.ylim(-0.004, 0.104)
#        P.xlim(-5.0, 60.0 * 3.0 + 1)
        P.savefig(self.params.denSigmaFname, format='png', dpi = 120)
        P.show()

    def simTTData(self):
        denPoints = np.arange(0, 14.1, .33)
        nDen = len(denPoints)
        denArr = np.arange(0, 14.5, .1)
        randVar = [1.2, .9]
        P.figure(figsize=(12,6))
        
        tauPara = [.06, .01]
        maxR = [0.94, .92]
        for i in range(2):
            r = 1.0 - np.exp(-(denArr**2) * tauPara[i])
            pTT = r * np.exp(1.0 - r/maxR[i])
            rMaxMask = r >= maxR[i]
            
            pTT = np.where(rMaxMask, maxR[i], pTT)

            rPoints = 1.0 - np.exp(-(denPoints**2) * tauPara[i])
            pointTT = rPoints * np.exp(1.0 - rPoints/maxR[i])

            logitPTT = logit(pointTT)
            ldev = np.random.normal(0, randVar[i], nDen)
            logitPTT = logitPTT + ldev
            pointTT = inv_logit(logitPTT)
            P.subplot(1,2,i+1)
            P.scatter(denPoints, pointTT, color='k')
            P.plot(denArr, pTT, color = 'r', linewidth = 2)
            P.xlabel('Density ($ha^{-1}$)', fontsize = 14)
            if i == 0:
                P.ylabel('Detection rate', fontsize = 14)
            else:
                P.ylabel('')
            P.ylim(-0.02, 1.02)

        P.savefig(self.params.simTTFname, format='png', dpi = 120)
        P.show()




######################
# Main function
def main():
 
    params = Params()
    explore = Explore(params)




if __name__ == '__main__':
    main()

