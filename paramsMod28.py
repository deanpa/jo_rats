#!/usr/bin/env python

########################################
########################################
# This file is part of a MBIE funded project to study rat dynamics and detection
# process for snap traps, cameras and tracking tunnels in the Alabaster area
# Copyright (C) 2021 Jo Carpenter and Dean Anderson 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# ADDITIONAL NOTES ABOUT THIS SCRIPT.
########################################
########################################

import os
import pickle
import numpy as np
import datetime

class ModelParams(object):
    """
    Contains the parameters for the mcmc run. This object is also required 
    for the pre-processing. 
    """
    def __init__(self):
        ############################################################
        ###################################### USER MODIFY HERE ONLY
        #################################
        
        ###################################################
        ## Set number of MCMC iterations, thin rate and burn in
        self.ngibbs = 3000   #3000    # 1000  #3000  # number of estimates to save for each parameter
        self.thinrate = 100    #500   #10   #30   # 200      # thin rate
        self.burnin = 10000     # burn in number of iterations

        self.totalIterations = ((self.ngibbs * self.thinrate) + self.burnin)
        self.interval = self.totalIterations
        self.checkpointfreq = self.interval

        ## If have not initiated parameters, set to 'True'   
        self.firstRun = True   # True or False         
        print('First Run:', self.firstRun)
        ## USE CHECKED DATA
        self.useCheckedData = False   # gibbs arrays not full from previous runs

        ## CROSS VALIDATION
        self.crossValidation = True 
        if self.crossValidation:
            ## CV ENV VARIABLE
            self.cvJobID = int(os.getenv('SLURM_ARRAY_TASK_ID', default = '0'))  
#            self.cvJobID = 0

        ## Model number
        self.modelID = 28

        print('###')
        print('MODEL' + str(self.modelID) + ': FLAT TOP WITH IMMIGRATION')
        print('### WEEKLY TIME STEP')
        print('### IMMIGRATION REMOVED FROM DENSITY DEPENDENCE')
        print('### CROSS VALIDATION')
        print('###')
        print('###')




        ###################################################

        ## array of iteration id to keep
        self.keepseq = np.arange(self.burnin, ((self.ngibbs * self.thinrate) + 
            self.burnin), self.thinrate)

        # set paths to scripts and data
        self.inputDataPath = os.path.join(os.getenv('ALABASTORPROJDIR', default = '.'), 
            'Data')
        self.outputDataPath = os.path.join(os.getenv('ALABASTORPROJDIR', default = '.'), 
            'Results/mod28Results')

        print('input data path', self.inputDataPath, 'out', self.outputDataPath)

        ## set Data names
        self.basicdataFname = os.path.join(self.outputDataPath, 'basicdataMod28.pkl')
        self.gibbsFname = os.path.join(self.outputDataPath, 'gibbsMod28.pkl')
        self.paramsResFname = os.path.join(self.outputDataPath, 'parameterTableMod28.csv')

        ## Input data   
        self.inputDensityFname = os.path.join(self.inputDataPath, '20210924_densities.csv')
        self.inputSnapFname = os.path.join(self.inputDataPath, 'SnapTrapData.csv')
        self.inputTTFname = os.path.join(self.inputDataPath, 'TT_Data.csv')
        self.inputCameraFname = os.path.join(self.inputDataPath, 'Camera3Night.csv')
        self.inputEnvironFname = os.path.join(self.inputDataPath, 'environVarDevices.csv')

        ## PLOTS AND TABLE NAMES WRITTEN TO DIRECTORY
        self.varioPlotFname = os.path.join(self.outputDataPath, 'variogramPlot.png')
        self.seasonEffectFname = os.path.join(self.outputDataPath, 'seasonEffectPlot.png')
        self.missDensityFname = os.path.join(self.outputDataPath, 'missingDensity.csv')
        self.missSigmaFname = os.path.join(self.outputDataPath, 'missingSigma.csv')
        self.D0Fname = os.path.join(self.outputDataPath, 'D0_Table.csv')
        self.predictedObservedFname = os.path.join(self.outputDataPath, 
                'predictedObservedDen.png')
        self.densityDepFname = os.path.join(self.outputDataPath, 'densityDependenceMod28.png')
        self.rImmRatioFname = os.path.join(self.outputDataPath, 'rImmRatioMod28.png')
        self.fullSimFname = os.path.join(self.outputDataPath, 'fullSimMod28.png')
        self.predOneStepAheadFname = os.path.join(self.outputDataPath, 
            'den_1StepAheadMod28.png')
    
        # pickle mcmcData for checkpointing
        self.outCheckingFname = os.path.join(self.outputDataPath,'out_checking_mod' + 
            str(self.modelID)  + '.pkl')

        print('params basic path', self.basicdataFname) 
        
        ## Set initial parameter values


        ## COVARIATE DATA DICTIONARY FOR DENSITY AND SIGMA
        self.xdatDictionary = {'InterceptGamma' : 0, 'InterceptBeta' : 1, 
                        'Elevation' : 2,
                        'TimeMast_High' : 3, 'SinWeek' : 4, 'CosWeek' : 5,
                        'DensityT_1' : 6, 'SigmaT_1' : 7, 'TimeMastSquared' : 8,
                        'weekWrpC' : 9, 'TimeMast_Low': 10}

        InterceptGamma = self.xdatDictionary['InterceptGamma']
        InterceptBeta = self.xdatDictionary['InterceptBeta']
        Elevation = self.xdatDictionary['Elevation']
        timeMastHigh  = self.xdatDictionary['TimeMast_High']
        timeMastSquared  = self.xdatDictionary['TimeMastSquared']
        SineWeek  = self.xdatDictionary['SinWeek']
        CosineWeek  = self.xdatDictionary['CosWeek']
        DensityT_1  = self.xdatDictionary['DensityT_1']
        SigmaT_1  = self.xdatDictionary['SigmaT_1']
        weekWrpC = self.xdatDictionary['weekWrpC']
        timeMastLow = self.xdatDictionary['TimeMast_Low']
        ######################################################
        ######################################################
        ##
        ## ADJUST THIS TO SET COVARIATES FOR DENSITY AND SIGMA
        self.xDenIndx = np.array([InterceptGamma, timeMastHigh, timeMastLow], dtype = int)
#        self.xDenIndx = np.array([InterceptGamma, Elevation, timeMast, SineWeek,
#            CosineWeek], dtype = int)

        ## SIGMA INDEX
        self.xSigIndx = np.array([InterceptBeta, Elevation], dtype = int)
        ##
        ######################################################
        ######################################################

        ## START DATE IS ONE WEEK BEYOND THE INITIAL POP SIZE ON 2019/3/9
        self.date0 = datetime.date(2019,3,15)


        ## GAMMA PARAMETERS FOR DENSITY KRIGING (EQ. 20 AND 21)
        ## DEN DEPEND INTERCEPT, ELEVATION AND GEN GAMMA COEFF
        self.gamm = np.array([0.1, 0.1])       # [0.15, -0.4, 595])
        self.gammPrior = np.array([np.log(0.1), 1.0])        
        self.gammSearch = 0.02
        self.nGamm = len(self.gamm)

#        self.KMinMax = [1, 2500]

        self.maxK = np.array([(11.5 *100.), (18. * 100.)])
        ## UNIFORM PRIOR
        self.maxKPrior = np.array([[600., 1200.],
                                    [1000., 2200.]])
#        self.maxKPrior = np.array([[np.log(1000), .5],
#                                    [np.log(2000), .5]])
#        self.maxKSearch = 0.03  ## LOG VARIANCE
        self.maxKSearch = 50.0


        self.changeWk = 43
        self.changeWkPrior = np.array([36, 48])
        

####        self.bRatioThreshold = 11.0
####        self.bRatioThresPrior = [11.0, 15.0]
####        self.bRatioThresSearch = 0.6

        self.immWeek = np.array([20, 42], dtype = int)
        self.immWeekPrior = np.array([[18, 28],
                                        [36, 48]])

        self.immModels = np.array([5,7, 25, 27])
        self.immModel = False

        self.minBRatio = 0.01


#        self.bRatioThreshold = 0.75
#        self.bRatioThresPrior = np.array([np.log(0.75), 0.5])
#        self.bRatioThresSearch = 0.08

        ## COVARIANCE PARAMETERS FOR DENSITY
        self.phiDen = .473 #0.03    # DECAY
        self.phiDenPrior = [np.log(1), 1.5]
        self.phiDenSearch = 0.4    #0.18
        self.vDen = .5 # 0.075 #0.8     # VARIANCE FOR LOG OF PREDICTED DENSITY
        self.vDenPrior = [np.log(1.0), 0.75]
        self.vDenSearch = 0.25  #0.18

        ## GOMPERTZ POP GROWTH PARAMETER
        ## INITIAL VALUE: MAX RATE AND IMMIGRATION RATE
        self.r = np.array([0.045, .001])
        self.priorR = np.array([[np.log(.08), 1.0], 
                                [np.log(0.01), 1.0]])   # LOG NORMAL PRIOR
        self.searchR = [0.2, 0.4]




        ## BETA PARAMETERS FOR KRIGING SIGMA (EQ. 9 AND 10)
        ## INTERCEPT, ELEV, T-1 SIGMA
        self.beta = np.array([6.0, -0.33])
        self.nBeta = len(self.beta)
        # BETA PRIORS
#        self.diag = np.diagflat(np.tile(100., self.nBeta))
#        self.vinvert = np.linalg.inv(self.diag)
#        self.betaPrior = [0, 10]    
#        self.betaSearch = .08   #0.08
        self.vInvertBeta = np.linalg.inv(np.diag(np.repeat(20, self.nBeta)))    
        self.ppart = np.dot(self.vInvertBeta, np.zeros(self.nBeta))
#        ## COVARIANCE PARAMETERS FOR SIGMA
#        self.phiSig = 0.03    # DISTANCE DECAY FOR SIGMA
#        self.phiSigPrior = [np.log(1), .5]
#        self.phiSigSearch = 0.2
        self.vSig = 4.0     # VARIANCE FOR LOG OF PREDICTED SIGMA

        self.vSigPrior = [3.0, 3.0]
#        self.vSigSearch = 0.2

        ## LATENT DENSITIES AND SIGMA VIA IMPUTATION
        ## INITIAL DENSITIES AT EACH GRID
        self.D0 = np.array([(0.5 * 100), (0.2 * 100),(9.0 * 100),
            (5.1 * 100), (6.0 * 100), (12.0 * 100)])



#        self.D0 = np.array([0.02, 0.02, 5.36, 5.2, 6.2, 6.4])
#        self.D0 = np.exp(np.random.normal(np.log(1.0), .25, 6))
        self.nD0 = len(self.D0)
        self.D0Prior = np.array([[5.0, 100.0],
                                [5.0, 100.0],
                                [200.0, 1400.0],
                                [100.0, 1000.0],
                                [200.0, 1000.0],
                                [200.0, 1800.0]])
        self.searchD0 = [10.0, 10.0, 50.0, 50.0, 50.0, 50.0]

#        self.lnD0MeanPrior = [np.log(20.0), np.log(20.0), np.log(600), np.log(500), 
#            np.log(600), np.log(700)]
#        self.lnD0SDPrior = np.array([2.0, 2.0, 2.0, 2.0, 2.0, 2.0])
#        self.lnD0Prior = [np.log(2.5), 1.0]
#        self.searchD0 = 0.09
        ## INTIAL MISSING DENSITY ESTIMATES
#        self.missingDen = np.array([0.4, 0.2, 0.8, 0.5, 0.8, 1.3, 0.45])
###        self.missingDen = np.exp(np.random.normal(np.log(.3), .1, 7)) * 100.

####        self.missingDen = np.array([168.1661, 79.6869, 17.1263, 19.6997, 
####            79.3105, 17.0685, 27.1534])

        self.missingDen = np.array([168.1661, 79.6869, 17.1263, 19.6997, 
            79.3105, 17.0685, 27.1534])



        print('missingDen', self.missingDen)

        self.nMissingDen = len(self.missingDen)
        self.missDenPrior = np.array([10, 175.0])
###        self.missDenPrior = [np.log(90), 1.0]
        self.searchMissDen = 10.0


#        ## INITIAL SIGMA AT EACH GRID
        self.sig0 = np.random.normal(35.0, 4.0, 6)
        self.nSig0 = len(self.sig0)
        ## INTIAL MISSING SIGMA ESTIMATES
        self.missingSig = np.random.normal(50.0, 5.0, 7)
        self.nMissingSig = len(self.missingSig)
        self.missSigPrior = [np.log(35), 1.0]
        self.searchMissSig = 0.12



        #################################
        ####################################### END USER MODIFICATION
        ##############################################################
        ## INDICES FOR COSINE AND SINE PLOT IN RESULTS
#        self.sinCosIndx = np.array([SineWeek, CosineWeek], dtype = int)


