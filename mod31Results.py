#!/usr/bin/env python


from paramsMod31 import ModelParams
import numpy as np
import pylab as P
from scipy.stats.mstats import mquantiles
import prettytable
import pickle
import os
import processResults

def main():
    params = ModelParams()
    print('########################')
    print('########################')
    print('###')
    print('#    Model', params.modelID)
    print('###')
    print('########################')
    print('########################')

    # paths and data to read in
    ratpath = os.path.join(os.getenv('ALABASTERPROJDIR', default = '.'), 
            'Results/mod31Results')

#    inputGibbs = os.path.join(predatorpath, 'out_gibbs.pkl')
    fileobj = open(params.gibbsFname, 'rb')
    gibbsobj = pickle.load(fileobj)

    fileobj.close()
    print('gibbsResults Name:', params.gibbsFname)    

    resultsobj = processResults.ResultsProcessing(gibbsobj, params, ratpath)

    print('')
    print('## TABLE OF PARAMETERS FOR DENSITY AND SIGMA AT SECR')
    resultsobj.makeTableFX()

    print('')
    print('## TABLE OF MISSING DENSITY ESTIMATES')
    resultsobj.makeMissDensityTable()

    print('')
    print('## TABLE OF MISSING SIGMA ESTIMATES')
    resultsobj.makeMissSigmaTable()

    print('')
    print('## TABLE OF INITIAL DENSITY ESTIMATES')
    resultsobj.makeInitialDensityTable()

    resultsobj.tracePlotFX()

#    resultsobj.traceMissDen()

###    resultsobj.traceMissSig()

    resultsobj.traceD0()

#    resultsobj.makeVariogram()


    resultsobj.plotDensityDependence()

#    resultsobj.plotTimeMastEff()

#    resultsobj.plotMeanPred()

    resultsobj.plotPredDenSite()

    resultsobj.simulateDensity()

#    resultsobj.calcDIC()

    resultsobj.getRMSE(0, 34)

#    resultsobj.simDensity()

if __name__ == '__main__':
    main()


