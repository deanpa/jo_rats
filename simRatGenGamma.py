#!/usr/bin/env python

import os
import numpy as np
from scipy import stats
import pylab as P
from scipy.special import gamma
from scipy.stats.mstats import mquantiles
import datetime

def dwrpcauchy(th, mu, rho):
    """
    wrapped cauchy pdf: direction is mu, and focus is rho.
    mu is real, and rho > 0
    Wikipedia pdf equation
    """
#    e_num = np.exp(-2*rho)
#    e_denom = 2 * np.exp(-rho)
#    sinh_rho = (1 - e_num) / e_denom
#    cosh_rho = (1 + e_num) / e_denom
    sinh_rho = np.sinh(rho)
    cosh_rho = np.cosh(rho)
    cos_mu_th = np.cos(th - mu)
    dwrpc = sinh_rho / 2 / np.pi / (cosh_rho - cos_mu_th)
    return dwrpc



def genGammPDF(x, a, d, p):
    ## FROM WIKIPEDIA
    num = p / a**d
    dem = gamma(d/p)
    num2 = (x**(d-1)) * (np.exp(-(x / a)**p))
    ggPDF = num * num2 / dem
    return(ggPDF)

def writeMissingData(densityData, dataDates, grid):
    ## MISSING DATA WRITE TO FILE
    missDen = np.zeros(7)
    missSig = np.zeros(7)
    gridDat = np.empty(7, dtype = 'U12')
    dateDat = np.empty(7, dtype = datetime.date)

    mask = (dataDates == datetime.date(2019,7,13)) & (grid == 'highA')



    missDen[0] = densityData[mask]
    gridDat[0] = grid[mask][0]
    dateDat[0] = dataDates[mask][0]
    mask = (dataDates == datetime.date(2020,10,10)) & (grid == 'highA')

    print('len dendata', len(densityData), 'sum mask', np.sum(mask), 'len mask', len(mask),
        'densityDat', densityData[0])


    missDen[1] = densityData[mask]
    gridDat[1] = grid[mask][0]
    dateDat[1] = dataDates[mask][0]
    mask = (dataDates == datetime.date(2021,1,9)) & (grid == 'highA')
    missDen[2] = densityData[mask]
    gridDat[2] = grid[mask][0]
    dateDat[2] = dataDates[mask][0]
    mask = (dataDates == datetime.date(2019,7,13)) & (grid == 'highB')
    missDen[3] = densityData[mask]
    gridDat[3] = grid[mask][0]
    dateDat[3] = dataDates[mask][0]
    mask = (dataDates == datetime.date(2020,10,10)) & (grid == 'highB')
    missDen[4] = densityData[mask]
    gridDat[4] = grid[mask][0]
    dateDat[4] = dataDates[mask][0]
    mask = (dataDates == datetime.date(2021,1,9)) & (grid == 'highB')
    missDen[5] = densityData[mask]
    gridDat[5] = grid[mask][0]
    dateDat[5] = dataDates[mask][0]
    mask = (dataDates == datetime.date(2021,1,9)) & (grid == 'midB')
    missDen[6] = densityData[mask]
    gridDat[6] = grid[mask][0]
    dateDat[6] = dataDates[mask][0]
    ## STRUCTURED ARRAY
    structured = np.empty((7,), dtype=[('Grid', 'U12'), ('Date', 'U12'), 
            ('Density', np.float)])
    structured['Grid'] = gridDat
    structured['Date'] = dateDat
    structured['Density'] = missDen

    filePath = '/home/dean/workfolder/projects/jo_rats/Data'
    fName = os.path.join(filePath, 'missingDataSim.csv')
    np.savetxt(fName, structured, fmt=['%s', '%s', '%.4f'],
        comments = '', delimiter = ',', header = 'Grid, date, missDen')





def makeDataFile(densityData, dataDates):
    grid = np.array(['lowA', 'midA', 'highA', 'lowB', 'midB', 'highB'])
    grid = np.repeat(grid, 7)
    height = np.array([30, 450, 860, 50, 440, 900])
    height = np.repeat(height, 7)
    monthMast = np.ones(len(height))
    ## MISSING DATA WRITE TO FILE
    writeMissingData(densityData, dataDates, grid)

    densityData = np.where((dataDates == datetime.date(2019,7,13)) & (grid == 'highA'),
        np.nan, densityData)
    densityData = np.where((dataDates == datetime.date(2020,10,10)) & (grid == 'highA'),
        np.nan, densityData)
    densityData = np.where((dataDates == datetime.date(2021,1,9)) & (grid == 'highA'),
        np.nan, densityData)
    densityData = np.where((dataDates == datetime.date(2019,7,13)) & (grid == 'highB'),
        np.nan, densityData)
    densityData = np.where((dataDates == datetime.date(2020,10,10)) & (grid == 'highB'),
        np.nan, densityData)
    densityData = np.where((dataDates == datetime.date(2021,1,9)) & (grid == 'highB'),
        np.nan, densityData)
    densityData = np.where((dataDates == datetime.date(2021,1,9)) & (grid == 'midB'),
        np.nan, densityData)
    print('dendat', densityData)
    return(grid, height, monthMast, densityData)



def simRatGenGamma():
    ## ARRAY OF WEEKS SINCE MAST
    wkMast = np.arange(1,102)
    startDate = datetime.date(2019,3,16)
#    startDate = (startDate - datetime.timedelta(days=startDate.weekday()))
    (dates, dataDates, weekRadian, radSeqWeekInYear, indxWeeksInYear) = makeDates(
        wkMast, startDate)
    ## NUMBER OF DATA POINTS FOR DATA SET EXPORT
    nData = len(dataDates)
#    immigDates = [datetime.date(2019,3,15), datetime.date(2020,12,31)]
#    dateMask = (dates > immigDates[0]) & (dates < immigDates[1])
    ## MEAN DENSITY DENPENDENCE PARAMETER AND TIME SINCE MAST COVARIATE

    ## RICKER MODEL ##########################
    para = np.array([.15, 595])
    ## MAXIMUM POPULATION GROWTH RATE; AND IMMIGRATION EFFECT FOR HIGH

    ## RICKER MODEL
    r = [.045, .004]
    ## RHO AND MU FOR WRAP CAUCHY
#    wrpcPara = [0.9, 0.2]
    popSD = 0.08
    ## PARAMETERS FOR THE GENERALISED GAMMA DISTRIBUTION FOR TIME SINCE MAST
#    tmPara = np.array([130., 1.0, 5.0])
    tmPara = np.array([[90., 1.0, 4],
                        [90., 1.07, 4]])
    ## ELEVATION EFFECT FOR LOW, MID AND HIGH
    ## RICKER
    elevEff = np.array([0.0, .00, -.4])


    bRatioThreshold = 0.7


    ## INITIAL DENSITY OF RATS PER HA FOR LOW, MID AND HIGH ELEVATIONS
    ## ADJUSTED TO RATS PER KM SQUARED FOR CALCULATIONS  
#    lnD0 = np.log(np.array([[(5.0 * 100), (7.2 * 100), (0.03 * 100)],
#                            [(6.8 * 100), (6.2 * 100), (0.01 * 100)]]))  #.00001)

    ##  RICKER MODEL
    D0 = (np.array([[(5.4 * 100), (10.0 * 100), (0.05 * 100)],
                            [(7.1 * 100), (6.6 * 100), (0.09 * 100)]]))  #.00001)




    ndat = len(wkMast)
    dArr = np.zeros((ndat, 6))
    bArr = np.zeros((ndat, 6))
    densityData = np.zeros(nData)
    mastArr = np.zeros(ndat)
    migEffArr = np.zeros(ndat)
    b_ratio = np.ones((2,ndat,3))
    bRatioMidTest = np.ones((ndat,2))

    print('ndat', ndat, 'nData', nData)

#    self.n = self.n * np.exp(self.r * (1.0 - (self.n / self.k)))

    
    for k in range(2):
        for j in range(1,3):    
            if j < 2:
                tm_i = genGammPDF(.5, tmPara[0,0], tmPara[0,1], tmPara[0,2])
            else:
                tm_i = genGammPDF(.5, tmPara[1,0], tmPara[1,1], tmPara[1,2])

            ## RICKER FORMULATION   #####################
            b_i = np.exp(para[0] + (para[-1]*tm_i) + elevEff[j])
            

#            b_i = para[0] + (para[-1]*tm_i) + elevEff[j]
#            c = b_i + 1
#            CCapacity = r[0] / (1-c)
            if j == 1:
                b_ratio[k, 0, j] = D0[k,j] / b_i * np.log(D0[k,j])
#                b_ratio[k, 0, j] = D0[k,j]**2 / b_i
                bRatioMidTest[0, k] = D0[k,j] / b_i



#                b_ratio[k, 0, j] = lnD0[k,j] / CCapacity
###                b_ratio[k, 0, j] = lnD0[k,j]**2 / CCapacity
#                bRatioMidTest[0, k] = lnD0[k,j] / CCapacity



            elif j == 2:
                b_ratio[k, 0, j] = D0[k,j] / b_i


#                b_ratio[k, 0, j] = lnD0[k,j] / CCapacity



    countData = 0
    countSites = 0
    ## LOOP EACH LOW, MID AND HIGH TWICE FOR SITES A AND B
    for k in range(2):
#        lnDMid_t_1 = lnD0[k, 1]

        prevYearIndx = 0
        th = radSeqWeekInYear[prevYearIndx]
        dwrpC = dwrpcauchy(th, wrpcPara[1], wrpcPara[0])
        maxDWrpC = np.max(dwrpC)

        for j in range(3):
            D_j = D0[k, j]
#           lnD_j = lnD0[k, j]

            for i in range(ndat):
                date_i = dates[i]

                if j == 2:
                    year_i = date_i.year
#                    yrIndx_i = np.abs(2019 - year_i)
#                    if (yrIndx_i == prevYearIndx):
#                        wrpCMax_i = maxDWrpC
#                    else:
#                        th = radSeqWeekInYear[yrIndx_i] 
#                        dwrpC = dwrpcauchy(th, wrpcPara[1], wrpcPara[0])
#                        maxDWrpC = np.max(dwrpC)
#                        prevYearIndx = yrIndx_i
#                    wrpCDen_i = dwrpcauchy(weekRadian[i], wrpcPara[1], wrpcPara[0])
#                    seasonMigEff = 1.0
#                    seasonMigEff = wrpCDen_i / maxDWrpC


#                tm_i = genGammPDF(wkMast[i], tmPara[0], tmPara[1], tmPara[2])


                if j < 2:
                    tm_i = genGammPDF(wkMast[i], tmPara[0,0], tmPara[0,1], tmPara[0,2])
                else:
                    tm_i = genGammPDF(wkMast[i], tmPara[1,0], tmPara[1,1], tmPara[1,2])



                b_i = np.exp(para[0] + (para[-1]*tm_i) + elevEff[j])


#                c = b_i + 1
#                if (c < -1) | (c > 1):
#                    print('WARNING: c is extreme =', c, 'b_i', b_i) 



#                if (j == 2):
#                if (j == 2) & ((b_ratio[k,1] / b_ratio[k, 2]) >= .25):
#                if (j == 2) & (bRatioMidTest[i, k] >= b_ratio[k, i, 2]):
                if (j == 2) & (bRatioMidTest[i, k] >= bRatioThreshold):
#                if (j == 2) & (b_ratio[k, i, 1] >= bRatioThreshold):
###                if (j == 2) & dateMask[i]:
#                    a = r[1] * lnDMid_t_1**2 / lnD_j
#                    a = r[1] * lnDMid_t_1**2 / lnD_j
                    a = ((r[1] * (b_ratio[k, i, 1] / b_ratio[k, i, 2]))+ r[0])
                    if k == 0:
                        migEffArr[i] = (b_ratio[k, i, 1] / b_ratio[k, i, 2] * r[1])

                    ## RICKER MODEL
                    D_j = D_j * np.exp(a * (1.0  - (D_j / b_i)))

                    # self.n = self.n * np.exp(self.r * (1.0 - (self.n / self.k)))
#                    lnD_j = (lnD_j * (1.0 + b_i)) + a
                else:
                    a = r[0]

                    ## RICKER MODEL

                    dPre = D_j
                    D_j = D_j * np.exp(a * (1.0  - (D_j / b_i)))


#                   lnD_j = (lnD_j * (1.0 + b_i)) + a


                if (k==0) & (j>0):
                    print('wk', i, 'j', j, 'br1', np.round(b_ratio[k, i, 1],2), 'br2', 
                        np.round(b_ratio[k, i, 2], 2), 'a', np.round(a, 2),
                        'brTest', np.round(bRatioMidTest[i,k],2),
                        'dPre', np.round(dPre/100., 2), 
                        'b_i', np.round(b_i/100, 2),
                        'D', np.round(D_j/100, 2), 
                        dates[i])



                if (j == 0) & (k == 0):
                    mastArr[i] = tm_i
#                if j == 2:
#                    lnDMid_t_1 = dArr[i, (countSites - 1)]
                
                if (date_i == dataDates[countData]):
#                    print('countData', countData)

                    ## RICKER MODEL
                    D_j = np.random.normal(D_j, popSD)
                    densityData[countData] = D_j


#                    lnD_j = np.random.normal(lnD_j, popSD)
#                    densityData[countData] = lnD_j

                    if countData < (nData - 1):
                        countData += 1

                ## RICKER MODEL
                dArr[i, countSites] = D_j


#                dArr[i, countSites] = lnD_j
                bArr[i, countSites] = b_i


                if (j > 0) & (i < (ndat-1)):
#                if (j > 0)  & dateMask[i]:
###                    CCapacity = a / (1-c)


                    if j < 2:
                        tm_i = genGammPDF(wkMast[(i+1)], tmPara[0,0], tmPara[0,1], 
                            tmPara[0,2])
                    else:
                        tm_i = genGammPDF(wkMast[(i+1)], tmPara[1,0], tmPara[1,1], 
                            tmPara[1,2])



#                    tm_i = genGammPDF(wkMast[(i+1)], tmPara[0], tmPara[1], tmPara[2])
                    b_i = np.exp(para[0] + (para[-1]*tm_i) + elevEff[j])



                    if j == 1:
                        a = r[0]
                        
                        ## RICKER MODEL
                        b_ratio[k, (i+1), j] = D_j / b_i * np.log(D_j)
#                        b_ratio[k, (i+1), j] = D_j**2 / b_i



                        bRatioMidTest[(i + 1), k] = D_j / b_i 


#                        b_ratio[k, (i+1), j] = lnD_j / CCapacity
#                        b_ratio[k, (i+1), j] = lnD_j**2 / CCapacity
#                        bRatioMidTest[(i + 1), k] = lnD_j / CCapacity 
                    elif j == 2:

                        ## RICKER MODEL
                        b_ratio[k, (i+1), j] = D_j / b_i 

#                        b_ratio[k, (i+1), j] = lnD_j / CCapacity 




            countSites += 1
    return(dArr, bArr, mastArr, dates, dataDates, densityData, wkMast, mastArr, migEffArr) 




def plotPopulations(dArr, wkMast, mastArr, bArr, dates, grid,
        dataDates, densityData, migEffArr):
    uGrid = np.array(['lowA', 'midA', 'highA', 'lowB', 'midB', 'highB'])

    denArr = densityData / 100.0


#    denArr = np.exp(densityData) / 100.0
    P.figure(figsize=(13,9))
    P.subplot(2,1,1)


    popDen = dArr / 100.0

#    popDen = np.exp(dArr) / 100.0

    colLab = np.tile(np.array(['k', 'b', 'r']), 2)
    elevLab = np.array(['Low', 'Mid', 'High'])
    for i in range(6):
        if i < 3:
            P.plot(dates, popDen[:,i], color = colLab[i], label=elevLab[i])
        else:
            P.plot(dates, popDen[:,i], color = colLab[i])
        dataMask = (grid == uGrid[i])
        P.plot(dataDates[dataMask], denArr[dataMask], color=colLab[i], 
            linestyle='',marker='o')
###    P.plot(dates, bArr[:, -1] / 100.0, color = 'r', linestyle='dashed')
###    P.plot(dates, bArr[:, 1] / 100.0, color = 'b', linestyle='dashed')
#    P.plot(dates, popDen[:,1], color = 'b', label='Mid')
#    P.plot(dates, popDen[:,2], color = 'r', label='High')
    P.vlines(x = datetime.date(2019, 7, 13), ymin = np.min(popDen[:,2]), 
        ymax = np.max(popDen[:,1]), color = 'k', linestyle = 'dashed')
    P.ylabel('Rat density ($ha^{-1}$)')
    P.xlabel('Weeks since mast')
    P.legend(loc= 'upper right')
    P.subplot(2,3,4)
    P.plot(wkMast, mastArr)
    P.ylabel('Mast effect (gen gamma PDF)')
    P.xlabel('Weeks since mast')
    P.subplot(2,3,5)
    P.plot(wkMast, bArr[:,0] / 100, color = 'k', label='Low')
    P.plot(wkMast, bArr[:,1] / 100, color = 'b', label='Mid')
    P.plot(wkMast, bArr[:,2] / 100, color = 'r', label='High')
    P.legend(loc= 'upper right')
    P.ylabel('Density dependence')
    P.xlabel('Weeks since mast')
    P.subplot(2,3,6)
    P.plot(wkMast, migEffArr)
    P.ylabel('Migration effect')
    P.xlabel('Weeks since mast')
    P.tight_layout()
    P.savefig('ratDyn_GenGamma2.png', format='png', dpi = 600)
    P.show()



def makeDates(wkMast, startDate):
    dates = []
    startDate = datetime.date(2019, 3, 16)
    date_i = startDate
    monthsSinceMast = np.array([4,7,10,14,17,19,22])
    dataDates = []
#    dataJulYear = []
    julYearRun = []
    n = len(wkMast)
    dates.append(date_i)
    ## GET WEEK RADIANS 
    weekRadian = []
    year_i = date_i.year
    weekInYear = datetime.date(year_i, 12, 28).isocalendar()[1]
    week_i = date_i.isocalendar()[1]
    weekRad_i = week_i/weekInYear * 2.0 * np.pi
    weekRadian.append(weekRad_i)
    ## SEQ OF WEEKS IN EACH YEAR FOR CALC MAX WRP CAUCHY PDF
    radSeqWeekInYear = getWeekSeq()
    indxWeeksInYear = []
    
    cc = 0
    for i in range(1, n):
        date_i = date_i + datetime.timedelta(days = 7)            
        dates.append(date_i)
        ## GET WEEKLY RADIANS FOR WRAPPED CAUCHY
        year_i = date_i.year
        weekInYear = datetime.date(year_i, 12, 28).isocalendar()[1]
        week_i = date_i.isocalendar()[1]
        weekRad_i = week_i/weekInYear * 2.0 * np.pi
        weekRadian.append(weekRad_i)
        indxWeeksInYear.append(np.abs(2019 - year_i))
        if (cc < len(monthsSinceMast)):
            weeksSinceMast = np.round((monthsSinceMast[cc] * 4.3),0)
            if (weeksSinceMast == i):
                dataDates.append(date_i)
#                print('i', i, 'weeksSince', weeksSinceMast, 'date_i', date_i)
                cc += 1
    dates = np.array(dates)
    dataDates = np.array(dataDates)
    dataDates = np.tile(dataDates, 6)
    julYearRun = np.array(julYearRun)
    weekRadian = np.array(weekRadian)
    
    print('dataDates', dataDates, 'dates', dates, 'weekRad', weekRadian)
    return(dates, dataDates, weekRadian, radSeqWeekInYear, indxWeeksInYear)

def getWeekSeq():
    years = np.arange(2019, 2022, dtype = int)
    radSeqWeekInYear = []
    for i in years:
        weekInYear = datetime.date(i, 12, 28).isocalendar()[1]
        yearSeqWeekInYear = (np.arange(1, (weekInYear +1)))
        radSeqWeekInYear.append(yearSeqWeekInYear / np.max(yearSeqWeekInYear) * 
            2.0 * np.pi)
    return(radSeqWeekInYear)


def printData(dataDates, densityData, grid, height):
    ndat = len(grid)
    for i in range(ndat):
        print('i', i,dataDates[i], grid[i], height[i], 
            'den', np.round(((densityData[i]) / 100.0), 4))



def simSigma(densityData, height):
    ndat = len(densityData)
    sigmaSD = .1
    beta = np.array([5.8, -0.23])
    sigmaData = np.zeros(ndat)
    for i in range(ndat):
        if np.isnan(densityData[i]):
            sigmaData[i] = np.nan
        else:
            muNumer = np.exp(beta[0] + beta[1]*np.log(height[i]))
            den_i = (densityData[i]) / 100.0
            mu = muNumer / np.sqrt(den_i)
            sigmaData[i] = np.exp(np.random.normal(np.log(mu), sigmaSD))
        print('i', i, 'den', np.round(densityData[i] /100.0,2), 
            'sigma', np.round(sigmaData[i],2))
    return(sigmaData)






def writeToFile(grid, dataDates, height, densityData, sigmaData):
    # create new structured array with columns of different types
    ndat = len(grid)
    structured = np.empty((ndat,), dtype=[('Grid', 'U12'), ('Date', 'U12'), 
            ('Height', np.float), ('Density', np.float), ('Sigma', np.float)])

    ratPerHa = (densityData)/100.0
    print(np.shape(structured), len(grid), len(dataDates), len(height), 
        len(densityData), 'densityData', (densityData)/100.0)

    # copy data over
    structured['Grid'] = grid
    structured['Date'] = dataDates
    structured['Height'] = height
    structured['Density'] = ratPerHa
    structured['Sigma'] = sigmaData    
    filePath = '/home/dean/workfolder/projects/jo_rats/Data'
    fName = os.path.join(filePath, 'simPopData2.csv')
    np.savetxt(fName, structured, fmt=['%s', '%s', '%.1f', '%.4f', '%.4f'],
        comments = '', delimiter = ',', header = 'Grid, date, Height, D, sigma')




def main():

    (dArr,bArr, mastArr, dates,dataDates,densityData,wkMast,mastArr, migEffArr) = simRatGenGamma()
    (grid, height, monthMast, densityData) = makeDataFile(densityData, dataDates)
    printData(dataDates, densityData, grid, height) 
    plotPopulations(dArr, wkMast, mastArr, bArr, dates, grid, dataDates, densityData,
        migEffArr)        
    sigmaData = simSigma(densityData, height)
    writeToFile(grid, dataDates, height, densityData, sigmaData)



if __name__ == '__main__':
    main()





def simRatGenGammaXXX():
    ## ARRAY OF WEEKS SINCE MAST
    wkMast = np.arange(1,102)
    startDate = datetime.date(2019,3,16)
#    startDate = (startDate - datetime.timedelta(days=startDate.weekday()))
    (dates, dataDates, weekRadian) = makeDates(wkMast, startDate)
    nData = len(dataDates)
#    immigDates = [datetime.date(2019,3,15), datetime.date(2020,12,31)]
#    immigDates = [datetime.date(2019,7,15), datetime.date(2020,4,10)]
#    dateMask = (dates > immigDates[0]) & (dates < immigDates[1])
    ## MEAN DENSITY DENPENDENCE PARAMETER AND TIME SINCE MAST COVARIATE
    para = np.array([-.22, 21.5])
    ## MAXIMUM POPULATION GROWTH RATE; AND IMMIGRATION EFFECT FOR HIGH
    r = [.25, .1]
    wrpcPara = [1.0, .48]
    popSD = 0.175
    ## PARAMETERS FOR THE GENERALISED GAMMA DISTRIBUTION FOR TIME SINCE MAST
    tmPara = np.array([127., 1.0, 3.7])
#    tmParaHigh = tmPara.copy()      #np.array([105., 1.0, 2.5])
#    tmParaHigh = np.array([105., 1.0, 2.5])
    ## ELEVATION EFFECT FOR LOW, MID AND HIGH
    elevEff = np.array([0.0, .00, -.03])
    ## INITIAL DENSITY OF RATS PER HA FOR LOW, MID AND HIGH ELEVATIONS
    ## ADJUSTED TO RATS PER KM SQUARED FOR CALCULATIONS  
    lnD0 = np.log(np.array([[(5.0 * 100), (8.6 * 100), (0.0101 * 100)],
                            [(7.4 * 100), (6.2 * 100), (0.05 * 100)]]))  #.00001)
    ndat = len(wkMast)
    dArr = np.zeros((ndat, 6))
    bArr = np.zeros((ndat, 6))
    densityData = np.zeros(nData)
    mastArr = np.zeros(ndat)
    b_ratio = np.ones((2,3))
    
    for k in range(2):
        for j in range(1,3):    
            tm_i = genGammPDF(.5, tmPara[0], tmPara[1], tmPara[2])
            b_i = para[0] + (para[-1]*tm_i) + elevEff[j]
            c = b_i + 1
            CCapacity = r[0] / (1-c)
            if j == 1:
                b_ratio[k, j] = lnD0[k,j]**2 / CCapacity
            else:
                b_ratio[k, j] = lnD0[k,j] / CCapacity







    bRatioThreshold = .75
    countData = 0
    countSites = 0
    ## LOOP EACH LOW, MID AND HIGH TWICE FOR SITES A AND B
    for k in range(2):
        lnDMid_t_1 = lnD0[k, 1]
        ## LOOP WEEKS
        for i in range(ndat):
            date_i = dates[i]
            tm_i = genGammPDF(wkMast[i], tmPara[0], tmPara[1], tmPara[2])
            ## LOOP SITES
            for j in range(3):
                if i == 0:
                    lnD_j = lnD0[k, j]
                b_i = para[0] + (para[-1]*tm_i) + elevEff[j]

                c = b_i + 1
                if (c < -1) | (c > 1):
                    print('WARNING: c is extreme =', c, 'b_i', b_i) 
#                if (j == 2):
#                if (j == 2) & ((b_ratio[k,1] / b_ratio[k, 2]) >= .25):
                if (j == 2) & (b_ratio[k,1] >= bRatioThreshold):
###                if (j == 2) & dateMask[i]:
#                    a = r[1] * lnDMid_t_1**2 / lnD_j
#                    a = r[1] * lnDMid_t_1**2 / lnD_j
                    a = (r[1] * (b_ratio[k,1] / b_ratio[k, 2])) + r[0]
                    lnD_j = (lnD_j * (1.0 + b_i)) + a
                else:
                    a = r[0]
                    lnD_j = (lnD_j * (1.0 + b_i)) + a
                if (j == 0) & (k == 0):
                    mastArr[i] = tm_i
                if j == 2:
                    lnDMid_t_1 = dArr[i, (countSites - 1)]
                
                if (date_i == dataDates[countData]):
#                    print('countData', countData)
                    lnD_j = np.random.normal(lnD_j, popSD)
                    densityData[countData] = lnD_j
                    if countData < (nData - 1):
                        countData += 1
                dArr[i, countSites] = lnD_j
                bArr[i, countSites] = b_i


                if (j > 0):
#                if (j > 0)  & dateMask[i]:
                    CCapacity = a / (1-c)
                    if j == 1:
                        b_ratio[k, j] = lnD_j**2 / CCapacity 
                    else:
                        b_ratio[k, j] = lnD_j / CCapacity 




            countSites += 1
    return(dArr, bArr, mastArr, dates, dataDates, densityData, wkMast, mastArr) 
