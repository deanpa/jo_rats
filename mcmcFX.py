#!/usr/bin/env python

########################################
########################################
# This file is part of OSPRI possum g0 and sigma project
# Copyright (C) 2018 Dean Anderson 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# ADDITIONAL NOTES ABOUT THIS SCRIPT.
########################################
########################################

### Import modules: ###
import os
from scipy import stats
from scipy.special import gammaln
from scipy.special import gamma
import numpy as np
#from numba import jit
from numba import njit
import pickle
from scipy.stats.mstats import mquantiles
from copy import deepcopy
from preProcessing import genGammaPDF

def gamma_pdf(xx, shape, scale):
    gampdf = 1.0 / (scale**shape) / gamma(shape) * xx**(shape - 1) * np.exp(-(xx/scale))
    return gampdf


def logit(x):
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    return np.exp(x) / (1 + np.exp(x))

    
@njit
def rickerWeeksSites(i, nDenSites, D, diffWeeks,
        r, mArray_i, bRatio_i, 
        K_s, nDenSessions, bRatio_Up1, immWeek, week, immModel, k_up1, minBRatio):
    ## LOOP THROUGH WEEKS IN SESSION i
    for k in range(diffWeeks[i]):
        ## LOOP DENSITY SITES (6)
        for j in range(nDenSites):
            ## DENSITY DEPENDENCE FOR EACH CURRENT SUB-SESSION
            if j < 2:
                if k == 0:
                    K_ij = k_up1[0]
                else:
                    K_ij = K_s[k - 1, 0]
            else:
                if k == 0:
                    K_ij = k_up1[1]
                else:
                    K_ij = K_s[k - 1, 1]
            ## IF HAVE MIGRATION CONDITIONS
            if (j < 2) & immModel:
                if (week >= immWeek[0]) & (week <= immWeek[1]):
                    if bRatio_i[k, j] < minBRatio:
                        bRatio_i[k, j] = minBRatio
                    a = r[1] * (bRatio_i[k, (j + 2)] / bRatio_i[k, j])
                    D[j] = (D[j] * np.exp(r[0] * (1.0  - (D[j] / K_ij))) + 
                        (D[j + 4] * a))
                else:
                    D[j] = D[j] * np.exp(r[0] * (1.0  - (D[j] / K_ij)))
            else:
                D[j] = D[j] * np.exp(r[0] * (1.0  - (D[j] / K_ij)))
            if D[j] < 5.0:
                D[j] = 5.0
            ## IF NOT LAST WEEK IN SESSION, UPDATE B RATIO VALUES
            if (k < (diffWeeks[i] - 1)) & immModel: 
                if j < 2:
                    bRatio_i[(k+1), j] = D[j] / K_s[k, 0]   #K_ij
                elif j > 3:
                    bRatio_i[(k+1), (j - 2)] = D[j] / K_s[k, 1] #  K_ij
            ##  IF LAST WEEK OF SESSION; POPULATE mArray_i
            if k == (diffWeeks[i] - 1):
                mArray_i[j] = np.log(D[j])
                if (i < (nDenSessions - 1)) & immModel:
                    if j < 2:
                        bRatio_Up1[j] = D[j] / K_s[k, 0]
                    elif j > 3:
                        bRatio_Up1[(j - 2)] = D[j] / K_s[k, 1]
        week += 1
    return(mArray_i, bRatio_i, K_s, bRatio_Up1, week)



@njit
def loopSitesWeeks(i, nDenSites, spacePred_s, tmPred_i, lnDensity_i_1, diffWeeks,
        r, mArray_i):
    ## LOOP DENSITY SITES (6)
    for j in range(nDenSites):
        logD = lnDensity_i_1[j]
        ## LOOP THROUGH WEEKS IN SESSION i
        for k in range(diffWeeks[i]):
            ## DENSITY DEPENDENCE FOR EACH SUB-SESSION
            b_ijk = spacePred_s[j] + tmPred_i[k] 
            logD = logD * (1.0 + b_ijk) + r 
        mArray_i[j] = logD
    return(mArray_i)

def loopSitesWeeksTEST(i, nDenSites, spacePred_s, totalTemporal_i, lnDensity_i_1, 
        diffWeeks, r, mArray_i, spacePred, TOTALTEMPORAL, ARRAYM):
    ## LOOP DENSITY SITES (6)
    for j in range(nDenSites):
        logD = lnDensity_i_1[j]
        LOGD = lnDensity_i_1[j]
        ## LOOP THROUGH WEEKS IN SESSION i
        for k in range(diffWeeks[i]):
            ## DENSITY DEPENDENCE FOR EACH SUB-SESSION
            b_ijk = spacePred_s[j] + totalTemporal_i[k] 
            logD = logD * (1.0 + b_ijk) + r
            B = spacePred[j] + TOTALTEMPORAL[k]
            LOGD = LOGD * (1.0 + B) + r 
        mArray_i[j] = logD
        ARRAYM[j] = LOGD
    return(mArray_i, ARRAYM)







class MCMC(object):
    def __init__(self, params, basicdata, checkingdata=None):

        self.basicdata = basicdata
        self.params = params
        self.wkUpDown = [-1, 1]
        self.immModel = self.params.immModel
#        self.immModel = np.in1d(self.params.modelID, self.params.immModels)


        print("immModel", self.immModel)

        ## If use checking data
        if checkingdata is None:
            # storage arrays for parameters
            self.startingStep = 0
            ## STORAGE ARRAYS FOR POSTERIORS
            self.gammGibbs = np.zeros((self.params.ngibbs, self.params.nGamm))
            self.maxKGibbs = np.zeros((self.params.ngibbs, 2))
            self.changeWkGibbs = np.zeros(self.params.ngibbs)
            self.vDenGibbs = np.zeros(self.params.ngibbs)                        
            self.phiDenGibbs = np.zeros(self.params.ngibbs)        
            self.betaGibbs = np.zeros((self.params.ngibbs, self.params.nBeta))      
            self.vSigGibbs = np.zeros(self.params.ngibbs)                        
#           self.phiSigGibbs = np.zeros(self.params.ngibbs)        
            self.D0Gibbs = np.zeros((self.params.ngibbs, self.params.nD0)) 
            self.missDenGibbs = np.zeros((self.params.ngibbs, self.params.nMissingDen))
#           self.sig0Gibbs = np.zeros((self.params.ngibbs, self.params.nSig0))
            self.missSigGibbs = np.zeros((self.params.ngibbs, self.params.nMissingSig))
            self.rGibbs = np.zeros((self.params.ngibbs, 2))
            self.immWeekGibbs = np.zeros((self.params.ngibbs, 2))
            self.DICGibbs = np.zeros(self.params.ngibbs)
            self.DIC_g = 0.0
####            self.bRatioThresGibbs = np.zeros(self.params.ngibbs)
            ## STORAGE ARRAYS OF PREDICTED MEANS FOR PLOTTING
            self.meanMDen = np.zeros(self.basicdata.nDensity)
            self.meanMuSigma = np.zeros(self.basicdata.nDensity)
            # Index into array of Gibbs steps to save
            self.cc = 0
        else:
            self.startingStep = checkingdata.startingStep
            self.gammGibbs = checkingdata.gammGibbs
            self.maxKGibbs = checkingdata.maxKGibbs
            self.changeWkGibbs = checkingdata.changeWkGibbs
            self.vDenGibbs = checkingdata.vDenGibbs                        
            self.phiDenGibbs = checkingdata.phiDenGibbs        
            self.betaGibbs = checkingdata.betaGibbs      
            self.vSigGibbs = checkingdata.vSigGibbs                        
            self.D0Gibbs = checkingdata.D0Gibbs 
            self.missDenGibbs = checkingdata.missDenGibbs
            self.missSigGibbs = checkingdata.missSigGibbs
            self.rGibbs = checkingdata.rGibbs
            self.immWeekGibbs = checkingdata.immWeekGibbs
####            self.bRatioThresGibbs = checkingdata.bRatioThresGibbs
            ## STORAGE ARRAYS OF PREDICTED MEANS FOR PLOTTING
            self.meanMDen = np.zeros(self.basicdata.nDensity)
            self.meanMuSigma = np.zeros(self.basicdata.nDensity)
            # Index into array of Gibbs steps to save
            self.cc = checkingdata.cc

        if self.params.crossValidation:
            self.cvErrorSq = 0.0
            self.cvMeanErr = 0.0

        ## run mcmcFX - gibbs loop
        print('Start cc', self.cc)
        self.mcmc()




    def gamm_update(self):
        """
        METROPOLIS SAMPLER FOR GAMMA AND BETA
        """
        ## INTERCEPT AND ELEVATION OR OTHER SPATIAL COVARIATES
        for h in range(self.params.nGamm):
            llik_s = 0.0
            llik = 0.0
            g_h = np.exp(np.random.normal(np.log(self.basicdata.gamm[h]), 
                    self.params.gammSearch))
            g_s = self.basicdata.gamm.copy()
            g_s[h] = g_h
            K_High0 = self.basicdata.maxK[0] 
            K_Low0 = self.basicdata.maxK[1]
            k_up1 = np.zeros(2)                 ## K FOR FIRST WEEK OF NEXT SESS
            week = 0
            ## LOOP SESSIONS
            for i in range(self.basicdata.nDenSessions):
                ## TIME SINCE MAST EFFECT
                wkMask = self.basicdata.maskNegExpWk_List[i]
                negExpWk_i = self.basicdata.negExpWk_List[i]
                K_High_s = np.repeat(self.basicdata.maxK[0], self.basicdata.diffWeeks[i])
                negExpHigh_s = np.exp(-negExpWk_i * g_s[0])
                K_High_s[wkMask] = self.basicdata.maxK[0] * negExpHigh_s
                K_Low_s = np.repeat(self.basicdata.maxK[1], self.basicdata.diffWeeks[i])
                negExpLow_s = np.exp(-negExpWk_i * g_s[1])
                K_Low_s[wkMask] = self.basicdata.maxK[1] * negExpLow_s

                self.basicdata.negExpPred_HighList_s[i] = negExpHigh_s
                self.basicdata.negExpPred_LowList_s[i] = negExpLow_s

                if np.sum(wkMask) != len(negExpHigh_s):
                    print('gamm', 'len wkMask', len(wkMask), 'sumWkMask', np.sum(wkMask),
                        'len negExpH_s & L', len(negExpHigh_s), len(negExpLow_s))

                bRatio_i = self.basicdata.bRatioList_s[i]
                self.basicdata.K_List_s[i][:, 0] = K_High_s
                self.basicdata.K_List_s[i][:, 1] = K_Low_s

                ## START DENSITY FOR SESSION I
                if i == 0:
                    D = self.basicdata.D0.copy()
                    k_up1[0] = K_High0                    
                    k_up1[1] = K_Low0                    
                    ## GET B RATIO FOR HIGH AND MID SITES FOR SESSION 0
                    for j in range(2):
                        ## MID SITE DENSITY AND B (CARRYING CAPACITY) AND bRatio
                        D_mid = self.basicdata.D0[j + 4]
                        K_mid = K_Low0
                        bRatio_i[0, (j + 2)] = D_mid / K_mid
                        ## HIGH SITE bRatio
                        D_High= self.basicdata.D0[j]
                        K_High = K_High0
                        bRatio_i[0, j] = D_High / K_High
                else:
                    D = deepcopy(self.basicdata.listDensity[(i - 1)])
                    ## LAST K OF PREVIOUS SESSION - WILL BE FIRST K IN SESS I
                    k_up1[0] = self.basicdata.K_List_s[i - 1][-1, 0]                    
                    k_up1[1] = self.basicdata.K_List_s[i - 1][-1, 1]                    
                
                bRatio_Up1 = np.zeros(4)


#                print('START  h', h, 'ses', i, 'M', np.round(self.basicdata.listM[i],4),
#                    'M_s', np.round(self.basicdata.listM_s[i], 4))

                mArray_i = self.basicdata.listM_s[i]

#                print('BEFORE h', h, 'ses', i, 'M', np.round(self.basicdata.listM[i],4),
#                    'mArray_i', np.round(mArray_i, 4),
#                    'M_s', np.round(self.basicdata.listM_s[i], 4))



                ## NUMBA LOOP THROUGH WEEKS AND SITES
                (mArray_i, bRatio_i, K_s, 
                    bRatio_Up1, week) = rickerWeeksSites(i, 
                    self.basicdata.nDenSites, D, 
                    self.basicdata.diffWeeks,
                    self.basicdata.r, mArray_i, bRatio_i, 
                    self.basicdata.K_List_s[i], 
                    self.basicdata.nDenSessions, bRatio_Up1, self.basicdata.immWeek, 
                    week, self.immModel, k_up1, self.params.minBRatio)


#                print('AFTER  h', h, 'ses', i, 'M', np.round(self.basicdata.listM[i],4),
#                    'mArray_i', np.round(mArray_i, 4),
#                    'M_s', np.round(self.basicdata.listM_s[i], 4))


#                self.basicdata.listM_s[i] = deepcopy(mArray_i)


#                print('POST NUMBA COPY  h', h, 'ses', i, 'M', np.round(self.basicdata.listM[i],4),
#                    'mArray_i', np.round(mArray_i, 4),
#                    'M_s', np.round(self.basicdata.listM_s[i], 4))




                ## PUT THE B RATIO AND B MID TEST VALUES IN NEXT UP SESSION FOR TIME 0
                if i < (self.basicdata.nDenSessions - 1):
                    for j in range(self.basicdata.nDenSites):
                        if j < 2:
                            self.basicdata.bRatioList_s[(i + 1)][0, j] = bRatio_Up1[j]
                        elif j > 3:
                            self.basicdata.bRatioList_s[(i + 1)][0, (j - 2)] = (
                                bRatio_Up1[j-2])
                ## GET LIKELIHOODS
                ll_s_i = stats.multivariate_normal.logpdf(self.basicdata.listLnDensity[i],
                    mArray_i, self.basicdata.gammCovMat)
                ll_i = stats.multivariate_normal.logpdf(self.basicdata.listLnDensity[i], 
                    self.basicdata.listM[i], self.basicdata.gammCovMat)
                llik_s += ll_s_i 
                llik += ll_i
            ##  PRIORS AND IMPORTANCE RATIO
            prior_s = (stats.norm.logpdf(np.log(g_s[h]), self.params.gammPrior[0], 
                self.params.gammPrior[1]))
            prior = (stats.norm.logpdf(np.log(self.basicdata.gamm[h]), self.params.gammPrior[0], 
                self.params.gammPrior[1]))
            pnow = llik + prior
            pnew = llik_s + prior_s
            pdiff = pnew - pnow
            if pdiff > 1.0:
                rValue = 1.0
                zValue = 0.0
            elif pdiff < -12.0:
                rValue = 0.0
                zValue = 1.0
            else:
                rValue = np.exp(pdiff)        # calc importance ratio
                zValue = np.random.uniform(0.0, 1.0, size = None)
            if (rValue > zValue):
                self.basicdata.gamm = g_s.copy()
                self.basicdata.listM = deepcopy(self.basicdata.listM_s)
                self.basicdata.K_List = deepcopy(self.basicdata.K_List_s)
                self.basicdata.bRatioList = deepcopy(self.basicdata.bRatioList_s)
####                self.basicdata.bRatioMidTestList = (
####                    deepcopy(self.basicdata.bRatioMidTestList_s))
                self.basicdata.negExpPred_HighList = deepcopy(
                    self.basicdata.negExpPred_HighList_s)
                self.basicdata.negExpPred_LowList = deepcopy(
                    self.basicdata.negExpPred_LowList_s)


    def v_update(self, v, searchPara, Cor, Cov, Y, currentPrediction, prior):
        v_s = np.exp(np.random.normal(np.log(v), searchPara))
        cov_s = v_s * Cor
        llik_s = 0.0
        llik = 0.0
        for i in range(self.basicdata.nDenSessions):
            llik += stats.multivariate_normal.logpdf(Y[i], currentPrediction[i], Cov)
            llik_s += stats.multivariate_normal.logpdf(Y[i], currentPrediction[i], cov_s)
        prior_s = (stats.norm.logpdf(np.log(v_s), prior[0], prior[1]))
        prior = (stats.norm.logpdf(np.log(v), prior[0], prior[1]))
        pnow = llik + prior
        pnew = llik_s + prior_s
        pdiff = pnew - pnow
        if pdiff > 1.0:
            rValue = 1.0
            zValue = 0.0
        elif pdiff < -12.0:
            rValue = 0.0
            zValue = 1.0
        else:
            rValue = np.exp(pdiff)        # calc importance ratio
            zValue = np.random.uniform(0.0, 1.0, size = None)
        if (rValue > zValue):
            v = v_s
            Cov = cov_s.copy()
        return(v, Cov)


    def phi_update(self, p, v, searchPara, Cor, Cov, Y, currentPrediction, prior):
        p_s = np.exp(np.random.normal(np.log(p), searchPara))
        cor_s = np.exp(-p_s * self.basicdata.denDistMat)
        cov_s = v * cor_s
        llik_s = 0.0
        llik = 0.0
        for i in range(self.basicdata.nDenSessions):
            llik += stats.multivariate_normal.logpdf(Y[i], currentPrediction[i], Cov)
            llik_s += stats.multivariate_normal.logpdf(Y[i], currentPrediction[i], cov_s)
        prior_s = (stats.norm.logpdf(np.log(p_s), prior[0], prior[1]))
        prior = (stats.norm.logpdf(np.log(p), prior[0], prior[1]))
        pnow = llik + prior
        pnew = llik_s + prior_s
        pdiff = pnew - pnow
        if pdiff > 1.0:
            rValue = 1.0
            zValue = 0.0
        elif pdiff < -12.0:
            rValue = 0.0
            zValue = 1.0
        else:
            rValue = np.exp(pdiff)        # calc importance ratio
            zValue = np.random.uniform(0.0, 1.0, size = None)
        if (rValue > zValue):
            p = p_s
            Cor = cor_s.copy()
            Cov = cov_s.copy()
        return(p, Cor, Cov)


    def updateMissDensity(self):
        ## GET D0_S PROPOSAL FROM UNIFORM DISTRIBUTION AND M-H CORRECTION
#        qNewGivenNow = 0.0
#        qNowGivenNew = 0.0
        for m in range(self.params.nMissingDen):
            missDen_s = self.basicdata.missingDen.copy()
            minDiff = self.basicdata.missingDen[m] - self.params.missDenPrior[0] 
            maxDiff = self.params.missDenPrior[1] - self.basicdata.missingDen[m]
            if minDiff < self.params.searchMissDen:
                lowSearch = self.params.missDenPrior[0]
                lowCount = minDiff
            else:
                lowSearch = self.basicdata.missingDen[m] - self.params.searchMissDen
                lowCount = self.params.searchMissDen
            if maxDiff < self.params.searchMissDen:
                highSearch = self.params.missDenPrior[1]
                highCount = maxDiff
            else:
                highSearch = self.basicdata.missingDen[m] + self.params.searchMissDen
                highCount = self.params.searchMissDen
            nowCount = lowCount + highCount
            qNewGivenNow = np.log(1.0 / nowCount)
            missDen_s[m] = np.random.uniform(lowSearch, highSearch)
            ## GET PROPOSAL COUNT FOR M-H CORRECTION
            minDiff = missDen_s[m] - self.params.missDenPrior[0]
            maxDiff = self.params.missDenPrior[1] - missDen_s[m]
            if minDiff < self.params.searchMissDen:
                lowNewCount = minDiff
            else:
                lowNewCount = self.params.searchMissDen
            if maxDiff < self.params.searchMissDen:
                highNewCount = maxDiff
            else:
                highNewCount = self.params.searchMissDen
            newCount = lowNewCount + highNewCount
            qNowGivenNew = np.log(1.0 / newCount)
#        print('before den', np.round(self.basicdata.listLnDensity,3)) 
#        print('before d_s', np.round(self.basicdata.listLnDen_s,3))
###        missDen_s = np.exp(np.random.normal(np.log(self.basicdata.missingDen), 
###            self.params.searchMissDen))
###        missDen_s = np.where(missDen_s < 1.0, missDen_s +1.0, missDen_s)

            if np.min(missDen_s) <=0:
                print('missDen_s <=0', missDen_s)

            lnMissDen_s = np.log(missDen_s)
            K_High0 = self.basicdata.maxK[0] 
            K_Low0 = self.basicdata.maxK[1]
#        print('proposed den_s', np.round(lnMissDen_s,3))
        ## LOOP THRU ALL MISSING DATA TO POPULATE PROPOSED DENSITY DATA
            for i in range(self.params.nMissingDen):
                sess_i = self.basicdata.listSessSiteMissDen[i][0]
                site_i = self.basicdata.listSessSiteMissDen[i][1]
                self.basicdata.listLnDen_s[sess_i][site_i] = lnMissDen_s[i]
                self.basicdata.listDen_s[sess_i][site_i] = missDen_s[i]
            ## CALC LIKELIHOODS
            llik_s = 0.0
            llik = 0.0
            sigLLik_s = 0.0
            sigLLik = 0.0
            k_up1 = np.zeros(2)                 ## K FOR FIRST WEEK OF NEXT SESS
            week = 0
            ## LOOP THRU SESSIONS
            for i in range(self.basicdata.nDenSessions):
                bRatio_i = self.basicdata.bRatioList_s[i]
####            bRatioMidTest_i = self.basicdata.bRatioMidTestList_s[i]
#            K_s = self.basicdata.K_List[i]
                ## START DENSITY FOR SESSION i
                if i == 0:
                    D = self.basicdata.D0.copy()
                    k_up1[0] = K_High0                    
                    k_up1[1] = K_Low0
                    ## GET B RATIO FOR HIGH AND MID SITES FOR SESSION 0
                    for j in range(2):
                        ## MID SITE DENSITY AND B (CARRYING CAPACITY) AND bRatio
                        D_mid = self.basicdata.D0[j + 4]
                        K_mid = K_Low0
                        bRatio_i[0, (j + 2)] = D_mid / K_mid
####                    bRatioMidTest_i[0, j] = D_mid
####                    bRatioMidTest_i[0, j] = D_mid / K_mid
                        ## HIGH SITE bRatio
                        D_High= self.basicdata.D0[j]
                        K_High = K_High0
                        bRatio_i[0, j] = D_High / K_High

#############
                else:
                    D = deepcopy(self.basicdata.listDen_s[(i - 1)]) ## CHECK DEEPCOPY
                    ## LAST K OF PREVIOUS SESSION - WILL BE FIRST K IN SESS I
                    k_up1[0] = self.basicdata.K_List[i - 1][-1, 0]                    
                    k_up1[1] = self.basicdata.K_List[i - 1][-1, 1]
   
                bRatio_Up1 = np.zeros(4)
####            bRatioMidTest_Up1 = np.zeros(2)
                mArray_i = self.basicdata.listM_s[i]

                ## NUMBA LOOP THROUGH WEEKS AND SITES
                (mArray_i, bRatio_i, K_s, 
                    bRatio_Up1, week) = rickerWeeksSites(i, 
                    self.basicdata.nDenSites, D, 
                    self.basicdata.diffWeeks,
                    self.basicdata.r, mArray_i, bRatio_i, 
                    self.basicdata.K_List[i], 
                    self.basicdata.nDenSessions, bRatio_Up1, self.basicdata.immWeek, 
                    week, self.immModel, k_up1, self.params.minBRatio)

                ## PUT THE B RATIO AND B MID TEST VALUES IN NEXT UP SESSION FOR TIME 0
                if i < (self.basicdata.nDenSessions - 1):
                    for j in range(self.basicdata.nDenSites):
                        if j < 2:
                            self.basicdata.bRatioList_s[(i + 1)][0, j] = bRatio_Up1[j]
                        elif j > 3:
                            self.basicdata.bRatioList_s[(i + 1)][0, (j - 2)] = (
                                bRatio_Up1[j-2])
                llik_s += stats.multivariate_normal.logpdf(self.basicdata.listLnDen_s[i], 
                    mArray_i, self.basicdata.gammCovMat)
                llik += stats.multivariate_normal.logpdf(self.basicdata.listLnDensity[i], 
                    self.basicdata.listM[i], self.basicdata.gammCovMat)
            ## SIGMA LIKELIHOOD - BECAUSE DEPENDS ON DENSITY
            self.basicdata.Den_s[self.basicdata.missIndx] = missDen_s
            self.basicdata.lnDen_s[self.basicdata.missIndx] = lnMissDen_s 
            ## PREDICTED SIGMA FROM COVARIATES AND DENSITY
####            mu_s = (np.exp(np.dot(self.basicdata.betaX, self.basicdata.beta)) /
####                np.sqrt(self.basicdata.Den_s / 100.0))
####            sigLLik_s = np.sum(stats.norm.logpdf(self.basicdata.lnSigma, 
####                np.log(mu_s), np.sqrt(self.basicdata.vSig)))
####            sigLLik = np.sum(stats.norm.logpdf(self.basicdata.lnSigma, 
####                self.basicdata.lnMu, np.sqrt(self.basicdata.vSig)))
#            ## PRIORS
#            prior_s = np.sum(stats.norm.logpdf(lnMissDen_s, self.params.missDenPrior[0], 
#                self.params.missDenPrior[1]))
#            prior = np.sum(stats.norm.logpdf(np.log(self.basicdata.missingDen), 
#                self.params.missDenPrior[0], self.params.missDenPrior[1]))
            ## CALCULATE IMPORTANCE RATIO
            pnow = llik + qNewGivenNow    ##prior
            pnew = llik_s + qNowGivenNew    ## prior_s
####            pnow = llik + sigLLik + qNewGivenNow    ##prior
####            pnew = llik_s + sigLLik_s + qNowGivenNew    ## prior_s
            pdiff = pnew - pnow
            if pdiff > 1.0:
                rValue = 1.0
                zValue = 0.0
            elif pdiff < -12.0:
                rValue = 0.0
                zValue = 1.0
            else:
                rValue = np.exp(pdiff)        # calc importance ratio
                zValue = np.random.uniform(0.0, 1.0, size = None)
            if (rValue > zValue):
                self.basicdata.missingDen = missDen_s.copy()
                self.basicdata.listLnDensity = deepcopy(self.basicdata.listLnDen_s)
                self.basicdata.listDensity = deepcopy(self.basicdata.listDen_s)
                self.basicdata.listM = deepcopy(self.basicdata.listM_s)
####                self.basicdata.mu = mu_s.copy()
                self.basicdata.lnMu = np.log(self.basicdata.mu)
                self.basicdata.D = self.basicdata.Den_s.copy()
                self.basicdata.lnD = self.basicdata.lnDen_s.copy()
                self.basicdata.bRatioList = deepcopy(self.basicdata.bRatioList_s)




    def updateMissDensityCrossValid(self):
        """
        ## IF CROSS VALIDATE, UPDATE LATENT MISSING TEST DATUM
        """
#            missDen_s = self.basicdata.missingDen.copy()
        minDiff = self.basicdata.cvMissDen - self.basicdata.cvMissPriorLow 
        maxDiff = self.basicdata.cvMissPriorHigh - self.basicdata.cvMissDen
        if minDiff < self.params.searchMissDen:
            lowSearch = self.basicdata.cvMissPriorLow
            lowCount = minDiff
        else:
            lowSearch = self.basicdata.cvMissDen - self.params.searchMissDen
            lowCount = self.params.searchMissDen
        if maxDiff < self.params.searchMissDen:
            highSearch = self.basicdata.cvMissPriorHigh
            highCount = maxDiff
        else:
            highSearch = self.basicdata.cvMissDen + self.params.searchMissDen
            highCount = self.params.searchMissDen
        nowCount = lowCount + highCount
        qNewGivenNow = np.log(1.0 / nowCount)
        missDenCV_s = np.random.uniform(lowSearch, highSearch)
        ## GET PROPOSAL COUNT FOR M-H CORRECTION
        minDiff = missDenCV_s - self.basicdata.cvMissPriorLow
        maxDiff = self.basicdata.cvMissPriorHigh - missDenCV_s
        if minDiff < self.params.searchMissDen:
            lowNewCount = minDiff
        else:
            lowNewCount = self.params.searchMissDen
        if maxDiff < self.params.searchMissDen:
            highNewCount = maxDiff
        else:
            highNewCount = self.params.searchMissDen
        newCount = lowNewCount + highNewCount
        qNowGivenNew = np.log(1.0 / newCount)
        lnMissDenCV_s = np.log(missDenCV_s)
        K_High0 = self.basicdata.maxK[0] 
        K_Low0 = self.basicdata.maxK[1]
        self.basicdata.listLnDen_s[self.basicdata.sessCV][self.basicdata.siteCV] = (
            lnMissDenCV_s)
        self.basicdata.listDen_s[self.basicdata.sessCV][self.basicdata.siteCV] = (
            missDenCV_s)
        ## CALC LIKELIHOODS
        llik_s = 0.0
        llik = 0.0
        sigLLik_s = 0.0
        sigLLik = 0.0
        k_up1 = np.zeros(2)                 ## K FOR FIRST WEEK OF NEXT SESS
        week = self.basicdata.weekMastList[self.basicdata.sessCV][0]
#        ## LOOP THRU SESSIONS
#        for i in range(self.basicdata.nDenSessions):
#            bRatio_i = self.basicdata.bRatioList_s[i]

        i = self.basicdata.sessCV

#        if self.immModel:
        bRatio_i = self.basicdata.bRatioList_s[self.basicdata.sessCV]
        ## START DENSITY FOR SESSION i
        if self.basicdata.sessCV == 0:
            D = self.basicdata.D0.copy()
            k_up1[0] = K_High0                    
            k_up1[1] = K_Low0       
            if self.immModel:
                ## GET B RATIO FOR HIGH AND MID SITES FOR SESSION 0
                for j in range(2):
                    ## MID SITE DENSITY AND B (CARRYING CAPACITY) AND bRatio
                    D_mid = self.basicdata.D0[j + 4]
                    K_mid = K_Low0
                    bRatio_i[0, (j + 2)] = D_mid / K_mid
                    ## HIGH SITE bRatio
                    D_High= self.basicdata.D0[j]
                    K_High = K_High0
                    bRatio_i[0, j] = D_High / K_High

#############
        else:
            D = deepcopy(self.basicdata.listDensity[(i - 1)]) ## CHECK DEEPCOPY
            ## LAST K OF PREVIOUS SESSION - WILL BE FIRST K IN SESS I
            k_up1[0] = self.basicdata.K_List[i - 1][-1, 0]                    
            k_up1[1] = self.basicdata.K_List[i - 1][-1, 1]
        bRatio_Up1 = np.zeros(4)
        mArray_i = self.basicdata.listM_s[i]

        ## NUMBA LOOP THROUGH WEEKS AND SITES
        (mArray_i, bRatio_i, K_s, 
            bRatio_Up1, week) = rickerWeeksSites(i, 
            self.basicdata.nDenSites, D, 
            self.basicdata.diffWeeks,
            self.basicdata.r, mArray_i, bRatio_i, 
            self.basicdata.K_List[i], 
            self.basicdata.nDenSessions, bRatio_Up1, self.basicdata.immWeek, 
            week, self.immModel, k_up1, self.params.minBRatio)

        ## PUT THE B RATIO AND B MID TEST VALUES IN NEXT UP SESSION FOR TIME 0
        if (i < (self.basicdata.nDenSessions - 1)) & self.immModel:
            for j in range(self.basicdata.nDenSites):
                if j < 2:
                    self.basicdata.bRatioList_s[(i + 1)][0, j] = bRatio_Up1[j]
                elif j > 3:
                    self.basicdata.bRatioList_s[(i + 1)][0, (j - 2)] = (
                        bRatio_Up1[j-2])
        llik_s += stats.multivariate_normal.logpdf(self.basicdata.listLnDen_s[i], 
            mArray_i, self.basicdata.gammCovMat)
        llik += stats.multivariate_normal.logpdf(self.basicdata.listLnDensity[i], 
            self.basicdata.listM[i], self.basicdata.gammCovMat)
####            ## SIGMA LIKELIHOOD - BECAUSE DEPENDS ON DENSITY
        self.basicdata.Den_s[self.basicdata.den1D_IndxCV] = missDenCV_s
        self.basicdata.lnDen_s[self.basicdata.den1D_IndxCV] = lnMissDenCV_s 
            ## PREDICTED SIGMA FROM COVARIATES AND DENSITY
####            mu_s = (np.exp(np.dot(self.basicdata.betaX, self.basicdata.beta)) /
####                np.sqrt(self.basicdata.Den_s / 100.0))
####            sigLLik_s = np.sum(stats.norm.logpdf(self.basicdata.lnSigma, 
####                np.log(mu_s), np.sqrt(self.basicdata.vSig)))
####            sigLLik = np.sum(stats.norm.logpdf(self.basicdata.lnSigma, 
####                self.basicdata.lnMu, np.sqrt(self.basicdata.vSig)))
#            ## PRIORS
#            prior_s = np.sum(stats.norm.logpdf(lnMissDen_s, self.params.missDenPrior[0], 
#                self.params.missDenPrior[1]))
#            prior = np.sum(stats.norm.logpdf(np.log(self.basicdata.missingDen), 
#                self.params.missDenPrior[0], self.params.missDenPrior[1]))
            ## CALCULATE IMPORTANCE RATIO
        pnow = llik + qNewGivenNow    ##prior
        pnew = llik_s + qNowGivenNew    ## prior_s
####            pnow = llik + sigLLik + qNewGivenNow    ##prior
####            pnew = llik_s + sigLLik_s + qNowGivenNew    ## prior_s
        pdiff = pnew - pnow
        if pdiff > 1.0:
            rValue = 1.0
            zValue = 0.0
        elif pdiff < -12.0:
            rValue = 0.0
            zValue = 1.0
        else:
            rValue = np.exp(pdiff)        # calc importance ratio
            zValue = np.random.uniform(0.0, 1.0, size = None)
        if (rValue > zValue):
            self.basicdata.cvMissDen = missDenCV_s
            self.basicdata.listLnDensity = deepcopy(self.basicdata.listLnDen_s)
            self.basicdata.listDensity = deepcopy(self.basicdata.listDen_s)
            self.basicdata.listM = deepcopy(self.basicdata.listM_s)
###                self.basicdata.mu = mu_s.copy()
            self.basicdata.lnMu = np.log(self.basicdata.mu)
            self.basicdata.D = self.basicdata.Den_s.copy()
            self.basicdata.lnD = self.basicdata.lnDen_s.copy()
            self.basicdata.bRatioList = deepcopy(self.basicdata.bRatioList_s)








    def updateMissDensity_ContinuousPrior(self):
        missDen_s = np.exp(np.random.normal(np.log(self.basicdata.missingDen), 
            self.params.searchMissDen))
        missDen_s = np.where(missDen_s < 1.0, missDen_s +1.0, missDen_s)

        lnMissDen_s = np.log(missDen_s)
        K_High0 = self.basicdata.maxK[0] 
        K_Low0 = self.basicdata.maxK[1]


#        print('proposed den_s', np.round(lnMissDen_s,3))
        ## LOOP THRU ALL MISSING DATA TO POPULATE PROPOSED DENSITY DATA
        for i in range(self.params.nMissingDen):
            sess_i = self.basicdata.listSessSiteMissDen[i][0]
            site_i = self.basicdata.listSessSiteMissDen[i][1]
            self.basicdata.listLnDen_s[sess_i][site_i] = lnMissDen_s[i]
            self.basicdata.listDen_s[sess_i][site_i] = missDen_s[i]
        ## CALC LIKELIHOODS
        llik_s = 0.0
        llik = 0.0
        sigLLik_s = 0.0
        sigLLik = 0.0
        week = 0
        ## LOOP THRU SESSIONS
        for i in range(self.basicdata.nDenSessions):
            bRatio_i = self.basicdata.bRatioList_s[i]
####            bRatioMidTest_i = self.basicdata.bRatioMidTestList_s[i]
#            K_s = self.basicdata.K_List[i]
            ## START DENSITY FOR SESSION i
            if i == 0:
                D = self.basicdata.D0.copy()
                ## GET B RATIO FOR HIGH AND MID SITES FOR SESSION 0
                for j in range(2):
                    ## MID SITE DENSITY AND B (CARRYING CAPACITY) AND bRatio
                    D_mid = self.basicdata.D0[j + 4]
                    K_mid = K_Low0
                    bRatio_i[0, (j + 2)] = D_mid / K_mid * np.log(D_mid)
####                    bRatioMidTest_i[0, j] = D_mid
####                    bRatioMidTest_i[0, j] = D_mid / K_mid
                    ## HIGH SITE bRatio
                    D_High= self.basicdata.D0[j]
                    K_High = K_High0
                    bRatio_i[0, j] = D_High / K_High

#############
            else:
                D = deepcopy(self.basicdata.listDen_s[(i - 1)]) ## CHECK DEEPCOPY
               
            bRatio_Up1 = np.zeros(4)
####            bRatioMidTest_Up1 = np.zeros(2)
            mArray_i = self.basicdata.listM_s[i]

            ## NUMBA LOOP THROUGH WEEKS AND SITES
            (mArray_i, bRatio_i, K_s, 
                bRatio_Up1, week) = rickerWeeksSites(i, 
                self.basicdata.nDenSites, D, 
                self.basicdata.diffWeeks,
                self.basicdata.r, mArray_i, bRatio_i, 
                self.basicdata.K_List[i], 
                self.basicdata.nDenSessions, bRatio_Up1, self.basicdata.immWeek, 
                week, self.immModel)

            ## PUT THE B RATIO AND B MID TEST VALUES IN NEXT UP SESSION FOR TIME 0
            if i < (self.basicdata.nDenSessions - 1):
                for j in range(self.basicdata.nDenSites):
                    if j < 2:
                        self.basicdata.bRatioList_s[(i + 1)][0, j] = bRatio_Up1[j]
                    elif j > 3:
                        self.basicdata.bRatioList_s[(i + 1)][0, (j - 2)] = (
                            bRatio_Up1[j-2])
#                        self.basicdata.bRatioMidTestList_s[(i + 1)][0, (j-4)] = (
#                            bRatioMidTest_Up1[j-4])

            llik_s += stats.multivariate_normal.logpdf(self.basicdata.listLnDen_s[i], 
                mArray_i, self.basicdata.gammCovMat)
            llik += stats.multivariate_normal.logpdf(self.basicdata.listLnDensity[i], 
                self.basicdata.listM[i], self.basicdata.gammCovMat)
        ## SIGMA LIKELIHOOD - BECAUSE DEPENDS ON DENSITY
        self.basicdata.Den_s[self.basicdata.missIndx] = missDen_s
        self.basicdata.lnDen_s[self.basicdata.missIndx] = lnMissDen_s 
        ## PREDICTED SIGMA FROM COVARIATES AND DENSITY
        mu_s = (np.exp(np.dot(self.basicdata.betaX, self.basicdata.beta)) /
                np.sqrt(self.basicdata.Den_s / 100.0))
        sigLLik_s = np.sum(stats.norm.logpdf(self.basicdata.lnSigma, 
            np.log(mu_s), np.sqrt(self.basicdata.vSig)))
        sigLLik = np.sum(stats.norm.logpdf(self.basicdata.lnSigma, 
            self.basicdata.lnMu, np.sqrt(self.basicdata.vSig)))
        ## PRIORS
        prior_s = np.sum(stats.norm.logpdf(lnMissDen_s, self.params.missDenPrior[0], 
            self.params.missDenPrior[1]))
        prior = np.sum(stats.norm.logpdf(np.log(self.basicdata.missingDen), 
            self.params.missDenPrior[0], self.params.missDenPrior[1]))
        ## CALCULATE IMPORTANCE RATIO
        pnow = llik + sigLLik + prior
        pnew = llik_s + sigLLik_s + prior_s
        pdiff = pnew - pnow
        if pdiff > 1.0:
            rValue = 1.0
            zValue = 0.0
        elif pdiff < -12.0:
            rValue = 0.0
            zValue = 1.0
        else:
            rValue = np.exp(pdiff)        # calc importance ratio
            zValue = np.random.uniform(0.0, 1.0, size = None)
        if (rValue > zValue):
            self.basicdata.missingDen = missDen_s.copy()
            self.basicdata.listLnDensity = deepcopy(self.basicdata.listLnDen_s)
            self.basicdata.listDensity = deepcopy(self.basicdata.listDen_s)
            self.basicdata.listM = deepcopy(self.basicdata.listM_s)
            self.basicdata.mu = mu_s.copy()
            self.basicdata.lnMu = np.log(self.basicdata.mu)
            self.basicdata.D = self.basicdata.Den_s.copy()
            self.basicdata.lnD = self.basicdata.lnDen_s.copy()
            self.basicdata.bRatioList = deepcopy(self.basicdata.bRatioList_s)
####            self.basicdata.bRatioMidTestList = (
####                deepcopy(self.basicdata.bRatioMidTestList_s))






    def updateMissSigma(self):
        ## RANDOM PROPOSALS OF MISSING SIGMAS
        missSig_s = np.exp(np.random.normal(np.log(self.basicdata.missingSig), 
            self.params.searchMissSig))
        lnMissSig_s = np.log(missSig_s)
        ## POPULATE LONG ARRAYS OF SIGMA WITH PROPOSED MISSING VALUES
        self.basicdata.sigma_s[self.basicdata.maskMissSig] = missSig_s
        self.basicdata.lnSigma_s[self.basicdata.maskMissSig] = lnMissSig_s 
        ## CALC LIKELIHOODS
        sigLLik_s = np.sum(stats.norm.logpdf(self.basicdata.lnSigma_s, 
            self.basicdata.lnMu, np.sqrt(self.basicdata.vSig)))
        sigLLik = np.sum(stats.norm.logpdf(self.basicdata.lnSigma, 
            self.basicdata.lnMu, np.sqrt(self.basicdata.vSig)))
        ## PRIORS
        prior_s = np.sum(stats.norm.logpdf(lnMissSig_s, self.params.missSigPrior[0], 
            self.params.missSigPrior[1]))
        prior = np.sum(stats.norm.logpdf(np.log(self.basicdata.missingSig), 
            self.params.missSigPrior[0], self.params.missSigPrior[1]))
        ## CALCULATE IMPORTANCE RATIO
        pnow = sigLLik + prior
        pnew = sigLLik_s + prior_s
        pdiff = pnew - pnow
        if pdiff > 1.0:
            rValue = 1.0
            zValue = 0.0
        elif pdiff < -12.0:
            rValue = 0.0
            zValue = 1.0
        else:
            rValue = np.exp(pdiff)        # calc importance ratio
            zValue = np.random.uniform(0.0, 1.0, size = None)
        ## UPDATE PARAMETERS
        if (rValue > zValue):
            self.basicdata.missingSig = missSig_s.copy()
            self.basicdata.sigma = self.basicdata.sigma_s.copy()
            self.basicdata.lnSigma = self.basicdata.lnSigma_s.copy()




    def updateD0(self):
        D0_s = np.zeros(self.params.nD0)

        ## GET D0_S PROPOSAL FROM UNIFORM DISTRIBUTION AND M-H CORRECTION
        qNewGivenNow = 0.0
        qNowGivenNew = 0.0
        for elev in range(self.params.nD0):
#            if para < 2:
#                elev = 0
#            else:
#                elev = 1
            minDiff = self.basicdata.D0[elev] - self.params.D0Prior[elev, 0] 
            maxDiff = self.params.D0Prior[elev, 1] - self.basicdata.D0[elev]
            if minDiff < self.params.searchD0[elev]:
                lowSearch = self.params.D0Prior[elev, 0]
                lowCount = minDiff
            else:
                lowSearch = self.basicdata.D0[elev] - self.params.searchD0[elev]
                lowCount = self.params.searchD0[elev]
            if maxDiff < self.params.searchD0[elev]:
                highSearch = self.params.D0Prior[elev, 1]
                highCount = maxDiff
            else:
                highSearch = self.basicdata.D0[elev] + self.params.searchD0[elev]
                highCount = self.params.searchD0[elev]
            nowCount = lowCount + highCount
            qNewGivenNow += np.log(1.0 / nowCount)
            D0_s[elev] = np.random.uniform(lowSearch, highSearch)
            ## GET PROPOSAL COUNT FOR M-H CORRECTION
            minDiff = D0_s[elev] - self.params.D0Prior[elev, 0]
            maxDiff = self.params.D0Prior[elev, 1] - D0_s[elev]
            if minDiff < self.params.searchD0[elev]:
                lowNewCount = minDiff
            else:
                lowNewCount = self.params.searchD0[elev]
            if maxDiff < self.params.searchD0[elev]:
                highNewCount = maxDiff
            else:
                highNewCount = self.params.searchD0[elev]
            newCount = lowNewCount + highNewCount
            qNowGivenNew += np.log(1.0 / newCount)


        ## END GETTING PROPOSAL FROM UNIFORM
#        D0_s = np.exp(np.random.normal(np.log(self.basicdata.D0), 
#            self.params.searchD0))
        D = D0_s.copy()
        lnD0_s = np.log(D0_s)
        K_High0 = self.basicdata.maxK[0] 
        K_Low0 = self.basicdata.maxK[1]
        k_up1 = np.zeros(2)
        k_up1[0] = K_High0                    
        k_up1[1] = K_Low0    

        ## EMPTY ARRAY OF MU_S[0] AT SESSION 0 TO POPULATE
        bRatio_i = self.basicdata.bRatioList_s[0]

        mArray_i = self.basicdata.listM_s[0]
        ## GET B RATIO FOR HIGH AND MID SITES FOR SESSION 0
        for j in range(2):
            ## MID SITE DENSITY AND B (CARRYING CAPACITY) AND bRatio
            D_mid = D0_s[j + 4]
            K_mid = K_Low0
            bRatio_i[0, (j + 2)] = D_mid / K_mid 
####            bRatioMidTest_i[0, j] = D_mid
####            bRatioMidTest_i[0, j] = D_mid / K_mid
            ## HIGH SITE bRatio
            D_High= D0_s[j]
            K_High = K_High0
            bRatio_i[0, j] = D_High / K_High
        
        bRatio_Up1 = np.zeros(4)
####        bRatioMidTest_Up1 = np.zeros(2)
        week = 0
        ## NUMBA LOOP THROUGH WEEKS AND SITES
        (mArray_i, bRatio_i, K_s, 
            bRatio_Up1, week) = rickerWeeksSites(0, 
            self.basicdata.nDenSites, D, 
            self.basicdata.diffWeeks,
            self.basicdata.r, mArray_i, bRatio_i, 
            self.basicdata.K_List[0], 
            self.basicdata.nDenSessions, bRatio_Up1, self.basicdata.immWeek, 
            week, self.immModel, k_up1, self.params.minBRatio)
        ## CALCULATE LIKELIHOODS
        llik_s = stats.multivariate_normal.logpdf(self.basicdata.listLnDensity[0], 
            mArray_i, self.basicdata.gammCovMat)
        llik = stats.multivariate_normal.logpdf(self.basicdata.listLnDensity[0], 
            self.basicdata.listM[0], self.basicdata.gammCovMat)
#        prior_s = np.sum(stats.norm.logpdf(lnD0_s, self.params.lnD0MeanPrior, 
#            self.params.lnD0SDPrior))
#        prior = np.sum(stats.norm.logpdf(np.log(self.basicdata.D0), 
#            self.params.lnD0MeanPrior, self.params.lnD0SDPrior))
        ## CALCULATE IMPORTANCE RATIO
        pnow = llik + qNewGivenNow  #   prior
        pnew = llik_s + qNowGivenNew        #prior_s
        pdiff = pnew - pnow
        if pdiff > 1.0:
            rValue = 1.0
            zValue = 0.0
        elif pdiff < -12.0:
            rValue = 0.0
            zValue = 1.0
        else:
            rValue = np.exp(pdiff)        # calc importance ratio
            zValue = np.random.uniform(0.0, 1.0, size = None)
        if (rValue > zValue):
            self.basicdata.D0 = D0_s.copy()
            self.basicdata.lnD0 = lnD0_s.copy()
            self.basicdata.listM[0] = mArray_i.copy()
###            self.basicdata.bRatioList[0] = bRatio_i.copy()




    def betaUpdate(self):
        """
        ## UPDATE BETAS FOR SIGMA MODEL
        """
        xTranspose = np.transpose(self.basicdata.betaX)
        xCrossProd = np.dot(xTranspose, self.basicdata.betaX)
        sinv = 1.0/self.basicdata.vSig
        sx = np.multiply(xCrossProd, sinv)
        lnSigma_Density = np.log(self.basicdata.sigma * np.sqrt(self.basicdata.D / 100.0))
        xyCrossProd = np.dot(xTranspose, lnSigma_Density)
        sy = np.multiply(xyCrossProd, sinv)
        bigv = np.linalg.inv(sx + self.params.vInvertBeta)
        smallv = sy + self.params.ppart
        meanVariate = np.dot(bigv, smallv)
        b = np.random.multivariate_normal(meanVariate, bigv)
        self.basicdata.beta =  np.transpose(b)
        self.basicdata.mu = (np.exp(np.dot(self.basicdata.betaX, self.basicdata.beta)) /
                np.sqrt(self.basicdata.D / 100.0))
        self.basicdata.lnMu = np.log(self.basicdata.mu)





    def betaUpdateXXX(self):
        """
        ## UPDATE BETAS FOR SIGMA MODEL
        """
        xTranspose = np.transpose(self.basicdata.betaX)
        xCrossProd = np.dot(xTranspose, self.basicdata.betaX)
        sinv = 1.0/self.basicdata.vSig
        sx = np.multiply(xCrossProd, sinv)
        lnSigma_Density = self.basicdata.lnSigma * np.sqrt(self.basicdata.D / 100.0)
        xyCrossProd = np.dot(xTranspose, lnSigma_Density)
        sy = np.multiply(xyCrossProd, sinv)
        bigv = np.linalg.inv(sx + self.params.vInvertBeta)
        smallv = sy + self.params.ppart
        meanVariate = np.dot(bigv, smallv)
        b = np.random.multivariate_normal(meanVariate, bigv)
        self.basicdata.beta =  np.transpose(b)
        self.basicdata.mu = (np.exp(np.dot(self.basicdata.betaX, self.basicdata.beta)) /
                np.sqrt(self.basicdata.D / 100.0))
        self.basicdata.lnMu = np.log(self.basicdata.mu)





    def vSigUpdate(self):
        predDiff = self.basicdata.lnSigma - self.basicdata.lnMu
        sx = np.sum(predDiff**2.0)
        u1 = self.params.vSigPrior[0] + np.multiply(.5, self.basicdata.nDensity)
        u2 = self.params.vSigPrior[1] + np.multiply(.5, sx)               # rate parameter    
        isg = np.random.gamma(u1, 1./u2, size = None)            # formulation using shape 
        self.basicdata.vSig = 1.0/isg





    def r_update(self):
        """
        METROPOLIS SAMPLER FOR r GROWTH RATE PARAMETER
        """

#        print('########################################################## rupdate')
        if not self.immModel:
            nPara = 1
        else:
            nPara = 2
        for para in range(nPara):
            r_s = self.basicdata.r.copy()
            r_s[para] = np.exp(np.random.normal(np.log(self.basicdata.r[para]), 
                self.params.searchR[para]))
            llik_s = 0.0
            llik = 0.0
            k_up1 = np.zeros(2)
            week = 0
            for i in range(self.basicdata.nDenSessions):
                bRatio_i = self.basicdata.bRatioList_s[i]
            ## START DENSITY FOR SESSION I
                if i == 0:
                    D = self.basicdata.D0.copy()
                    k_up1[0] = self.basicdata.maxK[0]                    
                    k_up1[1] = self.basicdata.maxK[1]
                    ## GET B RATIO FOR HIGH AND MID SITES FOR SESSION 0
                    for j in range(2):
                        ## MID SITE DENSITY AND B (CARRYING CAPACITY) AND bRatio
                        D_mid = self.basicdata.D0[j + 4]
                        K_mid = self.basicdata.maxK[1]
                        bRatio_i[0, (j + 2)] = D_mid / K_mid
                        ## HIGH SITE bRatio
                        D_High= self.basicdata.D0[j]
                        K_High = self.basicdata.maxK[0]
                        bRatio_i[0, j] = D_High / K_High
#############
                else:
                    D = deepcopy(self.basicdata.listDensity[(i - 1)]) ## CHECK DEEPCOPY
                    ## LAST K OF PREVIOUS SESSION - WILL BE FIRST K IN SESS I
                    k_up1[0] = self.basicdata.K_List[i - 1][-1, 0]                    
                    k_up1[1] = self.basicdata.K_List[i - 1][-1, 1]  

                ## EMPTY ARRAYS TO POPULATE
                bRatio_Up1 = np.zeros(4)
####                bRatioMidTest_Up1 = np.zeros(2)
                mArray_i = self.basicdata.listM_s[i]

#                print('Before sess', i, 'para', para, 'mArr_s', np.round(mArray_i, 4))
#                print('Before sess', i, 'para', para, 
#                    'listM_s', np.round(self.basicdata.listM_s[i],4))
#                print('Before sess', i, 'para', para, 
#                    'listM', np.round(self.basicdata.listM[i],4))
#                print('Before week', week, 'imm Week', self.basicdata.immWeek)

                ## NUMBA LOOP THROUGH WEEKS AND SITES
                (mArray_i, bRatio_i, K_s, 
                    bRatio_Up1, week) = rickerWeeksSites(i, 
                    self.basicdata.nDenSites, D, 
                    self.basicdata.diffWeeks,
                    r_s, mArray_i, bRatio_i, 
                    self.basicdata.K_List[i], 
                    self.basicdata.nDenSessions, bRatio_Up1, self.basicdata.immWeek, 
                    week, self.immModel, k_up1, self.params.minBRatio)

#                self.basicdata.listM_s[i] = deepcopy(mArray_i)


#                print('After sess', i, 'para', para, 'mArr_s', np.round(mArray_i, 4))
#                print('After sess', i, 'para', para, 
#                    'listM_s', np.round(self.basicdata.listM_s[i],4))
#                print('After sess', i, 'para', para, 
#                    'listM', np.round(self.basicdata.listM[i],4))
#                print('After week', week)




                ## PUT THE B RATIO AND B MID TEST VALUES IN NEXT UP SESSION FOR TIME 0
                if i < (self.basicdata.nDenSessions - 1):
                    for j in range(self.basicdata.nDenSites):
                        if j < 2:
                            self.basicdata.bRatioList_s[(i + 1)][0, j] = bRatio_Up1[j]
                        elif j > 3:
                            self.basicdata.bRatioList_s[(i + 1)][0, (j - 2)] = (
                                bRatio_Up1[j-2])
####                            self.basicdata.bRatioMidTestList_s[(i + 1)][0, (j-4)] = (
####                                    bRatioMidTest_Up1[j-4])
                ## ADD TO LIKELIHOODS
                llik_s += stats.multivariate_normal.logpdf(self.basicdata.listLnDensity[i], 
                    mArray_i, self.basicdata.gammCovMat)
                llik += stats.multivariate_normal.logpdf(self.basicdata.listLnDensity[i], 
                    self.basicdata.listM[i], self.basicdata.gammCovMat)

#                diffDen = np.exp(mArray_i) - np.exp(self.basicdata.listLnDensity[i])
#                maxMask = diffDen == np.max(diffDen)
#                if diffDen[maxMask] > (1.2 * np.exp(self.basicdata.listLnDensity[i][maxMask])):
#                    print('high Den in r_up', 'sess', i, 'site', np.arange(6)[maxMask],
#                        'llik', llik, 'llik_s', llik_s,
#                        'mArr', np.exp(mArray_i[maxMask]), 
#                        'D', np.exp(self.basicdata.listLnDensity[i][maxMask]))


            prior_s = (stats.norm.logpdf(np.log(r_s[para]), self.params.priorR[para,0], 
                self.params.priorR[para,1]))  
            prior = (stats.norm.logpdf(np.log(self.basicdata.r[para]), 
                self.params.priorR[para,0], self.params.priorR[para,1])) 
            pnow = llik + prior
            pnew = llik_s + prior_s

#            print('para', para, 'r', np.round(self.basicdata.r[para],6), 
#                'r_s', np.round(r_s[para], 6), 
#                'llik', np.round(llik, 3), 'llik_s', np.round(llik_s, 3), 
#                'pr', np.round(prior, 3), 
#                'pr_s', np.round(prior_s, 3),
#                'pnow', np.round(pnow - pnew, 3))

            pdiff = pnew - pnow
            if pdiff > 1.0:
                rValue = 1.0
                zValue = 0.0
            elif pdiff < -12.0:
                rValue = 0.0
                zValue = 1.0
            else:
                rValue = np.exp(pdiff)        # calc importance ratio
                zValue = np.random.uniform(0.0, 1.0, size = None)
            if (rValue > zValue):
                self.basicdata.r = r_s.copy()
                self.basicdata.listM = deepcopy(self.basicdata.listM_s)
###                self.basicdata.bRatioList = deepcopy(self.basicdata.bRatioList_s)

#            print('r > z', rValue > zValue)


    def tm_updateBlock(self):
        """
        METROPOLIS SAMPLER FOR PARAMETERS FOR TIME SINCE MAST (GAMMA PDF)
        """
        ## UPDATE HIGH AND MID/LOW PARAMETERS SEPARATELY
        for h in range(2):
            for pcc in range(3):
                tm_s = self.basicdata.TM.copy()
                tm_s[h, pcc] = np.exp(np.random.normal(np.log(self.basicdata.TM[h, pcc]), 
                    self.params.searchTM[pcc]))
                ## SET UP INITIAL B RATIOS ARRAYS IN LIST
                tmGamPDF_High0_s = genGammaPDF(.5, tm_s[0, 0], tm_s[0, 1], tm_s[0, 2])
                tmGamPDF_Low0_s = genGammaPDF(.5, tm_s[1, 0], tm_s[1, 1], tm_s[1, 2])
                tmPred_High0_s = (self.basicdata.gamm[0] + self.basicdata.gamm[1] * 
                    tmGamPDF_High0_s)
                tmPred_Low0_s = (self.basicdata.gamm[0] + self.basicdata.gamm[2] * 
                    tmGamPDF_Low0_s)
                llik_s = 0.0
                llik = 0.0
                for i in range(self.basicdata.nDenSessions):
                    ## TIME MAST GAMMA PDF
                    self.basicdata.tmGamPDF_HighList_s[i] = genGammaPDF(
                        self.basicdata.weekMastList[i], tm_s[0, 0], tm_s[0, 1], tm_s[0, 2])
                    self.basicdata.tmGamPDF_LowList_s[i] = genGammaPDF(
                        self.basicdata.weekMastList[i], tm_s[1, 0], tm_s[1, 1], tm_s[1, 2])
                    ## PREDICTION OF TIME SINCE MAST EFFECT
                    self.basicdata.tmPred_HighList_s[i] = (self.basicdata.gamm[0] + 
                        self.basicdata.gamm[1] * self.basicdata.tmGamPDF_HighList_s[i])
                    self.basicdata.tmPred_LowList_s[i] =  (self.basicdata.gamm[0] + 
                        self.basicdata.gamm[2] * self.basicdata.tmGamPDF_LowList_s[i])
                    ## ARRAY TO POPULATE FOR B RATIO AND DENSITY DEPENDENCE FOR I
                    bRatio_i = self.basicdata.bRatioList_s[i]
                    bRatioMidTest_i = self.basicdata.bRatioMidTestList_s[i]
                    B_s = self.basicdata.listB_s[i]    
                    ## START DENSITY FOR SESSION I
                    if i == 0:
                        D = self.basicdata.D0.copy()
                        ## GET B RATIO FOR HIGH AND MID SITES FOR SESSION 0
                        for j in range(2):
                            ## MID SITE DENSITY AND B (CARRYING CAPACITY) AND bRatio
                            D_mid = self.basicdata.D0[j + 4]
                            b_mid = np.exp(tmPred_Low0_s)
                            bRatio_i[0, (j + 2)] = D_mid / b_mid * np.log(D_mid)
                            bRatioMidTest_i[0, j] = D_mid / b_mid
                            ## HIGH SITE bRatio
                            D_High= self.basicdata.D0[j]
                            b_High = np.exp(tmPred_High0_s)
                            bRatio_i[0, j] = D_High / b_High
                    else:
                        D = deepcopy(self.basicdata.listDensity[(i - 1)]) ## CHECK DEEPCOPY
                                
                    bRatio_Up1 = np.zeros(4)
                    bRatioMidTest_Up1 = np.zeros(2)
                    mArray_i = self.basicdata.listM_s[i]
                    ## NUMBA LOOP THROUGH WEEKS AND SITES
                    (mArray_i, bRatio_i, bRatioMidTest_i, B_s, 
                        bRatio_Up1, bRatioMidTest_Up1) = rickerWeeksSites(i, 
                        self.basicdata.nDenSites, D, 
                        self.basicdata.diffWeeks,
                        self.basicdata.r, mArray_i, self.basicdata.tmPred_HighList_s[i], 
                        self.basicdata.tmPred_LowList_s[i], bRatio_i,
                        bRatioMidTest_i, self.basicdata.bRatioThreshold, B_s, 
                        self.basicdata.nDenSessions, bRatio_Up1, bRatioMidTest_Up1, 
                        fx = 'tm')

                    ###############################################
                    ##### BREAK OUT OF LOOP IF K IS TOO HIGH OR LOW
                    minB = np.min(B_s)
                    maxB = np.max(B_s)
                    if (minB < self.params.bMinMax[0]) | (maxB > self.params.bMinMax[1]):
                        break



                    ## PUT THE B RATIO AND B MID TEST VALUES IN NEXT UP SESSION FOR TIME 0
                    if i < (self.basicdata.nDenSessions - 1):
                        for j in range(self.basicdata.nDenSites):
                            if j < 2:
                                self.basicdata.bRatioList_s[(i + 1)][0, j] = bRatio_Up1[j]
                            elif j > 3:
                                self.basicdata.bRatioList_s[(i + 1)][0, (j - 2)] = (
                                    bRatio_Up1[j-2])
                                self.basicdata.bRatioMidTestList_s[(i + 1)][0, (j-4)] = (
                                    bRatioMidTest_Up1[j-4])
                    ## GET LIKELIHOODS
                    llik_s += stats.multivariate_normal.logpdf(self.basicdata.listLnDensity[i], 
                        mArray_i, self.basicdata.gammCovMat)
                    llik += stats.multivariate_normal.logpdf(self.basicdata.listLnDensity[i], 
                        self.basicdata.listM[i], self.basicdata.gammCovMat)

                ###############################################
                ##### CONTINUE LOOP IF K IS TOO HIGH OR LOW
                if (minB < self.params.bMinMax[0]) | (maxB > self.params.bMinMax[1]):
                    continue

                prior_s = stats.norm.logpdf(np.log(tm_s[h, pcc]), 
                    self.params.priorTM[h][pcc, 0], self.params.priorTM[h][pcc, 1])

                prior = np.sum(stats.norm.logpdf(np.log(self.basicdata.TM[h, pcc]), 
                    self.params.priorTM[h][pcc, 0], self.params.priorTM[h][pcc, 1]))

                pnow = llik + prior
                pnew = llik_s + prior_s
                pdiff = pnew - pnow
                if pdiff > 1.0:
                    rValue = 1.0
                    zValue = 0.0
                elif pdiff < -12.0:
                    rValue = 0.0
                    zValue = 1.0
                else:
                    rValue = np.exp(pdiff)        # calc importance ratio
                    zValue = np.random.uniform(0.0, 1.0, size = None)
                if (rValue > zValue):
                    self.basicdata.TM = tm_s.copy()
                    self.basicdata.listM = deepcopy(self.basicdata.listM_s)
                    ## PDF GEN GAMMA
                    self.basicdata.tmGamPDF_High0 = tmGamPDF_High0_s.copy()
                    self.basicdata.tmGamPDF_Low0 = tmGamPDF_Low0_s.copy()
                    self.basicdata.tmGamPDF_HighList = deepcopy(
                        self.basicdata.tmGamPDF_HighList_s)
                    self.basicdata.tmGamPDF_LowList = deepcopy(
                        self.basicdata.tmGamPDF_LowList_s)
                    ## PREDICTED TM EFFECT
                    self.basicdata.tmPred_High0 = tmPred_High0_s.copy()
                    self.basicdata.tmPred_Low0 = tmPred_Low0_s.copy()
                    self.basicdata.tmPred_HighList = deepcopy(self.basicdata.tmPred_HighList_s)
                    self.basicdata.tmPred_LowList = deepcopy(self.basicdata.tmPred_LowList_s)
                    ## B RATIO EFFECT ON DENSITY DEPENDENCE
                    self.basicdata.B = deepcopy(self.basicdata.listB_s)
                    self.basicdata.bRatioList = deepcopy(self.basicdata.bRatioList_s)
                    self.basicdata.bRatioMidTestList = (deepcopy(
                        self.basicdata.bRatioMidTestList_s))




    def bRatioThresUpdate(self):
        ## RANDOM PROPOSAL
        minDiff = self.basicdata.bRatioThreshold - self.params.bRatioThresPrior[0]
        maxDiff = self.params.bRatioThresPrior[1] - self.basicdata.bRatioThreshold 
        if minDiff < self.params.bRatioThresSearch:
            lowSearch = self.params.bRatioThresPrior[0]
            lowCount = minDiff
        else:
            lowSearch = self.basicdata.bRatioThreshold - self.params.bRatioThresSearch
            lowCount = self.params.bRatioThresSearch
        if maxDiff < self.params.bRatioThresSearch:
            highSearch = self.params.bRatioThresPrior[1]
            highCount = maxDiff
        else:
            highSearch = self.basicdata.bRatioThreshold + self.params.bRatioThresSearch
            highCount = self.params.bRatioThresSearch
        nowCount = lowCount + highCount
        qNewGivenNow = np.log(1.0 / nowCount)
        bRatioThreshold_s = np.random.uniform(lowSearch, highSearch)
        ## GET PROPOSAL COUNT FOR M-H CORRECTION
        minDiff = bRatioThreshold_s - self.params.bRatioThresPrior[0]
        maxDiff = self.params.bRatioThresPrior[1] - bRatioThreshold_s
        if minDiff < self.params.bRatioThresSearch:
            lowNewCount = minDiff
        else:
            lowNewCount = self.params.bRatioThresSearch
        if maxDiff < self.params.bRatioThresSearch:
            highNewCount = maxDiff
        else:
            highNewCount = self.params.bRatioThresSearch
        newCount = lowNewCount + highNewCount
        qNowGivenNew = np.log(1.0 / newCount)

        llik_s = 0.0
        llik = 0.0
        for i in range(self.basicdata.nDenSessions):
            bRatio_i = self.basicdata.bRatioList_s[i]
            bRatioMidTest_i = self.basicdata.bRatioMidTestList_s[i]
#            K_s = self.basicdata.K_List[i]  
            ## START DENSITY FOR SESSION I
            if i == 0:
                D = self.basicdata.D0.copy()
                ## GET B RATIO FOR HIGH AND MID SITES FOR SESSION 0
                for j in range(2):
                    ## MID SITE DENSITY AND B (CARRYING CAPACITY) AND bRatio
                    D_mid = self.basicdata.D0[j + 4]
                    K_mid = self.basicdata.maxK[1]
                    bRatio_i[0, (j + 2)] = D_mid / K_mid * np.log(D_mid)
                    bRatioMidTest_i[0, j] = D_mid 
                    ## HIGH SITE bRatio
                    D_High= self.basicdata.D0[j]
                    K_High = self.basicdata.maxK[0]
                    bRatio_i[0, j] = D_High / K_High

#############
            else:
                D = deepcopy(self.basicdata.listDensity[(i - 1)]) ## CHECK DEEPCOPY
            ## EMPTY ARRAYS TO POPULATE
            bRatio_Up1 = np.zeros(4)
            bRatioMidTest_Up1 = np.zeros(2)
            mArray_i = self.basicdata.listM_s[i]
            ## NUMBA LOOP THROUGH WEEKS AND SITES
            (mArray_i, bRatio_i, bRatioMidTest_i, K_s, 
                bRatio_Up1, bRatioMidTest_Up1) = rickerWeeksSites(i, 
                self.basicdata.nDenSites, D, 
                self.basicdata.diffWeeks,
                self.basicdata.r, mArray_i, bRatio_i, 
                bRatioMidTest_i, bRatioThreshold_s,
                self.basicdata.K_List[i], 
                self.basicdata.nDenSessions, bRatio_Up1, bRatioMidTest_Up1)
            ## PUT THE B RATIO AND B MID TEST VALUES IN NEXT UP SESSION FOR TIME 0
            if i < (self.basicdata.nDenSessions - 1):
                for j in range(self.basicdata.nDenSites):
                    if j < 2:
                        self.basicdata.bRatioList_s[(i + 1)][0, j] = bRatio_Up1[j]
                    elif j > 3:
                        self.basicdata.bRatioList_s[(i + 1)][0, (j - 2)] = (
                            bRatio_Up1[j-2])
                        self.basicdata.bRatioMidTestList_s[(i + 1)][0, (j-4)] = (
                                bRatioMidTest_Up1[j-4])
            ## ADD TO LIKELIHOODS
            llik_s += stats.multivariate_normal.logpdf(self.basicdata.listLnDensity[i], 
                mArray_i, self.basicdata.gammCovMat)
            llik += stats.multivariate_normal.logpdf(self.basicdata.listLnDensity[i], 
                self.basicdata.listM[i], self.basicdata.gammCovMat)
        prior_s = (stats.norm.logpdf(np.log(bRatioThreshold_s), self.params.bRatioThresPrior[0], 
            self.params.bRatioThresPrior[1]))
        prior = (stats.norm.logpdf(np.log(self.basicdata.bRatioThreshold), 
            self.params.bRatioThresPrior[0], self.params.bRatioThresPrior[1]))
        pnow = llik + qNewGivenNow
        pnew = llik_s + qNowGivenNew
        pdiff = pnew - pnow
        if pdiff > 1.0:
            rValue = 1.0
            zValue = 0.0
        elif pdiff < -12.0:
            rValue = 0.0
            zValue = 1.0
        else:
            rValue = np.exp(pdiff)        # calc importance ratio
            zValue = np.random.uniform(0.0, 1.0, size = None)
        if (rValue > zValue):
            self.basicdata.listM = deepcopy(self.basicdata.listM_s)
            self.basicdata.bRatioList = deepcopy(self.basicdata.bRatioList_s)
            self.basicdata.bRatioMidTestList=(deepcopy(self.basicdata.bRatioMidTestList_s))
            self.basicdata.bRatioThreshold = bRatioThreshold_s

#        print('br threshold', self.basicdata.bRatioThreshold)



    def bRatioThresUpdateXXX(self):
        bRatioThreshold_s = np.exp(np.random.normal(np.log(self.basicdata.bRatioThreshold),
            self.params.bRatioThresSearch)) 
        llik_s = 0.0
        llik = 0.0
        for i in range(self.basicdata.nDenSessions):
            bRatio_i = self.basicdata.bRatioList_s[i]
            bRatioMidTest_i = self.basicdata.bRatioMidTestList_s[i]
#            K_s = self.basicdata.K_List[i]  
            ## START DENSITY FOR SESSION I
            if i == 0:
                D = self.basicdata.D0.copy()
                ## GET B RATIO FOR HIGH AND MID SITES FOR SESSION 0
                for j in range(2):
                    ## MID SITE DENSITY AND B (CARRYING CAPACITY) AND bRatio
                    D_mid = self.basicdata.D0[j + 4]
                    K_mid = self.basicdata.maxK[1]
                    bRatio_i[0, (j + 2)] = D_mid / K_mid * np.log(D_mid)
                    bRatioMidTest_i[0, j] = D_mid / K_mid
                    ## HIGH SITE bRatio
                    D_High= self.basicdata.D0[j]
                    K_High = self.basicdata.maxK[0]
                    bRatio_i[0, j] = D_High / K_High

#############
            else:
                D = deepcopy(self.basicdata.listDensity[(i - 1)]) ## CHECK DEEPCOPY
            ## EMPTY ARRAYS TO POPULATE
            bRatio_Up1 = np.zeros(4)
            bRatioMidTest_Up1 = np.zeros(2)
            mArray_i = self.basicdata.listM_s[i]
            ## NUMBA LOOP THROUGH WEEKS AND SITES
            (mArray_i, bRatio_i, bRatioMidTest_i, K_s, 
                bRatio_Up1, bRatioMidTest_Up1) = rickerWeeksSites(i, 
                self.basicdata.nDenSites, D, 
                self.basicdata.diffWeeks,
                self.basicdata.r, mArray_i, bRatio_i, 
                bRatioMidTest_i, bRatioThreshold_s,
                self.basicdata.K_List[i], 
                self.basicdata.nDenSessions, bRatio_Up1, bRatioMidTest_Up1)
            ## PUT THE B RATIO AND B MID TEST VALUES IN NEXT UP SESSION FOR TIME 0
            if i < (self.basicdata.nDenSessions - 1):
                for j in range(self.basicdata.nDenSites):
                    if j < 2:
                        self.basicdata.bRatioList_s[(i + 1)][0, j] = bRatio_Up1[j]
                    elif j > 3:
                        self.basicdata.bRatioList_s[(i + 1)][0, (j - 2)] = (
                            bRatio_Up1[j-2])
                        self.basicdata.bRatioMidTestList_s[(i + 1)][0, (j-4)] = (
                                bRatioMidTest_Up1[j-4])
            ## ADD TO LIKELIHOODS
            llik_s += stats.multivariate_normal.logpdf(self.basicdata.listLnDensity[i], 
                mArray_i, self.basicdata.gammCovMat)
            llik += stats.multivariate_normal.logpdf(self.basicdata.listLnDensity[i], 
                self.basicdata.listM[i], self.basicdata.gammCovMat)
        prior_s = (stats.norm.logpdf(np.log(bRatioThreshold_s), self.params.bRatioThresPrior[0], 
            self.params.bRatioThresPrior[1]))
        prior = (stats.norm.logpdf(np.log(self.basicdata.bRatioThreshold), 
            self.params.bRatioThresPrior[0], self.params.bRatioThresPrior[1]))
        pnow = llik + prior
        pnew = llik_s + prior_s
        pdiff = pnew - pnow
        if pdiff > 1.0:
            rValue = 1.0
            zValue = 0.0
        elif pdiff < -12.0:
            rValue = 0.0
            zValue = 1.0
        else:
            rValue = np.exp(pdiff)        # calc importance ratio
            zValue = np.random.uniform(0.0, 1.0, size = None)
        if (rValue > zValue):
            self.basicdata.listM = deepcopy(self.basicdata.listM_s)
            self.basicdata.bRatioList = deepcopy(self.basicdata.bRatioList_s)
            self.basicdata.bRatioMidTestList=(deepcopy(self.basicdata.bRatioMidTestList_s))
            self.basicdata.bRatioThreshold = bRatioThreshold_s

#        print('br threshold', self.basicdata.bRatioThreshold)


    def maxK_update(self):
        for elev in range(2):
            maxK_s = self.basicdata.maxK.copy()
            ## RANDOM PROPOSAL OF MAX K
            minDiff = self.basicdata.maxK[elev] - self.params.maxKPrior[elev, 0]
            maxDiff = self.params.maxKPrior[elev, 1] - self.basicdata.maxK[elev]
            if minDiff < self.params.maxKSearch:
                lowSearch = self.params.maxKPrior[elev, 0]
                lowCount = minDiff
            else:
                lowSearch = self.basicdata.maxK[elev] - self.params.maxKSearch
                lowCount = self.params.maxKSearch
            if maxDiff < self.params.maxKSearch:
                highSearch = self.params.maxKPrior[elev, 1]
                highCount = maxDiff
            else:
                highSearch = self.basicdata.maxK[elev] + self.params.maxKSearch
                highCount = self.params.maxKSearch
            nowCount = lowCount + highCount
            qNewGivenNow = 1.0 / nowCount
            maxK_s[elev] = np.random.uniform(lowSearch, highSearch)
            ## GET PROPOSAL COUNT FOR M-H CORRECTION
            minDiff = maxK_s[elev] - self.params.maxKPrior[elev, 0]
            maxDiff = self.params.maxKPrior[elev, 1] - maxK_s[elev]
            if minDiff < self.params.maxKSearch:
                lowNewCount = minDiff
            else:
                lowNewCount = self.params.maxKSearch
            if maxDiff < self.params.maxKSearch:
                highNewCount = maxDiff
            else:
                highNewCount = self.params.maxKSearch
            newCount = lowNewCount + highNewCount
            qNowGivenNew = 1.0 / newCount


            llik_s = 0.0
            llik = 0.0
#            maxK_s = np.exp(np.random.normal(np.log(self.basicdata.maxK), 
#                self.params.maxKSearch))
            K_High0 = maxK_s[0] 
            K_Low0 = maxK_s[1]
            k_up1 = np.zeros(2)
            week = 0
            ## LOOP SESSIONS
            for i in range(self.basicdata.nDenSessions):
                ## TIME SINCE MAST EFFECT
                wkMask = self.basicdata.maskNegExpWk_List[i]
                K_High_s = np.repeat(maxK_s[0], self.basicdata.diffWeeks[i])
                K_High_s[wkMask] = maxK_s[0] * self.basicdata.negExpPred_HighList[i]
                K_Low_s = np.repeat(maxK_s[1], self.basicdata.diffWeeks[i])
                K_Low_s[wkMask] = maxK_s[1] * self.basicdata.negExpPred_LowList[i]

                if np.sum(wkMask) != len(self.basicdata.negExpPred_HighList[i]):
                    print('gamm', 'len wkMask', len(wkMask), 'sumWkMask', np.sum(wkMask),
                        'len negExpH_s & L', len(negExpHigh_s), len(negExpLow_s))

                bRatio_i = self.basicdata.bRatioList_s[i]
####                bRatioMidTest_i = self.basicdata.bRatioMidTestList_s[i]
#                K_s = self.basicdata.K_List_s[i]    
#                K_s[:, 0] = K_High_s
#                K_s[:, 1] = K_Low_s

                self.basicdata.K_List_s[i][:, 0] = K_High_s
                self.basicdata.K_List_s[i][:, 1] = K_Low_s


                ## START DENSITY FOR SESSION I
                if i == 0:
                    D = self.basicdata.D0.copy()
                    k_up1[0] = K_High0                    
                    k_up1[1] = K_Low0
                    ## GET B RATIO FOR HIGH AND MID SITES FOR SESSION 0
                    for j in range(2):
                        ## MID SITE DENSITY AND B (CARRYING CAPACITY) AND bRatio
                        D_mid = self.basicdata.D0[j + 4]
                        K_mid = K_Low0
                        bRatio_i[0, (j + 2)] = D_mid / K_mid 
                        ## HIGH SITE bRatio
                        D_High= self.basicdata.D0[j]
                        K_High = K_High0
                        bRatio_i[0, j] = D_High / K_High

################
                else:
                    D = deepcopy(self.basicdata.listDensity[(i - 1)]) 
                    k_up1[0] = self.basicdata.K_List_s[i - 1][-1, 0]                    
                    k_up1[1] = self.basicdata.K_List_s[i - 1][-1, 1]
                bRatio_Up1 = np.zeros(4)
####                bRatioMidTest_Up1 = np.zeros(2)
                mArray_i = self.basicdata.listM_s[i]
                ## NUMBA LOOP THROUGH WEEKS AND SITES
                (mArray_i, bRatio_i, K_s, 
                    bRatio_Up1, week) = rickerWeeksSites(i, 
                    self.basicdata.nDenSites, D, 
                    self.basicdata.diffWeeks,
                    self.basicdata.r, mArray_i, bRatio_i, 
                    self.basicdata.K_List_s[i],
                    self.basicdata.nDenSessions, bRatio_Up1, self.basicdata.immWeek, 
                    week, self.immModel, k_up1, self.params.minBRatio)
                ## PUT THE B RATIO AND B MID TEST VALUES IN NEXT UP SESSION FOR TIME 0
                if i < (self.basicdata.nDenSessions - 1):
                    for j in range(self.basicdata.nDenSites):
                        if j < 2:
                            self.basicdata.bRatioList_s[(i + 1)][0, j] = bRatio_Up1[j]
                        elif j > 3:
                            self.basicdata.bRatioList_s[(i + 1)][0, (j - 2)] = (
                                bRatio_Up1[j-2])
#                            self.basicdata.bRatioMidTestList_s[(i + 1)][0, (j-4)] = (
#                                bRatioMidTest_Up1[j-4])
                ## GET LIKELIHOODS
                ll_s_i = stats.multivariate_normal.logpdf(self.basicdata.listLnDensity[i],
                    mArray_i, self.basicdata.gammCovMat)
                ll_i = stats.multivariate_normal.logpdf(self.basicdata.listLnDensity[i], 
                    self.basicdata.listM[i], self.basicdata.gammCovMat)
                llik_s += ll_s_i 
                llik += ll_i

            ##  PRIORS AND IMPORTANCE RATIO
#            prior_s = (stats.norm.logpdf(np.log(maxK_s[elev]), self.params.maxKPrior[elev, 0], 
#                self.params.maxKPrior[elev, 1]))
#            prior = (stats.norm.logpdf(np.log(self.basicdata.maxK[elev]), 
#                self.params.maxKPrior[elev, 0], self.params.maxKPrior[elev, 1]))
            pnow = llik + + np.log(qNewGivenNow)
            pnew = llik_s + + np.log(qNowGivenNew)
            pdiff = pnew - pnow
            if pdiff > 1.0:
                rValue = 1.0
                zValue = 0.0
            elif pdiff < -12.0:
                rValue = 0.0
                zValue = 1.0
            else:
                rValue = np.exp(pdiff)        # calc importance ratio
                zValue = np.random.uniform(0.0, 1.0, size = None)
            if (rValue > zValue): 
                self.basicdata.maxK = maxK_s.copy()
                self.basicdata.listM = deepcopy(self.basicdata.listM_s)
                self.basicdata.K_List = deepcopy(self.basicdata.K_List_s)
####                self.basicdata.bRatioList = deepcopy(self.basicdata.bRatioList_s)



    def maxK_updateXXX(self):
        ## RANDOM PROPOSAL OF MAX K
        minDiff = self.basicdata.maxK - self.params.maxKPrior[0]
        maxDiff = self.params.maxKPrior[1] - self.basicdata.maxK
        if self.basicdata.maxK == self.params.changeWkPrior[0]:
            changeWk_s = self.params.changeWkPrior[0] + 1
            ## FOR METROPOLIS HASTINGS
            qNowGivenNew = 0.5
            qNewGivenNow = 1.0
        elif self.basicdata.changeWk == self.params.changeWkPrior[1]:
            changeWk_s = self.params.changeWkPrior[1] - 1
            ## FOR METROPOLIS HASTINGS
            qNowGivenNew = 0.5
            qNewGivenNow = 1.0
        else:
            upDownWk = np.random.choice(self.wkUpDown)
            changeWk_s = self.basicdata.changeWk + upDownWk
            ## FOR METROPOLIS HASTINGS
            qNowGivenNew = 0.5
            qNewGivenNow = 0.5


        llik_s = 0.0
        llik = 0.0
        maxK_s = np.exp(np.random.normal(np.log(self.basicdata.maxK), 
            self.params.maxKSearch))
        K_High0 = self.basicdata.maxK[0] 
        K_Low0 = self.basicdata.maxK[1]
        ## LOOP SESSIONS
        for i in range(self.basicdata.nDenSessions):
            ## TIME SINCE MAST EFFECT
            wkMask = self.basicdata.maskNegExpWk_List[i]
            K_High_s = np.repeat(maxK_s[0], self.basicdata.diffWeeks[i])
            K_High_s[wkMask] = maxK_s[0] * self.basicdata.negExpPred_HighList[i]
            K_Low_s = np.repeat(maxK_s[1], self.basicdata.diffWeeks[i])
            K_Low_s[wkMask] = maxK_s[1] * self.basicdata.negExpPred_LowList[i]


            if np.sum(wkMask) != len(self.basicdata.negExpPred_HighList[i]):
                print('gamm', 'len wkMask', len(wkMask), 'sumWkMask', np.sum(wkMask),
                    'len negExpH_s & L', len(negExpHigh_s), len(negExpLow_s))







            bRatio_i = self.basicdata.bRatioList_s[i]
            bRatioMidTest_i = self.basicdata.bRatioMidTestList_s[i]
            K_s = self.basicdata.K_List_s[i]    
            K_s[:, 0] = K_High_s
            K_s[:, 1] = K_Low_s
            ## START DENSITY FOR SESSION I
            if i == 0:
                D = self.basicdata.D0.copy()
                ## GET B RATIO FOR HIGH AND MID SITES FOR SESSION 0
                for j in range(2):
                    ## MID SITE DENSITY AND B (CARRYING CAPACITY) AND bRatio
                    D_mid = self.basicdata.D0[j + 4]
                    K_mid = K_Low0
                    bRatio_i[0, (j + 2)] = D_mid / K_mid * np.log(D_mid)
                    bRatioMidTest_i[0, j] = D_mid / K_mid
                    ## HIGH SITE bRatio
                    D_High= self.basicdata.D0[j]
                    K_High = K_High0
                    bRatio_i[0, j] = D_High / K_High
            else:
                D = deepcopy(self.basicdata.listDensity[(i - 1)]) ## CHECK DEEPCOPY
            bRatio_Up1 = np.zeros(4)
            bRatioMidTest_Up1 = np.zeros(2)
            mArray_i = self.basicdata.listM_s[i]
            ## NUMBA LOOP THROUGH WEEKS AND SITES
            (mArray_i, bRatio_i, bRatioMidTest_i, K_s, 
                bRatio_Up1, bRatioMidTest_Up1) = rickerWeeksSites(i, 
                self.basicdata.nDenSites, D, 
                self.basicdata.diffWeeks,
                self.basicdata.r, mArray_i, bRatio_i, 
                bRatioMidTest_i, self.basicdata.bRatioThreshold, K_s, 
                self.basicdata.nDenSessions, bRatio_Up1, bRatioMidTest_Up1)
#            ###############################################
#            ##### BREAK OUT OF LOOP IF K IS TOO HIGH OR LOW
#            minK = np.min(K_s)
#            maxK = np.max(K_s)
#            if (minK < self.params.KMinMax[0]) | (maxK > self.params.KMinMax[1]):
#                break

            ## PUT THE B RATIO AND B MID TEST VALUES IN NEXT UP SESSION FOR TIME 0
            if i < (self.basicdata.nDenSessions - 1):
                for j in range(self.basicdata.nDenSites):
                    if j < 2:
                        self.basicdata.bRatioList_s[(i + 1)][0, j] = bRatio_Up1[j]
                    elif j > 3:
                        self.basicdata.bRatioList_s[(i + 1)][0, (j - 2)] = (
                            bRatio_Up1[j-2])
                        self.basicdata.bRatioMidTestList_s[(i + 1)][0, (j-4)] = (
                            bRatioMidTest_Up1[j-4])
            ## GET LIKELIHOODS
            ll_s_i = stats.multivariate_normal.logpdf(self.basicdata.listLnDensity[i],
                mArray_i, self.basicdata.gammCovMat)
            ll_i = stats.multivariate_normal.logpdf(self.basicdata.listLnDensity[i], 
                self.basicdata.listM[i], self.basicdata.gammCovMat)
            llik_s += ll_s_i 
            llik += ll_i

        ##  PRIORS AND IMPORTANCE RATIO
        prior_s = np.sum(stats.norm.logpdf(np.log(maxK_s), self.params.maxKPrior[:, 0], 
            self.params.maxKPrior[:, 1]))
        prior = np.sum(stats.norm.logpdf(np.log(self.basicdata.maxK), self.params.maxKPrior[:, 0], 
            self.params.maxKPrior[:, 1]))
        pnow = llik + prior
        pnew = llik_s + prior_s
        pdiff = pnew - pnow
        if pdiff > 1.0:
            rValue = 1.0
            zValue = 0.0
        elif pdiff < -12.0:
            rValue = 0.0
            zValue = 1.0
        else:
            rValue = np.exp(pdiff)        # calc importance ratio
            zValue = np.random.uniform(0.0, 1.0, size = None)
        if (rValue > zValue): 
            self.basicdata.maxK = maxK_s.copy()
            self.basicdata.listM = deepcopy(self.basicdata.listM_s)
            self.basicdata.K_List = deepcopy(self.basicdata.K_List_s)
            self.basicdata.bRatioList = deepcopy(self.basicdata.bRatioList_s)
            self.basicdata.bRatioMidTestList = (deepcopy(self.basicdata.bRatioMidTestList_s))


    def changeWk_update(self):
        ## RANDOM PROPOSAL OF CHANGE WEEK
        if self.basicdata.changeWk == self.params.changeWkPrior[0]:
            changeWk_s = self.params.changeWkPrior[0] + 1
            ## FOR METROPOLIS HASTINGS
            qNowGivenNew = 0.5
            qNewGivenNow = 1.0
        elif self.basicdata.changeWk == self.params.changeWkPrior[1]:
            changeWk_s = self.params.changeWkPrior[1] - 1
            ## FOR METROPOLIS HASTINGS
            qNowGivenNew = 0.5
            qNewGivenNow = 1.0
        else:
            upDownWk = np.random.choice(self.wkUpDown)
            changeWk_s = self.basicdata.changeWk + upDownWk
            ## FOR METROPOLIS HASTINGS
            qNowGivenNew = 0.5
            qNewGivenNow = 0.5
        llik_s = 0.0
        llik = 0.0
        K_High0 = self.basicdata.maxK[0] 
        K_Low0 = self.basicdata.maxK[1]
        k_up1 = np.zeros(2)                 ## K FOR FIRST WEEK OF NEXT SESS
        week = 0
        ## LOOP SESSIONS
        for i in range(self.basicdata.nDenSessions):
            ## MASK WEEKS FOR EXPONENTIAL DECAY
            wkMask_s = self.basicdata.weekMastList[i] >= changeWk_s
            self.basicdata.maskNegExpWk_List_s[i] = wkMask_s
            ## WEEK OF EXPONENTIAL DECAY
            wkNegExp_s = self.basicdata.weekMastList[i][wkMask_s] - changeWk_s
            self.basicdata.negExpWk_List_s[i] = wkNegExp_s       # WEEK MULTIPLIER FOR DECAY
            ## HIGH NEG EXP PRED AND K
            K_High_s = np.repeat(self.basicdata.maxK[0], self.basicdata.diffWeeks[i])
            negExpHigh_s = np.exp(-wkNegExp_s * self.basicdata.gamm[0]) ## LEN OF POST CHANGE WK
            K_High_s[wkMask_s] = self.basicdata.maxK[0] * negExpHigh_s
            self.basicdata.negExpPred_HighList_s[i] = negExpHigh_s
            ## LOW NEG EXP PRED AND K
            K_Low_s = np.repeat(self.basicdata.maxK[1], self.basicdata.diffWeeks[i])
            negExpLow_s = np.exp(-wkNegExp_s * self.basicdata.gamm[1]) ## LEN OF POST CHANGE WK
            K_Low_s[wkMask_s] = self.basicdata.maxK[1] * negExpLow_s
            self.basicdata.negExpPred_LowList_s[i] = negExpLow_s
            ## POPULATE THE PROPOSED K LIST
            self.basicdata.K_List_s[i][:,0] = K_High_s
            self.basicdata.K_List_s[i][:,1] = K_Low_s
            bRatio_i = self.basicdata.bRatioList_s[i]

            ## START DENSITY FOR SESSION I
            if i == 0:
                D = self.basicdata.D0.copy()
                k_up1[0] = K_High0                    
                k_up1[1] = K_Low0 
                ## GET B RATIO FOR HIGH AND MID SITES FOR SESSION 0
                for j in range(2):
                    ## MID SITE DENSITY AND B (CARRYING CAPACITY) AND bRatio
                    D_mid = self.basicdata.D0[j + 4]
                    K_mid = K_Low0
                    bRatio_i[0, (j + 2)] = D_mid / K_mid
                    ## HIGH SITE bRatio
                    D_High= self.basicdata.D0[j]
                    K_High = K_High0
                    bRatio_i[0, j] = D_High / K_High

#############
            else:
                D = deepcopy(self.basicdata.listDensity[(i - 1)]) ## CHECK DEEPCOPY
                ## LAST K OF PREVIOUS SESSION - WILL BE FIRST K IN SESS I
                k_up1[0] = self.basicdata.K_List_s[i - 1][-1, 0]                    
                k_up1[1] = self.basicdata.K_List_s[i - 1][-1, 1]
            bRatio_Up1 = np.zeros(4)
####            bRatioMidTest_Up1 = np.zeros(2)
            mArray_i = self.basicdata.listM_s[i]
            ## NUMBA LOOP THROUGH WEEKS AND SITES
            (mArray_i, bRatio_i, K_s, 
                bRatio_Up1, week) = rickerWeeksSites(i, 
                self.basicdata.nDenSites, D, 
                self.basicdata.diffWeeks,
                self.basicdata.r, mArray_i, bRatio_i, 
                self.basicdata.K_List_s[i],
                self.basicdata.nDenSessions, bRatio_Up1, self.basicdata.immWeek, 
                week, self.immModel, k_up1, self.params.minBRatio)

            ## PUT THE B RATIO AND B MID TEST VALUES IN NEXT UP SESSION FOR TIME 0
            if i < (self.basicdata.nDenSessions - 1):
                for j in range(self.basicdata.nDenSites):
                    if j < 2:
                        self.basicdata.bRatioList_s[(i + 1)][0, j] = bRatio_Up1[j]
                    elif j > 3:
                        self.basicdata.bRatioList_s[(i + 1)][0, (j - 2)] = (
                            bRatio_Up1[j-2])
#                        self.basicdata.bRatioMidTestList_s[(i + 1)][0, (j-4)] = (
#                            bRatioMidTest_Up1[j-4])
            ## GET LIKELIHOODS
            ll_s_i = stats.multivariate_normal.logpdf(self.basicdata.listLnDensity[i],
                mArray_i, self.basicdata.gammCovMat)
            ll_i = stats.multivariate_normal.logpdf(self.basicdata.listLnDensity[i], 
                self.basicdata.listM[i], self.basicdata.gammCovMat)
            llik_s += ll_s_i 
            llik += ll_i
        pnow = llik + np.log(qNewGivenNow) 
        pnew = llik_s + np.log(qNowGivenNew)

#        print('wk_s', changeWk_s, 'wk', self.basicdata.changeWk, 'pnow', np.round(pnow,1), 
#            'pnew', np.round(pnew,1), 'qNewGivenNow', qNewGivenNow, 'qNowGivenNew', qNowGivenNew)

        pdiff = pnew - pnow
        if pdiff > 1.0:
            rValue = 1.0
            zValue = 0.0
        elif pdiff < -12.0:
            rValue = 0.0
            zValue = 1.0
        else:
            rValue = np.exp(pdiff)        # calc importance ratio
            zValue = np.random.uniform(0.0, 1.0, size = None)
        if (rValue > zValue):
            self.basicdata.changeWk = changeWk_s
            self.basicdata.listM = deepcopy(self.basicdata.listM_s)
            self.basicdata.K_List = deepcopy(self.basicdata.K_List_s)
###            self.basicdata.bRatioList = deepcopy(self.basicdata.bRatioList_s)
            self.basicdata.negExpPred_HighList = deepcopy(self.basicdata.negExpPred_HighList_s)
            self.basicdata.negExpPred_LowList = deepcopy(self.basicdata.negExpPred_LowList_s)
            self.basicdata.maskNegExpWk_List = deepcopy(self.basicdata.maskNegExpWk_List_s)
            self.basicdata.negExpWk_List = deepcopy(self.basicdata.negExpWk_List_s)


    def immWeek_update(self):
        for para in range(2):
            immWk_s = self.basicdata.immWeek.copy()
            ## RANDOM PROPOSAL OF CHANGE WEEK
            if self.basicdata.immWeek[para] == self.params.immWeekPrior[para, 0]:
                immWk_s[para] = self.params.immWeekPrior[para, 0] + 1
                ## FOR METROPOLIS HASTINGS
                qNowGivenNew = 0.5
                qNewGivenNow = 1.0
            elif self.basicdata.immWeek[para] == self.params.immWeekPrior[para, 1]:
                immWk_s[para] = self.params.immWeekPrior[para, 1] - 1
                ## FOR METROPOLIS HASTINGS
                qNowGivenNew = 0.5
                qNewGivenNow = 1.0
            else:
                upDownWk = np.random.choice(self.wkUpDown)
                immWk_s[para] = self.basicdata.immWeek[para] + upDownWk
                ## FOR METROPOLIS HASTINGS
                qNowGivenNew = 0.5
                qNewGivenNow = 0.5
            llik_s = 0.0
            llik = 0.0
            k_up1 = np.zeros(2)                 ## K FOR FIRST WEEK OF NEXT SESS
            week = 0
            for i in range(self.basicdata.nDenSessions):
                bRatio_i = self.basicdata.bRatioList_s[i]
            ## START DENSITY FOR SESSION I
                if i == 0:
                    D = self.basicdata.D0.copy()
                    k_up1[0] = self.basicdata.maxK[0]                    
                    k_up1[1] = self.basicdata.maxK[1]
                    ## GET B RATIO FOR HIGH AND MID SITES FOR SESSION 0
                    for j in range(2):
                        ## MID SITE DENSITY AND B (CARRYING CAPACITY) AND bRatio
                        D_mid = self.basicdata.D0[j + 4]
                        K_mid = self.basicdata.maxK[1]
                        bRatio_i[0, (j + 2)] = D_mid / K_mid
                        ## HIGH SITE bRatio
                        D_High= self.basicdata.D0[j]
                        K_High = self.basicdata.maxK[0]
                        bRatio_i[0, j] = D_High / K_High
                else:
                    D = deepcopy(self.basicdata.listDensity[(i - 1)]) 
                    ## LAST K OF PREVIOUS SESSION - WILL BE FIRST K IN SESS I
                    k_up1[0] = self.basicdata.K_List[i - 1][-1, 0]                    
                    k_up1[1] = self.basicdata.K_List[i - 1][-1, 1]
                ## EMPTY ARRAYS TO POPULATE
                bRatio_Up1 = np.zeros(4)
                mArray_i = self.basicdata.listM_s[i]
                ## NUMBA LOOP THROUGH WEEKS AND SITES
                (mArray_i, bRatio_i, K_s, 
                    bRatio_Up1, week) = rickerWeeksSites(i, 
                    self.basicdata.nDenSites, D, 
                    self.basicdata.diffWeeks,
                    self.basicdata.r, mArray_i, bRatio_i, 
                    self.basicdata.K_List[i], 
                    self.basicdata.nDenSessions, bRatio_Up1, immWk_s, 
                    week, self.immModel, k_up1, self.params.minBRatio)

#                print('ses', i, 'M', np.round(self.basicdata.listM[i],4),
#                    'mArray_i', np.round(mArray_i, 4),
#                    'M_s', np.round(self.basicdata.listM_s[i], 4))

                ## PUT THE B RATIO AND B MID TEST VALUES IN NEXT UP SESSION FOR TIME 0
                if i < (self.basicdata.nDenSessions - 1):
                    for j in range(self.basicdata.nDenSites):
                        if j < 2:
                            self.basicdata.bRatioList_s[(i + 1)][0, j] = bRatio_Up1[j]
                        elif j > 3:
                            self.basicdata.bRatioList_s[(i + 1)][0, (j - 2)] = (
                                bRatio_Up1[j-2])
                ## ADD TO LIKELIHOODS
                llik_s += stats.multivariate_normal.logpdf(self.basicdata.listLnDensity[i],
                    mArray_i, self.basicdata.gammCovMat)
                llik += stats.multivariate_normal.logpdf(self.basicdata.listLnDensity[i], 
                    self.basicdata.listM[i], self.basicdata.gammCovMat)
            pnow = llik + np.log(qNewGivenNow)
            pnew = llik_s + np.log(qNowGivenNew)
            pdiff = pnew - pnow
            if pdiff > 1.0:
                rValue = 1.0
                zValue = 0.0
            elif pdiff < -12.0:
                rValue = 0.0
                zValue = 1.0
            else:
                rValue = np.exp(pdiff)        # calc importance ratio
                zValue = np.random.uniform(0.0, 1.0, size = None)

#            if para == 0:
#                print('imm', self.basicdata.immWeek, 'imm_s', immWk_s, 
#                    'den', self.basicdata.listLnDensity[0][:2],
#                    'M', self.basicdata.listM[0][:2],
#                    'm_s', self.basicdata.listM_s[0][:2],
#                    'pnow', np.round(pnow,2), 'pnew', np.round(pnew,2))


            if (rValue > zValue):
                self.basicdata.immWeek = immWk_s.copy()
                self.basicdata.listM = deepcopy(self.basicdata.listM_s)
###                self.basicdata.bRatioList = deepcopy(self.basicdata.bRatioList_s)




    def getDIC_Iter(self):
        """
        ## GET DIC FOR THE g ITERATION
        """
        self.DIC_g = 0.0
        for sess in range(self.basicdata.nDenSessions):
            self.DIC_g += (-2.0 * 
                stats.multivariate_normal.logpdf(self.basicdata.listLnDensity[sess],
                self.basicdata.listM[sess], self.basicdata.gammCovMat))





    ########            Main mcmc function
    ########
    def mcmc(self):
        # total steps including all checkpoints
        self.maxsteps = ((self.params.ngibbs * self.params.thinrate) + self.params.burnin)
        # stopping step for this run or checkpoint
        self.stoppingStep = np.min([(self.startingStep + self.params.interval), self.maxsteps])

        print('startstep =', self.startingStep, 'stopstep =', self.stoppingStep)

        for self.g in range(self.startingStep, self.stoppingStep):

#            print('g', self.g, 'D0', np.round(self.basicdata.D0[0],2), 
#                'miss0', np.round(self.basicdata.missingDen[0],2))

            ## UPDATE GAMMA PARAMETERS; EQ. 21
#            self.gammBlock_update()
            self.gamm_update()


#            (self.basicdata.gamm, self.basicdata.listM) = self.b_update(
#                self.basicdata.gamm, self.params.gammSearch, 
#                self.basicdata.listGammX, self.basicdata.gammCovMat, 
#                self.basicdata.listLnDensity, self.basicdata.listM, 
#                self.params.gammPrior, updateDensity = True)


####            ## UPDATE BETA PARAMETERS; EQ. 10
####            self.betaUpdate()


#            (self.basicdata.beta, self.basicdata.listMu) = self.b_update(
#                self.basicdata.beta, self.params.betaSearch, 
#                self.basicdata.listBetaX, self.basicdata.betaCovMat, 
#                self.basicdata.listLnSigma, self.basicdata.listMu, 
#                self.params.betaPrior, updateDensity = False)

            ## UPDATE VARIANCE FOR DENSITY PARAMETERS; EQ. 22
            (self.basicdata.vDen, self.basicdata.gammCovMat) = self.v_update(
                self.basicdata.vDen, self.params.vDenSearch, 
                self.basicdata.gammCorMat, self.basicdata.gammCovMat,
                self.basicdata.listLnDensity, self.basicdata.listM, 
                self.params.vDenPrior)

####            ## UPDATE VARIANCE FOR SIGMA PARAMETERS; EQ. 11
####            self.vSigUpdate()

#            (self.basicdata.vSig, self.basicdata.betaCovMat) = self.v_update(
#                self.basicdata.vSig, self.params.vSigSearch, 
#                self.basicdata.betaCorMat, self.basicdata.betaCovMat,
#                self.basicdata.listLnSigma, self.basicdata.listMu, 
#                self.params.vSigPrior)

            ## UPDATE PHI FOR DENSITY PARAMETERS; EQ. 22
            (self.basicdata.phiDen, self.basicdata.gammCorMat, 
                self.basicdata.gammCovMat) = self.phi_update(
                self.basicdata.phiDen, self.basicdata.vDen, self.params.phiDenSearch, 
                self.basicdata.gammCorMat, self.basicdata.gammCovMat,
                self.basicdata.listLnDensity, self.basicdata.listM, 
                self.params.phiDenPrior)

#            ## UPDATE PHI FOR SIGMA PARAMETERS; EQ. 11
#            (self.basicdata.phiSig, self.basicdata.betaCorMat, 
#                self.basicdata.betaCovMat) = self.phi_update(
#                self.basicdata.phiSig, self.basicdata.vSig, self.params.phiSigSearch, 
#                self.basicdata.betaCorMat, self.basicdata.betaCovMat,
#                self.basicdata.listLnSigma, self.basicdata.listMu, 
#                self.params.phiSigPrior)

            ## UPDATE MISSING DENSITY DATA
            self.updateMissDensity()

            ## IF DOING CROSS VALIDATION
            if self.params.crossValidation:
                self.updateMissDensityCrossValid()


####            ## UPDATE MISSING SIGMA DATA
####            self.updateMissSigma()
            ## UPDATE D0
            self.updateD0()
            ## UPDATE r GROWTH RATE PARAMETER
            self.r_update()
####            ## UPDATE THE THRESHOLD FOR B RATIO FOR IMMIGRATION TO HIGH
####            self.bRatioThresUpdate()
            ## UPDATE MAX K
            self.maxK_update()
            ## UPDATE CHANGE WK
            self.changeWk_update()
            ## UPDATE IMMIGRATION WEEKS
            if self.immModel:
                if self.params.limitImmWeek:
                    self.immWeek_update()


            ## POPULATE STORAGE ARRAYS
            if self.g in self.params.keepseq:
                self.gammGibbs[self.cc] = self.basicdata.gamm
                self.maxKGibbs[self.cc] = self.basicdata.maxK
                self.changeWkGibbs[self.cc] = self.basicdata.changeWk
                self.immWeekGibbs[self.cc] = self.basicdata.immWeek
                self.betaGibbs[self.cc] = self.basicdata.beta
                self.vDenGibbs[self.cc] = self.basicdata.vDen
                self.vSigGibbs[self.cc] = self.basicdata.vSig
                self.phiDenGibbs[self.cc] = self.basicdata.phiDen
                self.missDenGibbs[self.cc] = self.basicdata.missingDen
                self.missSigGibbs[self.cc] = self.basicdata.missingSig
                self.D0Gibbs[self.cc] = self.basicdata.D0
                self.rGibbs[self.cc] = self.basicdata.r

                ## GET DIC FOR g ITERATION
                self.getDIC_Iter()
                self.DICGibbs[self.cc] = self.DIC_g
                
                ## IF CROSSVALIDATION
                if self.params.crossValidation:
                    self.cvErrorSq += (((np.exp(
                        self.basicdata.listM[self.basicdata.sessCV][self.basicdata.siteCV]) / 100.0) - 
                        self.basicdata.cvObsDensity / 100.0)**2)
                    self.cvMeanErr += (((self.basicdata.cvMeanDensity / 100.0) -
                        (self.basicdata.cvObsDensity / 100.0))**2)


#                M0 = np.vstack(self.basicdata.listM)

                M = np.hstack(self.basicdata.listM)

                self.meanMDen += np.exp(M) / 100. 
                self.meanMuSigma += self.basicdata.mu


#                for c in range(self.basicdata.nDenSessions):
#                    for dd in range(6):
#                        print('sess', c, 'site', dd, 
#                            'lstDen', np.round(self.basicdata.listDensity[c][dd], 2),
#                            'M', np.round(np.exp(self.basicdata.listM[c][dd]), 2),
#                            'm diff', 
#                            np.round(((self.basicdata.listDensity[c][dd]) - 
#                            np.exp(self.basicdata.listM[c][dd])), 2))



#                m = np.exp(np.hstack(self.basicdata.listM))
#                quants = np.round(mquantiles(m, prob=[.05, .5, .9]), 2)
#                print('quants', quants, 'min', np.min(m), 'max', np.max(m))

                self.cc += 1
        print('End cc', self.cc)

        if self.params.crossValidation:
            print('RMSE', np.sqrt(self.cvErrorSq / self.params.ngibbs))
            self.writeCVResults()


        print('listM', np.round(np.exp(self.basicdata.listM) , 2))
        print('listDensity', np.round(self.basicdata.listDensity, 2))

#        print('gammGibbs', self.gammGibbs, 'beta', self.betaGibbs)
#        print('gammGibbs', self.gammGibbs, 'beta', self.betaGibbs, 'vSig', 
#            self.vSigGibbs, 'vDen', self.vDenGibbs, 'phiDen', self.phiDenGibbs)
#        print('missDen', self.missDenGibbs)
#        print('missSigma', self.missSigGibbs)
#        print('D0 update', self.D0Gibbs[:,:2])
#        print('R update', self.rGibbs)
#        print('tm High', self.tmHighGibbs, 'tm Low', self.tmLowGibbs)
                              
#        print('self.params.searchMissDen', self.params.searchMissDen)


    def writeCVResults(self):
        fname = os.path.join(self.params.outputDataPath, 
                'cvResultModel' + str(self.params.cvJobID) + '.csv')
        ## MAKE TABLE
        (m, n) = (1, 3)
        # create new structured array with columns of different types
        structured = np.empty((m,), dtype=[('Model', 'i8'), ('cvErrorSq', np.float),
            ('cvMeanErr', np.float)])
        # copy data over
        structured['Model'] = self.params.cvJobID
        structured['cvErrorSq'] = self.cvErrorSq
        structured['cvMeanErr'] = self.cvMeanErr
        np.savetxt(fname, structured, fmt=['%.f', '%.4f', '%.4f'], 
                    comments = '', delimiter = ',', 
                    header='Model, cvErrorSq, cvMeanErr')





###########################################
######### UN-USED FUNCTIONS
###


    def tm_update(self):
        """
        METROPOLIS SAMPLER FOR PARAMETERS FOR TIME SINCE MAST (GAMMA PDF)
        """
#        print('######################################################## tm update')
        tm_s = np.exp(np.random.normal(np.log(self.basicdata.TM), self.params.searchTM))
#        tmShape_s = (tm_s[0] / tm_s[1]) + 1.0
#        M_s_list = []
        listTMPDF_s = []
#        listTMPred_s = []

        ## UPDATE HIGH AND MID/LOW PARAMETERS SEPARATELY
        for h in range(2):
            tm_s = self.basicdata.TM.copy()
            tm_s[h] = np.exp(np.random.normal(np.log(self.basicdata.TM[h]), 
                self.params.searchTM))
            ## SET UP INITIAL B RATIOS ARRAYS IN LIST
            tmGamPDF_High0_s = genGammaPDF(.5, tm_s[0, 0], tm_s[0, 1], tm_s[0, 2])
            tmGamPDF_Low0_s = genGammaPDF(.5, tm_s[1, 0], tm_s[1, 1], tm_s[1, 2])
            tmPred_High0_s = self.basicdata.gamm[0] + self.basicdata.gamm[1] * tmGamPDF_High0_s
            tmPred_Low0_s = self.basicdata.gamm[0] + self.basicdata.gamm[2] * tmGamPDF_Low0_s
            llik_s = 0.0
            llik = 0.0
            for i in range(self.basicdata.nDenSessions):
                ## TIME MAST GAMMA PDF
                self.basicdata.tmGamPDF_HighList_s[i] = genGammaPDF(
                    self.basicdata.weekMastList[i], tm_s[0, 0], tm_s[0, 1], tm_s[0, 2])
                self.basicdata.tmGamPDF_LowList_s[i] = genGammaPDF(
                    self.basicdata.weekMastList[i], tm_s[1, 0], tm_s[1, 1], tm_s[1, 2])
                ## PREDICTION OF TIME SINCE MAST EFFECT
                self.basicdata.tmPred_HighList_s[i] = (self.basicdata.gamm[1] *
                    self.basicdata.tmGamPDF_HighList_s[i])
                self.basicdata.tmPred_LowList_s[i] =  (self.basicdata.gamm[2] *
                    self.basicdata.tmGamPDF_LowList_s[i])
                ## ARRAY TO POPULATE FOR B RATIO AND DENSITY DEPENDENCE FOR I
                bRatio_i = self.basicdata.bRatioList_s[i]
                bRatioMidTest_i = self.basicdata.bRatioMidTestList_s[i]
                B_s = self.basicdata.listB_s[i]    
                ## START DENSITY FOR SESSION I
                if i == 0:
                    D = self.basicdata.D0.copy()
                    ## GET B RATIO FOR HIGH AND MID SITES FOR SESSION 0
                    for j in range(2):
                        ## MID SITE DENSITY AND B (CARRYING CAPACITY) AND bRatio
                        D_mid = self.basicdata.D0[j + 4]
                        b_mid = np.exp(tmPred_Low0_s)
                        bRatio_i[0, (j + 2)] = D_mid / b_mid * np.log(D_mid)
                        bRatioMidTest_i[0, j] = D_mid / b_mid
                        ## HIGH SITE bRatio
                        D_High= self.basicdata.D0[j]
                        b_High = np.exp(tmPred_High0_s)
                        bRatio_i[0, j] = D_High / b_High
                else:
                    D = deepcopy(self.basicdata.listDensity[(i - 1)]) ## CHECK DEEPCOPY
                                
                bRatio_Up1 = np.zeros(4)
                bRatioMidTest_Up1 = np.zeros(2)
                mArray_i = self.basicdata.listM_s[i]
                ## NUMBA LOOP THROUGH WEEKS AND SITES
                (mArray_i, bRatio_i, bRatioMidTest_i, B_s, 
                    bRatio_Up1, bRatioMidTest_Up1) = rickerWeeksSites(i, 
                    self.basicdata.nDenSites, D, 
                    self.basicdata.diffWeeks,
                    self.basicdata.r, mArray_i, self.basicdata.tmPred_HighList_s[i], 
                    self.basicdata.tmPred_LowList_s[i], bRatio_i,
                    bRatioMidTest_i, self.basicdata.bRatioThreshold, B_s, 
                    self.basicdata.nDenSessions, bRatio_Up1, bRatioMidTest_Up1, 
                    fx = 'tm')
                ## PUT THE B RATIO AND B MID TEST VALUES IN NEXT UP SESSION FOR TIME 0
                if i < (self.basicdata.nDenSessions - 1):
                    for j in range(self.basicdata.nDenSites):
                        if j < 2:
                            self.basicdata.bRatioList_s[(i + 1)][0, j] = bRatio_Up1[j]
                        elif j > 3:
                            self.basicdata.bRatioList_s[(i + 1)][0, (j - 2)] = (
                                bRatio_Up1[j-2])
                            self.basicdata.bRatioMidTestList_s[(i + 1)][0, (j-4)] = (
                                bRatioMidTest_Up1[j-4])
                ## GET LIKELIHOODS
                llik_s += stats.multivariate_normal.logpdf(self.basicdata.listLnDensity[i], 
                    mArray_i, self.basicdata.gammCovMat)
                llik += stats.multivariate_normal.logpdf(self.basicdata.listLnDensity[i], 
                    self.basicdata.listM[i], self.basicdata.gammCovMat)
            prior_s = np.sum(stats.norm.logpdf(np.log(tm_s[h]), 
                self.params.priorTM[h][:, 0], self.params.priorTM[h][:, 1]))

            prior = np.sum(stats.norm.logpdf(np.log(self.basicdata.TM[h]), 
                self.params.priorTM[h][:, 0], self.params.priorTM[h][:, 1]))

            pnow = llik + prior
            pnew = llik_s + prior_s
            pdiff = pnew - pnow
            if pdiff > 1.0:
                rValue = 1.0
                zValue = 0.0
            elif pdiff < -12.0:
                rValue = 0.0
                zValue = 1.0
            else:
                rValue = np.exp(pdiff)        # calc importance ratio
                zValue = np.random.uniform(0.0, 1.0, size = None)
            if (rValue > zValue):
                self.basicdata.TM = tm_s.copy()
                self.basicdata.listM = deepcopy(self.basicdata.listM_s)
                ## PDF GEN GAMMA
                self.basicdata.tmGamPDF_High0 = tmGamPDF_High0_s.copy()
                self.basicdata.tmGamPDF_Low0 = tmGamPDF_Low0_s.copy()
                self.basicdata.tmGamPDF_HighList = deepcopy(
                    self.basicdata.tmGamPDF_HighList_s)
                self.basicdata.tmGamPDF_LowList = deepcopy(
                    self.basicdata.tmGamPDF_LowList_s)
                ## PREDICTED TM EFFECT
                self.basicdata.tmPred_High0 = tmPred_High0_s.copy()
                self.basicdata.tmPred_Low0 = tmPred_Low0_s.copy()
                self.basicdata.tmPred_HighList = deepcopy(self.basicdata.tmPred_HighList_s)
                self.basicdata.tmPred_LowList = deepcopy(self.basicdata.tmPred_LowList_s)
                ## B RATIO EFFECT ON DENSITY DEPENDENCE
                self.basicdata.B = deepcopy(self.basicdata.listB_s)
                self.basicdata.bRatioList = deepcopy(self.basicdata.bRatioList_s)
                self.basicdata.bRatioMidTestList = (deepcopy(
                    self.basicdata.bRatioMidTestList_s))





    def wrpC_update(self):
        """
        METROPOLIS SAMPLER FOR PARAMETERS FOR WRAPPED CAUCHY SEASONAL EFFECT
        """
        wrpC_s = np.zeros(2)
        wrpC_s[0] = np.random.normal(self.basicdata.wrpC[0], self.params.searchWrpC[0])
        wrpC_s[1] = np.exp(np.random.normal(np.log(self.basicdata.wrpC[1]), 
            self.params.searchWrpC[1]))
        M_s_list = []
        listWrpCPDF_s = []
        listSeasonEff_s = []
        llik_s = 0.0
        llik = 0.0
        for i in range(self.basicdata.nDenSessions):
            mArray_i = np.zeros(self.basicdata.nDenSites)
            ## SEASONAL WRAPPED CAUCHY EFFECT
            wrpCPDF_i = dwrpcauchy(self.basicdata.weekWrpCList[i], wrpC_s[0], wrpC_s[1])
            seasonalEff_i = wrpCPDF_i * self.basicdata.gamm[-1]
            listWrpCPDF_s.append(wrpCPDF_i)
            listSeasonEff_s.append(seasonalEff_i)
            ## TIME SINCE MAST EFFECT
            tmPred_i = self.basicdata.listPredTM[i]
            ## TOTAL TEMPORAL EFFECT: TIME-MAST AND SEASON
            totalTemporal_i = tmPred_i + seasonalEff_i
            
            if i == 0:
                lnDensity_i_1 = self.basicdata.lnD0
            else:
                lnDensity_i_1 = self.basicdata.listLnDensity[(i - 1)]
            ## NUMBA LOOPING ALTERNATIVE
            mArray_i = loopSitesWeeks(i, self.basicdata.nDenSites, self.basicdata.spacePred, 
                totalTemporal_i, lnDensity_i_1, 
                self.basicdata.diffWeeks, self.basicdata.r, mArray_i)
   
            M_s_list.append(mArray_i)
            llik_s += stats.multivariate_normal.logpdf(self.basicdata.listLnDensity[i], 
                mArray_i, self.basicdata.gammCovMat)
            llik += stats.multivariate_normal.logpdf(self.basicdata.listLnDensity[i], 
                self.basicdata.listM[i], self.basicdata.gammCovMat)
        ## PROPOSED PRIORS
        wrpCLog_s = wrpC_s.copy()
        wrpCLog_s[1] = np.log(wrpC_s[1])
        prior_s = np.sum(stats.norm.logpdf(wrpCLog_s, self.params.priorWrpC[:, 0], 
            self.params.priorWrpC[:, 1]))
        ## CURRENT PRIORS
        wrpCLog = self.basicdata.wrpC.copy()
        wrpCLog[1] = np.log(self.basicdata.wrpC[1])
        prior = np.sum(stats.norm.logpdf(wrpCLog, self.params.priorWrpC[:, 0], 
            self.params.priorWrpC[:, 1]))
        pnow = llik + prior
        pnew = llik_s + prior_s
        pdiff = pnew - pnow
        if pdiff > 1.0:
            rValue = 1.0
            zValue = 0.0
        elif pdiff < -12.0:
            rValue = 0.0
            zValue = 1.0
        else:
            rValue = np.exp(pdiff)        # calc importance ratio
            zValue = np.random.uniform(0.0, 1.0, size = None)
        if (rValue > zValue):
            self.basicdata.wrpC = wrpC_s
            self.basicdata.listM = deepcopy(M_s_list)
            self.basicdata.listWrpCPDF = deepcopy(listWrpCPDF_s)
            self.basicdata.listSeasonEff = deepcopy(listSeasonEff_s)



    def gammBlock_update(self):
        """
        METROPOLIS SAMPLER FOR GAMMA AND BETA
        """
        g_s = np.random.normal(self.basicdata.gamm, self.params.gammSearch)
        ## INTERCEPT AND ELEVATION OR OTHER SPATIAL COVARIATES
        listTMPred_s = []
        listSeasonEff_s = []
        M_s_list = []
        llik_s = 0.0
        llik = 0.0
        spacePred_s = np.dot(self.basicdata.spaceCovar, g_s[:-3])
        for i in range(self.basicdata.nDenSessions):
            mArray_i = np.zeros(self.basicdata.nDenSites)
            ## TIME SINCE MAST EFFECT
            tmPred_i = g_s[-3] * self.basicdata.listTMPDF[i]
            listTMPred_s.append(tmPred_i)
            ## SEASONAL COSINE - SINE EFFECT
            seasonalEff_i = np.dot(self.basicdata.weekSinCosList[i], g_s[-2:])
            listSeasonEff_s.append(seasonalEff_i)
            ## TOTAL TEMPORAL EFFECT: TIME-MAST AND SEASON
            totalTemporal_i = tmPred_i + seasonalEff_i



            
            if i == 0:
                lnDensity_i_1 = self.basicdata.lnD0
            else:
                lnDensity_i_1 = self.basicdata.listLnDensity[(i - 1)]


            ## NUMBA LOOPING ALTERNATIVE
            mArray_i = loopSitesWeeks(i, self.basicdata.nDenSites, spacePred_s, 
                totalTemporal_i, lnDensity_i_1, 
                self.basicdata.diffWeeks, self.basicdata.r, mArray_i)
   

#                ## LOOP DENSITY SITES (6)
#                for j in range(self.basicdata.nDenSites):
#                    ## DENSITY DEPENDENCE FOR EACH SUB-WEEK
#                    b_ij = spacePred_s[j] + totalTemporal_i 
#                    if i == 0:                                  ## FIRST SESSION
#                        logD = np.log(self.basicdata.D0[j])
#                    else:                                       ## SUBSEQUENT SESSIONS
#                        logD = self.basicdata.listLnDensity[(i - 1)][j]
#                    ## LOOP THROUGH WEEKS IN SESSION i
#                    for k in range(self.basicdata.diffWeeks[i]):
#                        logD = logD * (1.0 + b_ij[k]) + self.basicdata.r 
#                    mArray_i[j] = logD
            M_s_list.append(mArray_i)
            llik_s += stats.multivariate_normal.logpdf(self.basicdata.listLnDensity[i], 
                mArray_i, self.basicdata.gammCovMat)
            llik += stats.multivariate_normal.logpdf(self.basicdata.listLnDensity[i], 
                self.basicdata.listM[i], self.basicdata.gammCovMat)
        prior_s = np.sum(stats.norm.logpdf(g_s, self.params.gammMeanPrior, 
            self.params.gammSDPrior))
        prior = np.sum(stats.norm.logpdf(self.basicdata.gamm, 
            self.params.gammMeanPrior, self.params.gammSDPrior))
        pnow = llik + prior
        pnew = llik_s + prior_s
        pdiff = pnew - pnow
        if pdiff > 1.0:
            rValue = 1.0
            zValue = 0.0
        elif pdiff < -12.0:
            rValue = 0.0
            zValue = 1.0
        else:
            rValue = np.exp(pdiff)        # calc importance ratio
            zValue = np.random.uniform(0.0, 1.0, size = None)
        if (rValue > zValue):
            self.basicdata.gamm = g_s.copy()
            self.basicdata.listM = deepcopy(M_s_list)
            self.basicdata.spacePred = spacePred_s.copy()
            self.basicdata.listPredTM = deepcopy(listTMPred_s)
            self.basicdata.listSeasonEff = deepcopy(listSeasonEff_s)




def b_updateGibbsXXX(X, Cov, Y, bPrior, vinvert):
    """
    GIBBS SAMPLER OF CONJUGATE PRIORS FOR GAMMA AND BETA
    """
    xTranspose = np.transpose(X)
    invCovMat = np.linalg.inv(Cov)
#    xCrossProd = np.dot(xTranspose, X)
    ss = np.dot(xTranspose, invCovMat)
    sy = np.dot(ss, Y)
    sx = np.dot(ss, X)
    bigV = np.linalg.inv(sx + vinvert)
    smallv = sy + np.dot(vinvert, bPrior)
    meanVariate = np.dot(bigV, smallv)
    b = np.random.multivariate_normal(meanVariate, bigv)
    return(b)


    def gamm_updateXXX(self):
        """
        METROPOLIS SAMPLER FOR GAMMA AND BETA
        """
        g_s = np.random.normal(self.basicdata.gamm, self.params.gammSearch)
        ## INTERCEPT AND ELEVATION OR OTHER SPATIAL COVARIATES
        spacePred_s = np.dot(self.basicdata.spaceCovar, g_s[:-3])
        listTMPred_s = []
        listSeasonEff_s = []
        M_s_list = []
        mArray_i = np.zeros(self.basicdata.nDenSites)
        llik_s = 0.0
        llik = 0.0
        for i in range(self.basicdata.nDenSessions):
            ## TIME SINCE MAST EFFECT
            tmPred_i = g_s[-3] * self.basicdata.listTMPDF[i]
            listTMPred_s.append(tmPred_i)
            ## SEASONAL COSINE - SINE EFFECT
            seasonalEff_i = np.dot(self.basicdata.weekSinCosList[i], g_s[-2:])
            listSeasonEff_s.append(seasonalEff_i)
            ## TOTAL TEMPORAL EFFECT: TIME-MAST AND SEASON
            totalTemporal_i = tmPred_i + seasonalEff_i
            ## LOOP DENSITY SITES (6)
            for j in range(self.basicdata.nDenSites):
                ## DENSITY DEPENDENCE FOR EACH SUB-WEEK
                b_ij = spacePred_s[j] + totalTemporal_i 
                if i == 0:                                  ## FIRST SESSION
                    logD = np.log(self.basicdata.D0[j])
                else:                                       ## SUBSEQUENT SESSIONS
                    logD = self.basicdata.listLnDensity[(i - 1)][j]
                ## LOOP THROUGH WEEKS IN SESSION i
                for k in range(self.basicdata.diffWeeks[i]):
                    logD = logD * (1.0 + b_ij[k]) + self.basicdata.r 
                mArray_i[j] = logD
            M_s_list.append(mArray_i)
            llik_s += stats.multivariate_normal.logpdf(self.basicdata.listLnDensity[i], 
                mArray_i, self.basicdata.gammCovMat)
            llik += stats.multivariate_normal.logpdf(self.basicdata.listLnDensity[i], 
                self.basicdata.listM[i], self.basicdata.gammCovMat)
        prior_s = np.sum(stats.norm.logpdf(g_s, self.params.gammPrior[0], 
            self.params.gammPrior[1]))
        prior = np.sum(stats.norm.logpdf(self.basicdata.gamm, self.params.gammPrior[0], 
            self.params.gammPrior[1]))
        pnow = llik + prior
        pnew = llik_s + prior_s
        pdiff = pnew - pnow
        if pdiff > 1.0:
            rValue = 1.0
            zValue = 0.0
        elif pdiff < -12.0:
            rValue = 0.0
            zValue = 1.0
        else:
            rValue = np.exp(pdiff)        # calc importance ratio
            zValue = np.random.uniform(0.0, 1.0, size = None)
        if (rValue > zValue):
            self.basicdata.gamm = g_s.copy()
            self.basicdata.listM = M_s_list.copy()
            self.basicdata.spacePred = spacePred_s.copy()
            self.basicdata.listPredTM = listTMPred_s.copy()
            self.basicdata.listSeasonEff = listSeasonEff_s.copy()

