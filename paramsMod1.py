#!/usr/bin/env python

########################################
########################################
# This file is part of a MBIE funded project to study rat dynamics and detection
# process for snap traps, cameras and tracking tunnels in the Alabaster area
# Copyright (C) 2021 Jo Carpenter and Dean Anderson 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# ADDITIONAL NOTES ABOUT THIS SCRIPT.
########################################
########################################

import os
import pickle
import numpy as np


class ModelParams(object):
    """
    Contains the parameters for the mcmc run. This object is also required 
    for the pre-processing. 
    """
    def __init__(self):
        ############################################################
        ###################################### USER MODIFY HERE ONLY
        #################################
        
        ###################################################
        ## Set number of MCMC iterations, thin rate and burn in
        self.ngibbs = 2  #3000  # number of estimates to save for each parameter
        self.thinrate = 1   #30   # 200      # thin rate
        self.burnin = 0     # burn in number of iterations

        self.totalIterations = ((self.ngibbs * self.thinrate) + self.burnin)
        self.interval = self.totalIterations
        self.checkpointfreq = self.interval

        ## If have not initiated parameters, set to 'True'   
        self.firstRun = False   ## True or False         
        print('First Run:', self.firstRun)
        ## USE CHECKED DATA
        self.useCheckedData = False   # gibbs arrays not full from previous runs

        ## Model number
        self.modelID = 1

        ###################################################

        ## array of iteration id to keep
        self.keepseq = np.arange(self.burnin, ((self.ngibbs * self.thinrate) + self.burnin),
            self.thinrate)

        # set paths to scripts and data
        self.inputDataPath = os.path.join(os.getenv('ALABASTERPROJDIR', default = '.'), 
            'Data')
        self.outputDataPath = os.path.join(os.getenv('ALABASTORPROJDIR', default = '.'), 
            'Results/mod1_Results')

        print('input data path', self.inputDataPath, 'out', self.outputDataPath)


        ## set Data names
        self.basicdataFname = os.path.join(self.outputDataPath, 'basicdataMod1.pkl')
        self.gibbsFname = os.path.join(self.outputDataPath, 'gibbsMod1.pkl')
        self.paramsResFname = os.path.join(self.outputDataPath, 'parameterTableMod1.csv')

        ## Input data
        self.inputDensityFname = os.path.join(self.inputDataPath, 
            'densities_2021_02_24.csv')
        self.inputSnapFname = os.path.join(self.inputDataPath, 'SnapTrapData.csv')
        self.inputTTFname = os.path.join(self.inputDataPath, 'TT_Data.csv')
        self.inputCameraFname = os.path.join(self.inputDataPath, 'Camera3Night.csv')
        self.inputEnvironFname = os.path.join(self.inputDataPath, 'environVarDevices.csv')

        ## PLOTS AND TABLE NAMES WRITTEN TO DIRECTORY
        self.varioPlotFname = os.path.join(self.outputDataPath, 'variogramPlot.png')
        self.seasonEffectFname = os.path.join(self.outputDataPath, 'seasonEffectPlot.png')
        self.missDensityFname = os.path.join(self.outputDataPath, 'missingDensity.csv')
        self.missSigmaFname = os.path.join(self.outputDataPath, 'missingSigma.csv')
        self.D0Fname = os.path.join(self.outputDataPath, 'D0_Table.csv')

        # pickle mcmcData for checkpointing
        self.outCheckingFname = os.path.join(self.outputDataPath,'out_checking_mod' + 
            str(self.modelID)  + '.pkl')
  
        print('params basic path', self.basicdataFname) 
        
        ## Set initial parameter values


        ## COVARIATE DATA DICTIONARY FOR DENSITY AND SIGMA
        self.xdatDictionary = {'InterceptGamma' : 0, 'InterceptBeta' : 1, 
                        'Elevation' : 2,
                        'TimeMast' : 3, 'SinWeek' : 4, 'CosWeek' : 5,
                        'DensityT_1' : 6, 'SigmaT_1' : 7, 'TimeMastSquared' : 8,
                        'weekWrpC' : 9}

        InterceptGamma = self.xdatDictionary['InterceptGamma']
        InterceptBeta = self.xdatDictionary['InterceptBeta']
        Elevation = self.xdatDictionary['Elevation']
        timeMast  = self.xdatDictionary['TimeMast']
        timeMastSquared  = self.xdatDictionary['TimeMastSquared']
        SineWeek  = self.xdatDictionary['SinWeek']
        CosineWeek  = self.xdatDictionary['CosWeek']
        DensityT_1  = self.xdatDictionary['DensityT_1']
        SigmaT_1  = self.xdatDictionary['SigmaT_1']
        weekWrpC = self.xdatDictionary['weekWrpC']
        ######################################################
        ######################################################
        ##
        ## ADJUST THIS TO SET COVARIATES FOR DENSITY AND SIGMA
        self.xDenIndx = np.array([InterceptGamma, Elevation, timeMast], dtype = int)
#        self.xDenIndx = np.array([InterceptGamma, Elevation, timeMast, SineWeek,
#            CosineWeek], dtype = int)

        ## SIGMA INDEX
        self.xSigIndx = np.array([InterceptBeta, Elevation], dtype = int)
        ##
        ######################################################
        ######################################################

        ## GAMMA PARAMETERS FOR DENSITY KRIGING (EQ. 20 AND 21)
        ## GAMMA PARAS: INTERCEPT, ELEV, TIME MAST,SIN, COS 
#        self.gamm = np.array([-0.197, -0.088, 5.0, 0.065, -0.012])
        self.gamm = np.array([-0.197, -0.088, 5.0])      ## GEN GAMMA FORMULATION
#        self.gamm = np.array([-0.197, -0.088, 5.0, 0.065])      ## WRPC FORMULATION
        self.nGamm = len(self.gamm)
#        self.gammPrior = [0.0, 15.0]  
        self.gammMeanPrior = [-0.2, -0.01, 6.0, 0.0]        ## WRPC FORMULATION
#        self.gammMeanPrior = [-0.2, -0.01, 6.0, 0.0, 0.0]
        self.gammSDPrior = 5.0
        ## WRPC FORMULATION
        self.gammPrior = [[-0.2, 2.0], [-0.01, 10], [6.0, 5.0]]  #np.repeat(0, self.nGamm)
#        self.gammPrior = [[-0.2, 10.0], [-0.01, 10], [6.0, 15.0], [0.0, 15], [0.0, 15.0]]  #np.repeat(0, self.nGamm)
        self.gammSearch = [0.05, .1, 0.1]  #0.03
#        self.vInvertGamm = np.linalg.inv(np.diag(np.repeat(100, self.nGamm)))    
        ## COVARIANCE PARAMETERS FOR DENSITY
        self.phiDen = .473 #0.03    # DECAY
        self.phiDenPrior = [np.log(1), 1]
        self.phiDenSearch = 0.14    #0.18
        self.vDen = 0.75 #0.8     # VARIANCE FOR LOG OF PREDICTED DENSITY
        self.vDenPrior = [np.log(.5), .2]
        self.vDenSearch = 0.14  #0.18

        ## GOMPERTZ POP GROWTH PARAMETER
        ## INITIAL VALUE
        self.r = .182
        self.priorR = [np.log(.15), 1.2]   # LOG NORMAL PRIOR
        self.searchR = 0.04

#        ## WRAPPED CAUCHY PARAMETERS FOR SEASON
#        self.wrpC = np.array([0.0, 1.5])
#        self.priorWrpC = np.array([[0.0, 1.5], [np.log(1.5), .9]])
#        self.searchWrpC = [0.02, 0.02]


        ## TIME SINCE MAST: GENERALISED GAMMA PDF ON WEEKS
        self.TM = np.array([15.0, 1.05, 5.0])
        self.priorTM = np.array([[np.log(15), 1.0],
                                 [np.log(1.01), 0.01],
                                 [np.log(5.0), .6]])
        self.searchTM = [0.04, 0.01, 0.04]    #0.80


#        ## PRIOR FOR MODE AND SCALE PARAMETER ON EACH ROW, RESPECTIVELY
#        self.priorTM = np.array([[np.log(30), 1.0],[np.log(8), 0.9]])
#        self.searchTM = [0.1, 0.1]    #0.80

        ## BETA PARAMETERS FOR KRIGING SIGMA (EQ. 9 AND 10)
        ## INTERCEPT, ELEV, T-1 SIGMA
        self.beta = np.array([8.2, -0.33])
        self.nBeta = len(self.beta)
        # BETA PRIORS
#        self.diag = np.diagflat(np.tile(100., self.nBeta))
#        self.vinvert = np.linalg.inv(self.diag)
#        self.betaPrior = [0, 10]    
#        self.betaSearch = .08   #0.08
        self.vInvertBeta = np.linalg.inv(np.diag(np.repeat(50, self.nBeta)))    
        self.ppart = np.dot(self.vInvertBeta, np.zeros(self.nBeta))
#        ## COVARIANCE PARAMETERS FOR SIGMA
#        self.phiSig = 0.03    # DISTANCE DECAY FOR SIGMA
#        self.phiSigPrior = [np.log(1), .5]
#        self.phiSigSearch = 0.2
        self.vSig = 4.0     # VARIANCE FOR LOG OF PREDICTED SIGMA

        self.vSigPrior = [3.0, 3.0]
#        self.vSigSearch = 0.2

        ## LATENT DENSITIES AND SIGMA VIA IMPUTATION
        ## INITIAL DENSITIES AT EACH GRID
        self.D0 = np.array([0.02, 0.02, 5.36, 5.2, 6.2, 6.4])
#        self.D0 = np.exp(np.random.normal(np.log(1.0), .25, 6))
        self.nD0 = len(self.D0)
        self.lnD0MeanPrior = [np.log(0.2), np.log(0.2), np.log(4.5), np.log(4.5), 
            np.log(5.0), np.log(5.0)]
        self.lnD0SDPrior = [0.2, 0.2, 0.7, 0.7, 0.7, 0.7]
#        self.lnD0Prior = [np.log(2.5), 1.0]
        self.searchD0 = 0.2
        ## INTIAL MISSING DENSITY ESTIMATES
#        self.missingDen = np.array([0.4, 0.2, 0.8, 0.5, 0.8, 1.3, 0.45])
        self.missingDen = np.exp(np.random.normal(np.log(0.5), .7, 7))
        self.nMissingDen = len(self.missingDen)
        self.missDenPrior = [np.log(.3), 0.8]
        self.searchMissDen = 0.15
        ## INITIAL SIGMA AT EACH GRID
        self.sig0 = np.random.normal(35.0, 4.0, 6)
        self.nSig0 = len(self.sig0)
        ## INTIAL MISSING SIGMA ESTIMATES
        self.missingSig = np.random.normal(35.0, 5.0, 7)
        self.nMissingSig = len(self.missingSig)
        self.missSigPrior = [np.log(35), 1.0]
        self.searchMissSig = 0.3



        #################################
        ####################################### END USER MODIFICATION
        ##############################################################
        ## INDICES FOR COSINE AND SINE PLOT IN RESULTS
        self.sinCosIndx = np.array([SineWeek, CosineWeek], dtype = int)


