#!/bin/bash

#SBATCH --job-name=Mod28
#SBATCH --account=landcare03293 
#SBATCH --mail-type=end
#SBATCH --mail-user=deanpa@protonmail.com
#SBATCH --time=08:05:00

#SBATCH --array=0-34 
#SBATCH --mem=2000  
#SBATCH --cpus-per-task=1

module load TuiView/1.2.6-gimkl-2020a-Python-3.8.2
###module load TuiView/1.2.4-gimkl-2018b-Python-3.7.3

./startMod28.py