#!/usr/bin/env python

########################################
########################################
# This file is part of a MBIE funded project to study rat dynamics and detection
# process for snap traps, cameras and tracking tunnels in the Alabaster area
# Copyright (C) 2021 Jo Carpenter and Dean Anderson 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# ADDITIONAL NOTES ABOUT THIS SCRIPT.
########################################
########################################

import os
import pickle
import numpy as np
import datetime

class ModelParams(object):
    """
    Contains the parameters for the mcmc run. This object is also required 
    for the pre-processing. 
    """
    def __init__(self):
        ############################################################
        ###################################### USER MODIFY HERE ONLY
        #################################
        
        ###################################################
        ## Set number of MCMC iterations, thin rate and burn in
        self.ngibbs = 3000    #3000    # 1000  #3000  # number of estimates to save for each parameter
        self.thinrate = 500   #500   #10   #30   # 200      # thin rate
        self.burnin = 0     # burn in number of iterations

        self.totalIterations = ((self.ngibbs * self.thinrate) + self.burnin)
        self.interval = self.totalIterations
        self.checkpointfreq = self.interval

        ## If have not initiated parameters, set to 'True'   
        self.firstRun = False   # True or False         
        print('First Run:', self.firstRun)
        ## USE CHECKED DATA
        self.useCheckedData = False   # gibbs arrays not full from previous runs

        ## Model number
        self.modelID = 2

        ###################################################

        ## array of iteration id to keep
        self.keepseq = np.arange(self.burnin, ((self.ngibbs * self.thinrate) + 
            self.burnin), self.thinrate)

        # set paths to scripts and data
        self.inputDataPath = os.path.join(os.getenv('ALABASTORPROJDIR', default = '.'), 
            'Data')
        self.outputDataPath = os.path.join(os.getenv('ALABASTORPROJDIR', default = '.'), 
            'Results/mod2Results')

        print('input data path', self.inputDataPath, 'out', self.outputDataPath)

        ## set Data names
        self.basicdataFname = os.path.join(self.outputDataPath, 'basicdataMod2.pkl')
        self.gibbsFname = os.path.join(self.outputDataPath, 'gibbsMod2.pkl')
        self.paramsResFname = os.path.join(self.outputDataPath, 'parameterTableMod2.csv')

        ## Input data   
        self.inputDensityFname = os.path.join(self.inputDataPath, '20210924_densities.csv')
        self.inputSnapFname = os.path.join(self.inputDataPath, 'SnapTrapData.csv')
        self.inputTTFname = os.path.join(self.inputDataPath, 'TT_Data.csv')
        self.inputCameraFname = os.path.join(self.inputDataPath, 'Camera3Night.csv')
        self.inputEnvironFname = os.path.join(self.inputDataPath, 'environVarDevices.csv')

        ## PLOTS AND TABLE NAMES WRITTEN TO DIRECTORY
        self.varioPlotFname = os.path.join(self.outputDataPath, 'variogramPlot.png')
        self.seasonEffectFname = os.path.join(self.outputDataPath, 'seasonEffectPlot.png')
        self.missDensityFname = os.path.join(self.outputDataPath, 'missingDensity.csv')
        self.missSigmaFname = os.path.join(self.outputDataPath, 'missingSigma.csv')
        self.D0Fname = os.path.join(self.outputDataPath, 'D0_Table.csv')
        self.predictedObservedFname = os.path.join(self.outputDataPath, 
                'predictedObservedDen.png')
        self.rImmRatioFname = os.path.join(self.outputDataPath, 'rImmRatio.png')

        # pickle mcmcData for checkpointing
        self.outCheckingFname = os.path.join(self.outputDataPath,'out_checking_mod' + 
            str(self.modelID)  + '.pkl')

        print('params basic path', self.basicdataFname) 
        
        ## Set initial parameter values


        ## COVARIATE DATA DICTIONARY FOR DENSITY AND SIGMA
        self.xdatDictionary = {'InterceptGamma' : 0, 'InterceptBeta' : 1, 
                        'Elevation' : 2,
                        'TimeMast' : 3, 'SinWeek' : 4, 'CosWeek' : 5,
                        'DensityT_1' : 6, 'SigmaT_1' : 7, 'TimeMastSquared' : 8,
                        'weekWrpC' : 9}

        InterceptGamma = self.xdatDictionary['InterceptGamma']
        InterceptBeta = self.xdatDictionary['InterceptBeta']
        Elevation = self.xdatDictionary['Elevation']
        timeMast  = self.xdatDictionary['TimeMast']
        timeMastSquared  = self.xdatDictionary['TimeMastSquared']
        SineWeek  = self.xdatDictionary['SinWeek']
        CosineWeek  = self.xdatDictionary['CosWeek']
        DensityT_1  = self.xdatDictionary['DensityT_1']
        SigmaT_1  = self.xdatDictionary['SigmaT_1']
        weekWrpC = self.xdatDictionary['weekWrpC']
        ######################################################
        ######################################################
        ##
        ## ADJUST THIS TO SET COVARIATES FOR DENSITY AND SIGMA
        self.xDenIndx = np.array([InterceptGamma, Elevation, timeMast], dtype = int)
#        self.xDenIndx = np.array([InterceptGamma, Elevation, timeMast, SineWeek,
#            CosineWeek], dtype = int)

        ## SIGMA INDEX
        self.xSigIndx = np.array([InterceptBeta, Elevation], dtype = int)
        ##
        ######################################################
        ######################################################

        ## START DATE IS ONE WEEK BEYOND THE INITIAL POP SIZE ON 2019/3/9
        self.date0 = datetime.date(2019,3,15)


        ## GAMMA PARAMETERS FOR DENSITY KRIGING (EQ. 20 AND 21)
        ## DEN DEPEND INTERCEPT, ELEVATION AND GEN GAMMA COEFF
        self.gamm = np.array([0.15, -0.4, 595])       # [0.15, -0.4, 595])
        self.gammPrior = np.array([[0.15, 1.0],         # INTERCEPT
                            [-0.4, 1.0],     # HIGH ELEV EFFECT
                            [595, 30.]])          # TIME SINCE MAST EFFECT
#        self.gammPrior = np.array([[0.15, 0.5],         # INTERCEPT
#                            [-0.4, 0.05],     # HIGH ELEV EFFECT
#                            [595, 1]])          # TIME SINCE MAST EFFECT
        self.gammSearch = [0.06, 0.06, 3.5]  #0.03
        self.nGamm = len(self.gamm)

        self.bRatioThreshold = 0.75
        self.bRatioThresPrior = np.array([np.log(0.75), 0.3])
        self.bRatioThresSearch = 0.06

        ## COVARIANCE PARAMETERS FOR DENSITY
        self.phiDen = .473 #0.03    # DECAY
        self.phiDenPrior = [np.log(1), 1.5]
        self.phiDenSearch = 0.24    #0.18
        self.vDen = .5 # 0.075 #0.8     # VARIANCE FOR LOG OF PREDICTED DENSITY
        self.vDenPrior = [np.log(1.0), 0.75]
        self.vDenSearch = 0.24  #0.18

        ## GOMPERTZ POP GROWTH PARAMETER
        ## INITIAL VALUE: MAX RATE AND IMMIGRATION RATE
        self.r = np.array([0.045, .004])
        self.priorR = np.array([[np.log(.045), 0.3], [np.log(0.004), 0.25]])   # LOG NORMAL PRIOR
        self.searchR = [0.055, 0.055]


        ## TIME SINCE MAST: GENERALISED GAMMA PDF ON WEEKS FOR HIGH AND LOW RESPECTIVELY
        self.TM = np.array([[90.0, 1.07, 4.0],
                            [90.0, 1.0, 4.0]])
#        self.TM = np.array([127., 1.0, 3.7])
#        self.TM = np.array([15.0, 1.05, 5.0])
#        self.priorTMHigh = np.array([[np.log(90), 1],
#                                 [np.log(1.07), 0.001],
#                                 [np.log(4.0), .01]])
#        self.priorTMLow = np.array([[np.log(90), 1],
#                                 [np.log(1), 0.001],
#                                 [np.log(4.0), .01]])

        self.priorTM = [np.array([[np.log(90), 5.0],
                                 [np.log(1.04), 0.5],
                                 [np.log(4.0), 2.0]]),
                        np.array([[np.log(90), 5.0],
                                 [np.log(1), 0.5],
                                 [np.log(4.0), 1.0]])]

        self.searchTM = [0.015, 0.015, 0.015]    #0.80



#        self.priorTM = [np.array([[np.log(90), 1],
#                                 [np.log(1.07), 0.001],
#                                 [np.log(4.0), .01]]),
#                        np.array([[np.log(90), 1],
#                                 [np.log(1), 0.001],
#                                 [np.log(4.0), .01]])]

#        self.searchTM = [0.000004, 0.000002, 0.000001]    #0.80


        ## BETA PARAMETERS FOR KRIGING SIGMA (EQ. 9 AND 10)
        ## INTERCEPT, ELEV, T-1 SIGMA
        self.beta = np.array([6.0, -0.33])
        self.nBeta = len(self.beta)
        # BETA PRIORS
#        self.diag = np.diagflat(np.tile(100., self.nBeta))
#        self.vinvert = np.linalg.inv(self.diag)
#        self.betaPrior = [0, 10]    
#        self.betaSearch = .08   #0.08
        self.vInvertBeta = np.linalg.inv(np.diag(np.repeat(20, self.nBeta)))    
        self.ppart = np.dot(self.vInvertBeta, np.zeros(self.nBeta))
#        ## COVARIANCE PARAMETERS FOR SIGMA
#        self.phiSig = 0.03    # DISTANCE DECAY FOR SIGMA
#        self.phiSigPrior = [np.log(1), .5]
#        self.phiSigSearch = 0.2
        self.vSig = 4.0     # VARIANCE FOR LOG OF PREDICTED SIGMA

        self.vSigPrior = [3.0, 3.0]
#        self.vSigSearch = 0.2

        ## LATENT DENSITIES AND SIGMA VIA IMPUTATION
        ## INITIAL DENSITIES AT EACH GRID
        self.D0 = np.array([(0.05 * 100), (0.09 * 100),(5.4 * 100),
            (7.1 * 100), (10.0 * 100), (6.6 * 100)])

#        self.D0 = np.array([0.02, 0.02, 5.36, 5.2, 6.2, 6.4])
#        self.D0 = np.exp(np.random.normal(np.log(1.0), .25, 6))
        self.nD0 = len(self.D0)
        self.lnD0MeanPrior = [np.log(5.0), np.log(9.0), np.log(540), np.log(700), 
            np.log(1000), np.log(660)]
        self.lnD0SDPrior = np.repeat(1.0, 6)  #[0.5, 0.5, 0.99, 0.99, 0.99, 0.99]
#        self.lnD0Prior = [np.log(2.5), 1.0]
        self.searchD0 = 0.034
        ## INTIAL MISSING DENSITY ESTIMATES
#        self.missingDen = np.array([0.4, 0.2, 0.8, 0.5, 0.8, 1.3, 0.45])
###        self.missingDen = np.exp(np.random.normal(np.log(.3), .1, 7)) * 100.

####        self.missingDen = np.array([168.1661, 79.6869, 17.1263, 19.6997, 
####            79.3105, 17.0685, 27.1534])

        self.missingDen = np.array([168.1661, 79.6869, 17.1263, 19.6997, 
            79.3105, 17.0685, 27.1534])



        print('missingDen', self.missingDen)

        self.nMissingDen = len(self.missingDen)
        self.missDenPrior = [np.log(90), 1.0]
        self.searchMissDen = 0.034
#        ## INITIAL SIGMA AT EACH GRID
        self.sig0 = np.random.normal(35.0, 4.0, 6)
        self.nSig0 = len(self.sig0)
        ## INTIAL MISSING SIGMA ESTIMATES
        self.missingSig = np.random.normal(50.0, 5.0, 7)
        self.nMissingSig = len(self.missingSig)
        self.missSigPrior = [np.log(35), 1.0]
        self.searchMissSig = 0.12



        #################################
        ####################################### END USER MODIFICATION
        ##############################################################
        ## INDICES FOR COSINE AND SINE PLOT IN RESULTS
#        self.sinCosIndx = np.array([SineWeek, CosineWeek], dtype = int)


