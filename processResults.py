#!/usr/bin/env python

import numpy as np
import pylab as P
from scipy.stats.mstats import mquantiles
import prettytable
import pickle
import os
import datetime
from mcmcFX import gamma_pdf
from scipy.special import gammaln
from scipy.special import gamma
from scipy import stats
#from preProcessing import dwrpcauchy
from preProcessing import genGammaPDF
#from mcmcFX import rickerWeeksSites
from copy import deepcopy
from numba import njit
import pandas as pd

def logit(x):
    """
    Function (logit) to convert probability to real number
    """
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    """
    Function to convert real number to probability
    """
    return np.exp(x) / (1 + np.exp(x))

def distFX(x1, y1, x2, y2):
    """
    calc distance matrix between 2 sets of points
    resulting matrix dimensions = (len(x1), len(x2))
    """
    deltaX = np.add.outer(x1, -x2)
    deltaX_sq = deltaX**2
    deltaY = np.add.outer(y1, -y2)
    deltaY_sq = deltaY**2
    dist_2D = np.sqrt(deltaX_sq + deltaY_sq)
    return dist_2D

   
@njit
def processWeeksSites(i, nDenSites, D, diffWeeks,
        r, mArray_i, K_High_hi, K_Low_hi, bRatio_i,
        nDenSessions, bRatio_Up1, 
        h, rQuants3D, dPredict3D, immWeek_h, cc, immModel, k_up1,  minBRatio):
    ## LOOP THROUGH WEEKS IN SESSION i
    for k in range(diffWeeks[i]):
        ## LOOP DENSITY SITES (6)
        for j in range(nDenSites):
            ## DENSITY DEPENDENCE FOR EACH CURRENT SUB-SESSION
            if j < 2:
                if k == 0:
                    K_ij = k_up1[0]
                else:
                    K_ij = K_High_hi[k - 1]
            else:
                if k == 0:
                    K_ij = k_up1[1]
                else:
                    K_ij = K_Low_hi[k - 1]
            ## UPDATE 'a' IF HAVE MIGRATION CONDITIONS
            if (j < 2) & immModel:
                if (cc >= immWeek_h[0]) & (cc < immWeek_h[1]):            
                    if bRatio_i[k, j] > minBRatio:
                        bRatio_i[k, j] == minBRatio
                    a = r[1] * (bRatio_i[k, (j + 2)] / bRatio_i[k, j])
                    D[j] = (D[j] * np.exp(r[0] * (1.0  - (D[j] / K_ij))) +
                        (D[j + 4] * a))
                    rQuants3D[j, h, cc] = D[j + 4] * a
                else:
                    D[j] = D[j] * np.exp(r[0] * (1.0  - (D[j] / K_ij)))
            else:
                D[j] = D[j] * np.exp(r[0] * (1.0  - (D[j] / K_ij)))
            dPredict3D[j, h, cc] = D[j]
            ## IF NOT LAST WEEK IN SESSION, UPDATE B RATIO VALUES
            if (k < (diffWeeks[i] - 1)) & immModel: 
                if j < 2:
                    bRatio_i[(k+1), j] = D[j] / K_High_hi[k]
                elif j > 3:
                    bRatio_i[(k+1), (j - 2)] = D[j] / K_Low_hi[k]
            ##  IF LAST WEEK OF SESSION; POPULATE mArray_i
            if k == (diffWeeks[i] - 1):
                mArray_i[j] = np.log(D[j])
                if (i < (nDenSessions - 1)) & immModel:
                    if j < 2:
                        bRatio_Up1[j] = D[j] / K_High_hi[k]
                    elif j > 3:
                        bRatio_Up1[(j - 2)] = D[j] / K_Low_hi[k]
         
        cc += 1
    return(mArray_i, bRatio_i, bRatio_Up1,
            rQuants3D, dPredict3D, D, cc)





class ResultsProcessing(object):
    def __init__(self, gibbsobj, params, possumpath):

        self.params = params
        self.possumpath = possumpath
        self.gibbsobj = gibbsobj

        self.immModel = self.params.immModel    #np.in1d(self.params.modelID, self.params.immModels)

        self.ndat = self.params.ngibbs
        self.results = np.hstack([self.gibbsobj.gammGibbs,
                                self.gibbsobj.rGibbs,
                                self.gibbsobj.maxKGibbs,
                                np.expand_dims(self.gibbsobj.changeWkGibbs, 1),
                                self.gibbsobj.immWeekGibbs,
                                np.expand_dims(self.gibbsobj.vDenGibbs, 1),
                                np.expand_dims(self.gibbsobj.phiDenGibbs, 1),
                                self.gibbsobj.betaGibbs,
                                np.expand_dims(self.gibbsobj.vSigGibbs, 1)])
        self.npara = self.results.shape[1]
        print('total iter', ((self.ndat * self.params.thinrate) + 
                self.params.burnin))
        print('thin = ', self.params.thinrate, 'burn = ', self.params.burnin)

        self.denSites = ['high-A', 'high-B', 'low-A', 'low-B', 'mid-A', 'mid-B']
        print('densites', self.denSites)


    def makeTableFX(self):
        """
        ## MAKE TABLE WITH MEANS AND 90% CI
        """
        self.ncol = self.npara 
        ## MAKE NAMES ARRAY
        allCovariateList = np.array(list(self.params.xdatDictionary.keys()))
        growthNames = np.array(['r Intrinsic', 'r Immigration', 'maxK high', 'maxK low', 
            'ChangeWk', 'Imm start', 'Imm End'])
#        growthNames = np.array(['Growth rate', 'TM Mode', 'TM Scale', 'WrpC Mode',
#            'WrpC Scale'])
        namesGamma = np.append(np.array(['NegExpDecay High', 'NegExpDecay Low']), growthNames)
        namesGamma = np.append(namesGamma, 'nu Density')
        namesGamma = np.append(namesGamma, 'phi Density')

        namesBeta = np.append(allCovariateList[self.params.xSigIndx], 'nu Sigma')
#        namesBeta = np.append(namesBeta, 'phi Sigma')
        self.names = np.append(namesGamma, namesBeta)
        ## EMPTY TABLE TO POPULATE
        resultTable = np.zeros(shape = (3, self.ncol))
        resultTable[0] = np.round(np.mean(self.results[:, :self.ncol], axis = 0), 3)
        resultTable[1:3] = np.round(mquantiles(self.results[:, :self.ncol], 
                prob=[0.05, 0.95], axis = 0), 3)
        resultTable = resultTable.transpose()
        aa = prettytable.PrettyTable(['Names', 'Mean', 'Low CI', 'High CI'])
        for i in range(self.ncol):
            name = self.names[i]
            row = [name] + resultTable[i].tolist()
            aa.add_row(row)
        print(aa)
        self.summaryTable = resultTable.copy()
        (m, n) = resultTable.shape
        # create new structured array with columns of different types
        structured = np.empty((m,), dtype=[('Parameters', 'U12'),  
                ('Mean', np.float), ('Low CI', np.float), ('High CI', np.float)])
        # copy data over
        print('names', self.names, len(self.names))

        structured['Parameters'] = self.names
        structured['Mean'] = resultTable[:, 0]
        structured['Low CI'] = resultTable[:, 1]
        structured['High CI'] = resultTable[:, 2]
        np.savetxt(self.params.paramsResFname, structured, fmt=['%s', '%.2f', '%.2f', '%.2f'],
                    comments = '', delimiter = ',', 
                    header='Parameters, Mean, Low_CI, High_CI')






    def makeMissDensityTable(self):
        dateNames = []
        siteNames = []
        for i in range(self.params.nMissingDen):
            date_i = self.gibbsobj.uDates[self.gibbsobj.listSessSiteMissDen[i][0]]
            site_i = self.gibbsobj.denSites[self.gibbsobj.listSessSiteMissDen[i][1]]
            dateNames.append(str(date_i))     
            siteNames.append(site_i)
        ## EMPTY TABLE TO POPULATE
        resultTable = np.zeros(shape = (3, self.params.nMissingDen))
        resultTable[0] = np.round(np.mean(self.gibbsobj.missDenGibbs, axis = 0), 2)
        resultTable[1:3] = np.round(mquantiles(self.gibbsobj.missDenGibbs, 
                prob=[0.05, 0.95], axis = 0), 2)
        resultTable = resultTable.transpose()
        aa = prettytable.PrettyTable(['Date', 'Site', 'Mean', 'Low CI', 'High CI'])
        for i in range(self.params.nMissingDen):
            row = [dateNames[i]] + [siteNames[i]] + resultTable[i].tolist()
            aa.add_row(row)
        print(aa)

        self.stdMissDen = np.std(self.gibbsobj.missDenGibbs / 100., axis = 0)
        print('self.stdMissDen', self.stdMissDen)

        ## COPY TABLE FOR PLOTTING 95%CI LATER
        self.missDenTable = resultTable.copy()
        print('missDenTable', self.missDenTable)

        (m, n) = resultTable.shape
        # create new structured array with columns of different types
        structured = np.empty((m,), dtype=[('Date', 'U12'), ('Site', 'U12'), 
                ('Mean', np.float), ('Low CI', np.float), ('High CI', np.float)])
        # copy data over
        structured['Mean'] = resultTable[:, 0]
        structured['Low CI'] = resultTable[:, 1]
        structured['High CI'] = resultTable[:, 2]
        structured['Date'] = dateNames
        structured['Site'] = siteNames
        np.savetxt(self.params.missDensityFname , structured, fmt=['%s', '%s', '%.2f', '%.2f', '%.2f'],
                    comments = '', delimiter = ',', 
                    header='Date, Site, Mean, Low_CI, High_CI')


    def makeMissSigmaTable(self):
        dateNames = []
        siteNames = []
        for i in range(self.params.nMissingSig):
            date_i = self.gibbsobj.uDates[self.gibbsobj.listSessSiteMissDen[i][0]]
            site_i = self.gibbsobj.denSites[self.gibbsobj.listSessSiteMissDen[i][1]]
            dateNames.append(str(date_i))     
            siteNames.append(site_i)
        ## EMPTY TABLE TO POPULATE
        resultTable = np.zeros(shape = (3, self.params.nMissingSig))
        resultTable[0] = np.round(np.mean(self.gibbsobj.missSigGibbs, axis = 0), 2)
        resultTable[1:3] = np.round(mquantiles(self.gibbsobj.missSigGibbs, 
                prob=[0.05, 0.95], axis = 0), 2)
        resultTable = resultTable.transpose()
        aa = prettytable.PrettyTable(['Date', 'Site', 'Mean', 'Low CI', 'High CI'])
        for i in range(self.params.nMissingSig):
            row = [dateNames[i]] + [siteNames[i]] + resultTable[i].tolist()
            aa.add_row(row)
        print(aa)
        (m, n) = resultTable.shape
        # create new structured array with columns of different types
        structured = np.empty((m,), dtype=[('Date', 'U12'), ('Site', 'U12'), 
                ('Mean', np.float), ('Low CI', np.float), ('High CI', np.float)])
        # copy data over
        structured['Mean'] = resultTable[:, 0]
        structured['Low CI'] = resultTable[:, 1]
        structured['High CI'] = resultTable[:, 2]
        structured['Date'] = dateNames
        structured['Site'] = siteNames
        np.savetxt(self.params.missSigmaFname , structured, fmt=['%s', '%s', '%.2f', '%.2f', '%.2f'],
                    comments = '', delimiter = ',', 
                    header='Date, Site, Mean, Low_CI, High_CI')



    def makeInitialDensityTable(self):
        ## EMPTY TABLE TO POPULATE
        resultTable = np.zeros(shape = (3, self.gibbsobj.nUDenGrid))
        resultTable[0] = np.round(np.mean(self.gibbsobj.D0Gibbs, axis = 0), 2)
        resultTable[1:3] = np.round(mquantiles(self.gibbsobj.D0Gibbs, 
                prob=[0.05, 0.95], axis = 0), 2)
        resultTable = resultTable.transpose()
        aa = prettytable.PrettyTable(['Site', 'Mean', 'Low CI', 'High CI'])
        for i in range(self.gibbsobj.nUDenGrid):
            row = [self.gibbsobj.denSites[i]] + resultTable[i].tolist()
            aa.add_row(row)
        print(aa)
        (m, n) = resultTable.shape
        # create new structured array with columns of different types
        structured = np.empty((m,), dtype=[('Site', 'U12'), 
                ('Mean', np.float), ('Low CI', np.float), ('High CI', np.float)])
        # copy data over
        structured['Mean'] = resultTable[:, 0]
        structured['Low CI'] = resultTable[:, 1]
        structured['High CI'] = resultTable[:, 2]
        structured['Site'] = self.gibbsobj.denSites
        np.savetxt(self.params.D0Fname , structured, fmt=['%s', '%.2f', '%.2f', '%.2f'],
                    comments = '', delimiter = ',', 
                    header='Site, Mean, Low_CI, High_CI')







    def tracePlotFX(self):
        """
        plot diagnostic trace plots
        """
        nTotalFigs = self.ncol
        nfigures = np.int(np.ceil(nTotalFigs / 6.0))
        cc = 0
        for i in range(nfigures):
            P.figure(i)
            lastFigure = i == (nfigures - 1)
            for j in range(6):
                P.subplot(2,3,j+1)
                if cc < nTotalFigs:
                    nonZeroMask = self.results[:, cc] != 0
                    P.plot(self.results[nonZeroMask, cc])
                    P.title(self.names[cc])
                cc += 1
            P.show(block=lastFigure)

    def traceMissDen(self):
        """
        plot diagnostic trace plots
        """
        nTotalFigs = self.params.nMissingDen
        nfigures = np.int(np.ceil(nTotalFigs / 6.0))
        cc = 0
        for i in range(nfigures):
            P.figure(i)
            lastFigure = i == (nfigures - 1)
            for j in range(6):
                P.subplot(2,3,j+1)
                if cc < nTotalFigs:
                    P.plot(self.gibbsobj.missDenGibbs[:, cc])
                    P.title('Den: ' + 
                        'Ses ' + str(self.gibbsobj.listSessSiteMissDen[cc][0]) +
                        '; Sit ' + str(self.gibbsobj.listSessSiteMissDen[cc][1]))
                cc += 1
            P.show(block=lastFigure)



    def traceMissSig(self):
        """
        plot diagnostic trace plots
        """
        nTotalFigs = self.params.nMissingSig
        nfigures = np.int(np.ceil(nTotalFigs / 6.0))
        cc = 0
        for i in range(nfigures):
            P.figure(i)
            lastFigure = i == (nfigures - 1)
            for j in range(6):
                P.subplot(2,3,j+1)
                if cc < nTotalFigs:
                    P.plot(self.gibbsobj.missSigGibbs[:, cc])
                    P.title('Sig: ' + 
                        'Ses ' + str(self.gibbsobj.listSessSiteMissDen[cc][0]) +
                        '; Sit ' + str(self.gibbsobj.listSessSiteMissDen[cc][1]))
                cc += 1
            P.show(block=lastFigure)


    def traceD0(self):
        """
        plot diagnostic trace plots
        """
        nTotalFigs = self.params.nD0
        nfigures = np.int(np.ceil(nTotalFigs / 6.0))
        cc = 0
        for i in range(nfigures):
            P.figure(i)
            lastFigure = i == (nfigures - 1)
            for j in range(6):
                P.subplot(2,3,j+1)
                if cc < nTotalFigs:
                    P.plot(self.gibbsobj.D0Gibbs[:, cc])
                    P.title('D0: Site ' + str(cc))
                cc += 1
            P.show(block=lastFigure)



    ########            Write data to file
    ########
    def writeToFileFX(self):
        (m, n) = self.summaryTable.shape
        # create new structured array with columns of different types
        structured = np.empty((m,), dtype=[('Names', 'U12'), ('Mean', np.float),
                    ('Low CI', np.float), ('High CI', np.float)])
        # copy data over
        structured['Mean'] = self.summaryTable[:, 0]
        structured['Low CI'] = self.summaryTable[:, 1]
        structured['High CI'] = self.summaryTable[:, 2]
        structured['Names'] = self.names
        np.savetxt(self.params.paramsResFname, structured, fmt=['%s', '%.4f', '%.4f', '%.4f'], 
                    comments = '', delimiter = ',', 
                    header='Names, Mean, Low_CI, High_CI')


    def makeVariogram(self):
        nSamp = 100
        distSeq = np.linspace(0, 3.0, nSamp)
        varioMat = np.zeros((self.params.ngibbs, nSamp))
        for i in range(self.params.ngibbs): 
            varioMat[i] = self.gibbsobj.vDenGibbs[i] + (self.gibbsobj.vDenGibbs[i] * 
                (1.0 - np.exp(-self.gibbsobj.phiDenGibbs[i] * distSeq))) 
        vg = mquantiles(varioMat, prob = [0.5, 0.05, 0.95], axis = 0)
        P.figure(figsize = (11,9))
        ## VARIOGRAM FOR DENSITY
        ax = P.gca()    #P.subplot(1,2,1)
        lns0 = ax.plot(distSeq, vg[0], color = 'k', linewidth = 5)
        lns1 = ax.plot(distSeq, vg[1], color = 'k', linewidth = 2, ls = 'dashed')
        lns2 = ax.plot(distSeq, vg[2], color = 'k', linewidth = 2, ls = 'dashed')
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        P.title("A.", loc = 'left', fontsize = 15)
        ax.set_xlabel('Distance (km)', fontsize = 17)
        ax.set_ylabel('Semivariance', fontsize = 17)
#        P.show()
#        ## VARIOGRAM FOR SIGMA
#        for i in range(self.params.ngibbs): 
#            varioMat[i,] = self.gibbsobj.vSigGibbs[i] + (self.gibbsobj.vSigGibbs[i] * 
#                (1.0 - np.exp(-self.gibbsobj.phiSigGibbs[i] * distSeq))) 
#        vg = mquantiles(varioMat, prob = [0.5, 0.05, 0.95], axis = 0)
#        ax1 = P.subplot(1,2,2)
#        lns3 = ax1.plot(distSeq, vg[0], color = 'k', linewidth = 5)
#        lns4 = ax1.plot(distSeq, vg[1], color = 'k', linewidth = 2, ls = 'dashed')
#        lns5 = ax1.plot(distSeq, vg[2], color = 'k', linewidth = 2, ls = 'dashed')
#        for tick in ax1.xaxis.get_major_ticks():
#            tick.label.set_fontsize(14)
#        for tick in ax1.yaxis.get_major_ticks():
#            tick.label.set_fontsize(14)
#        P.title("B.", loc = 'left', fontsize = 15)
#        ax1.set_xlabel('Distance (km)', fontsize = 17)
#        ax1.set_ylabel('Semivariance', fontsize = 17)
        P.savefig(self.params.varioPlotFname, format='png', dpi = 600)
        P.show()
        print('Distance mat', np.round(self.gibbsobj.denDistMat, 2))



    def plotWrpC(self):
        allCovariateArr = np.array(list(self.params.xdatDictionary.keys()))
        modelCovArr = allCovariateArr[self.params.xDenIndx]

        seasonIndx = np.where(modelCovArr == 'weekWrpC')
        seasonPara = self.gibbsobj.gammGibbs[:, seasonIndx]
        nSteps = 100
        wkSeq = np.linspace(1,52.0, nSteps)
        wkMat = np.zeros((self.params.ngibbs, nSteps))
        wkRad = wkSeq/52.0 * 2.0 * np.pi
        for i in range(self.params.ngibbs):
            wrpCPDF_i = dwrpcauchy(wkRad, self.gibbsobj.wrpCGibbs[i,0],
                self.gibbsobj.wrpCGibbs[i,1])


            wkMat[i] = wrpCPDF_i * seasonPara[i]



        wkQuant = mquantiles(wkMat, prob = [0.5, 0.05, 0.95], axis = 0)
        P.figure(figsize=(11,9))
        ax1 = P.gca()
        lns3 = ax1.plot(wkSeq, wkQuant[0], color = 'k', linewidth = 5)
        lns4 = ax1.plot(wkSeq, wkQuant[1], color = 'k', linewidth = 2, ls = 'dashed')
        lns5 = ax1.plot(wkSeq, wkQuant[2], color = 'k', linewidth = 2, ls = 'dashed')
        for tick in ax1.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax1.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        ax1.set_xlabel('Time (weeks)', fontsize = 17)
        ax1.set_ylabel('Relative season effect', fontsize = 17)
        P.savefig(self.params.seasonEffectFname, format='png', dpi = 600)
        P.show()





    def plotSinCos(self):
        allCovariateArr = np.array(list(self.params.xdatDictionary.keys()))
        modelCovArr = allCovariateArr[self.params.xDenIndx]



        sinIndx = np.where(modelCovArr == 'SinWeek') 
        cosIndx = np.where(modelCovArr == 'CosWeek') 
        sinDat = self.gibbsobj.gammGibbs[:, sinIndx]
        cosDat = self.gibbsobj.gammGibbs[:, cosIndx]

        nSteps = 100
        wkSeq = np.linspace(1,52.0, nSteps)
        wkMat = np.zeros((self.params.ngibbs, nSteps))
        wkRad = wkSeq/52.0 * 2.0 * np.pi

        for i in range(self.params.ngibbs):
            wkMat[i] =  sinDat[i] * np.sin(wkRad) + cosDat[i] * np.cos(wkRad)
        wkQuant = mquantiles(wkMat, prob = [0.5, 0.05, 0.95], axis = 0)
        P.figure(figsize=(11,9))
        ax1 = P.gca()
        lns3 = ax1.plot(wkSeq, wkQuant[0], color = 'k', linewidth = 5)
        lns4 = ax1.plot(wkSeq, wkQuant[1], color = 'k', linewidth = 2, ls = 'dashed')
        lns5 = ax1.plot(wkSeq, wkQuant[2], color = 'k', linewidth = 2, ls = 'dashed')
        for tick in ax1.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax1.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        ax1.set_xlabel('Time (weeks)', fontsize = 17)
        ax1.set_ylabel('Relative season effect', fontsize = 17)
        P.savefig(self.params.seasonEffectFname, format='png', dpi = 600)
        P.show()


    def plotDensityDependence(self):
        ndat = len(self.gibbsobj.dateList) 
        wk = np.arange(ndat)
        self.quantsLow = np.zeros((ndat, 3))
        self.quantsHigh = np.zeros((ndat, 3))
        lenWk = 0
        cc = 0
        for i in range(self.gibbsobj.nDenSessions):
            tmWkSess = self.gibbsobj.weekMastList[i]
            lenWk += len(tmWkSess)

            ## LOOP THROUGH WEEKS IN SESSION i
            for k in range(self.gibbsobj.diffWeeks[i]):
                tmWk = tmWkSess[k]
            
                changeMask = cc >= self.gibbsobj.changeWkGibbs
                wkNegExp_cc = cc - self.gibbsobj.changeWkGibbs  #[changeMask]
                highMaxK = self.gibbsobj.maxKGibbs[:, 0]
                highNegExpK_cc = np.where(~changeMask, highMaxK, 
                    (np.exp(-self.gibbsobj.gammGibbs[:, 0] * wkNegExp_cc) * highMaxK)) 

#                print('wk', cc, 'diffwk', wkNegExp_cc[0],
#                    'highMaxK', highMaxK[0], 'highNegExpK_cc', highNegExpK_cc[0]) 


                lowMaxK = self.gibbsobj.maxKGibbs[:, 1]
                lowNegExpK_cc = np.where(~changeMask, lowMaxK, 
                    (np.exp(-self.gibbsobj.gammGibbs[:, 1] * wkNegExp_cc) * lowMaxK)) 

                ## DENSITY DEPENDENCE FOR EACH SUB-WEEK
                lowNegExpK_cc = lowNegExpK_cc / 100
                self.quantsLow[cc, 0] = mquantiles(lowNegExpK_cc, prob = .5)
                self.quantsLow[cc,1:] = mquantiles(lowNegExpK_cc, prob=[0.05, 0.95])
                ## HIGH ELEVATION
                highNegExpK_cc = highNegExpK_cc / 100
                self.quantsHigh[cc, 0] = mquantiles(highNegExpK_cc, prob = .5)
                self.quantsHigh[cc,1:] = mquantiles(highNegExpK_cc, prob=[0.05, 0.95])
                cc += 1
        dateLabels = self.gibbsobj.uDates[[0, 2, 4, 6]]
        reducedLabels = np.array(dateLabels).astype(str)

        P.Figure(figsize=(13,9))
#        P.subplot(1,2,1)
        ax1 = P.gca()
        lns1 = ax1.plot(self.gibbsobj.dateList, self.quantsLow[:, 0], 
            color = 'k', linewidth = 3, label = "Low and mid elevation")
        lns1 = ax1.plot(self.gibbsobj.dateList, self.quantsLow[:, 1], 
            color = 'k', linewidth = 1, ls= 'dashed')
        lns1 = ax1.plot(self.gibbsobj.dateList, self.quantsLow[:, 2], 
            color = 'k', linewidth = 1, ls= 'dashed')
#        P.subplot(1,2,2)
        lns1 = ax1.plot(self.gibbsobj.dateList, self.quantsHigh[:, 0], 
            color = 'b', linewidth = 3, label = "High elevation")
        lns1 = ax1.plot(self.gibbsobj.dateList, self.quantsHigh[:, 1], 
            color = 'b', linewidth = 1, ls= 'dashed')
        lns1 = ax1.plot(self.gibbsobj.dateList, self.quantsHigh[:, 2], 
            color = 'b', linewidth = 1, ls= 'dashed')
        for tick in ax1.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax1.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)

        ax1.set_xticks(dateLabels)
        ax1.set_xticklabels(reducedLabels, fontsize = 10, rotation=45)
        ax1.set_xlabel("Date", fontsize = 12)
        ax1.set_ylabel("Carrying capacity (rats ha$^{-1}$)", fontsize = 12)

        ax1.legend(loc = 'upper right')

        P.ylim(-.50, 25.)



        P.tight_layout()

        P.savefig(self.params.densityDepFname, format='eps', dpi = 600)
        P.show()

         
    def plotTimeMastEff(self):
        ndat = len(self.gibbsobj.dateList)
        wk = np.arange(ndat)
        quantsTMHigh = np.zeros((ndat, 3))
        quantsTMLow = np.zeros((ndat, 3))
        for i in range(ndat):
            tmpdfHigh = genGammaPDF((i+1.0), self.gibbsobj.tmHighGibbs[:,0], 
                    self.gibbsobj.tmHighGibbs[:,1], self.gibbsobj.tmHighGibbs[:,2])
            tmpdfLow = genGammaPDF((i+1.0), self.gibbsobj.tmLowGibbs[:,0], 
                    self.gibbsobj.tmLowGibbs[:,1], self.gibbsobj.tmLowGibbs[:,2])
            quantsTMHigh[i, 0] = np.mean(tmpdfHigh)
            quantsTMLow[i,1:] = mquantiles(tmpdfLow, prob=[0.05, 0.95])
            quantsTMLow[i, 0] = np.mean(tmpdfLow)
            quantsTMLow[i,1:] = mquantiles(tmpdfLow, prob=[0.05, 0.95])

        P.Figure(figsize=(11,9))
        ax1 = P.gca()
        lns1 = ax1.plot(self.gibbsobj.dateList, quantsTMHigh[:, 0], 
            color = 'b', linewidth = 3)
        lns2 = ax1.plot(self.gibbsobj.dateList, quantsTMHigh[:, 1], 
            color = 'b', linewidth = 1, ls= 'dashed')
        lns3 = ax1.plot(self.gibbsobj.dateList, quantsTMHigh[:, 2], 
            color = 'b', linewidth = 1, ls= 'dashed')
        lns4 = ax1.plot(self.gibbsobj.dateList, quantsTMLow[:, 0], 
            color = 'k', linewidth = 3)
        lns5 = ax1.plot(self.gibbsobj.dateList, quantsTMLow[:, 1], 
            color = 'k', linewidth = 1, ls= 'dashed')
        lns6 = ax1.plot(self.gibbsobj.dateList, quantsTMLow[:, 2], 
            color = 'k', linewidth = 1, ls= 'dashed')
        P.savefig(self.params.timeMastFname, format='png', dpi = 600)
        P.show()

    def plotMeanPred(self):
        denArr = self.gibbsobj.D.copy()
        denArr[self.gibbsobj.missIndx] = np.mean(self.gibbsobj.missDenGibbs, axis = 0)
        sigArr = self.gibbsobj.sigma.copy()
        sigArr[self.gibbsobj.maskMissSig] = np.mean(self.gibbsobj.missSigGibbs, axis = 0)
        meanMDen = self.gibbsobj.meanMDen / self.params.ngibbs
        meanMuSigma = self.gibbsobj.meanMuSigma / self.params.ngibbs

        P.figure()
        P.subplot(2,2,1)
        P.scatter(meanMDen, denArr)
        P.scatter(meanMDen[self.gibbsobj.missIndx], denArr[self.gibbsobj.missIndx], 
            color = 'r')
        P.xlabel('Predicted density', fontsize = 14)
        P.ylabel('Density', fontsize = 14)

        P.subplot(2,2,2)
        P.scatter(meanMuSigma, sigArr)
        P.scatter(meanMuSigma[self.gibbsobj.maskMissSig], 
            sigArr[self.gibbsobj.maskMissSig], color = 'r')
        P.xlabel('Predicted Sigma', fontsize = 14)
        P.ylabel('Sigma', fontsize = 14)

        P.subplot(2,2,3)
        P.scatter(np.log(meanMDen), np.log(meanMuSigma))
        P.scatter(np.log(meanMDen)[self.gibbsobj.missIndx], 
            np.log(meanMuSigma)[self.gibbsobj.maskMissSig], color = 'r')
        P.xlabel('Predicted ln Density', fontsize = 14)
        P.ylabel('Predicted ln Sigma', fontsize = 14)

        P.subplot(2,2,4)
        P.scatter(np.log(denArr), np.log(sigArr))
        P.scatter(np.log(denArr)[self.gibbsobj.missIndx], 
            np.log(sigArr)[self.gibbsobj.maskMissSig], color='r')
        P.xlabel('ln Density', fontsize = 14)
        P.ylabel('ln Sigma', fontsize = 14)
        P.show()


    def plotPredDenSite(self):
        self.denArr = self.gibbsobj.D.copy()
        meanMissDen = np.mean(self.gibbsobj.missDenGibbs, axis = 0)

        print('miss Indx', self.gibbsobj.listSessSiteMissDen)
        print('meanMissDen', np.round(meanMissDen, 4))


        missDates = self.gibbsobj.denDate[self.gibbsobj.missIndx]
        self.denArr[self.gibbsobj.missIndx] = meanMissDen

        siteOrderFromList = np.tile(self.gibbsobj.denSites,
            self.gibbsobj.nDenSessions)
        print('siteOrderFromList', siteOrderFromList)

        self.missDenBySiteList = []
        self.missDateBySiteList = []
        for i in range(len(self.gibbsobj.denSites)):
            dateList = []
            denList = []
            for j in range(len(self.gibbsobj.missIndx)):
                sess_j = self.gibbsobj.listSessSiteMissDen[j][0]
                site_j = self.gibbsobj.listSessSiteMissDen[j][1]
                if site_j == i:
                    dateList.append(self.gibbsobj.uDates[sess_j])
                    denList.append(meanMissDen[j])
    
                    print('list', self.gibbsobj.listSessSiteMissDen[j],
                        'sess', sess_j, 'site', site_j, 'miss den', 
                        np.round(meanMissDen[j],2))

            print('site', i, 'denList', denList)                        


            self.missDenBySiteList.append(denList)
            self.missDateBySiteList.append(dateList)

        print('self.missDateBySiteList', self.missDateBySiteList, 'self.missDenBySiteList',
            self.missDenBySiteList)

        meanMDen = (self.gibbsobj.meanMDen) / self.params.ngibbs


        dateLabels = self.gibbsobj.uDates[[0, 2, 4, 6]]
        reducedLabels = np.array(dateLabels).astype(str)

        plotOrder = [1,2,5,6,3,4]

        P.figure(figsize=(11,13))
        for i in range(len(self.gibbsobj.denSites)):
            P.subplot(3,2, plotOrder[i])
            site_i = self.gibbsobj.denSites[i]
            print('site_i', site_i)
            mask = self.gibbsobj.denGrid == site_i
            den = self.denArr[mask] 

            maskPred = siteOrderFromList == site_i
            predDen = meanMDen[maskPred]

            print('i', i, 'den', den / 100)
            print('i', i, 'predDen', predDen)

            P.plot(self.gibbsobj.uDates, predDen, 'o-', color = 'k',
                label = "Predicted density")
            P.plot(self.gibbsobj.uDates, den / 100., 'o-', color = 'r',
                label = "Measured density")
            P.plot(self.missDateBySiteList[i], 
                np.array(self.missDenBySiteList[i]) / 100, 'r*', markersize=12,
                label = "Data imputation")

            if i < 2:
                P.plot(self.gibbsobj.dateList, self.quantsHigh[:, 0], color = 'b', 
                    linewidth = 1, linestyle = 'dashed', label = 'Carrying capacity')
            else:
                P.plot(self.gibbsobj.dateList, self.quantsLow[:, 0], color = 'b', 
                    linewidth = 1, linestyle = 'dashed')
            P.ylim(-.50, 25.)
            P.title(self.denSites[i])
            

            ax = P.gca()
            if i == 0:
                ax.legend(loc = 'upper left')
            if (i == 0) | (i == 2) | (i == 4):
                ax.set_ylabel('Density (rats ha$^{-1}$)', fontsize = 12)
            else:
                ax.set_yticklabels([])
            if (i == 2) | (i == 3):
                ax.set_xticks(dateLabels)
                ax.set_xticklabels(reducedLabels, fontsize = 10, rotation=45)
                ax.set_xlabel("Date", fontsize = 12)
            else:
                ax.set_xticklabels([])
        P.savefig(self.params.predOneStepAheadFname, format='png', dpi = 600)
        P.show()

        #########################################
        ## RAW DATA PLOT
        ## GET 95% CI FOR DENSITY DATA FOR PLOT
        denDat = pd.read_csv(self.params.inputDensityFname, delimiter=',')
        denLCL = np.array(denDat[['lcl_D']]).flatten()
        denUCL = np.array(denDat[['ucl_D']]).flatten()
        denSE = np.array(denDat[['SE_D']]).flatten()
        denMEAN = np.array(denDat[['D']]).flatten()

        
        denNANMask = np.ma.masked_invalid(denMEAN)
        
        validMask = ~denNANMask.mask
        invalidMask = denNANMask.mask 

        print('denLCL', denLCL, 'denUCL', denUCL)
        print('not nan', np.ma.masked_invalid(denMEAN))
        print('validMask', validMask)
        print('invalidMask', invalidMask)


        denLCL[self.gibbsobj.missIndx] = self.missDenTable[:, 1] / 100.0
        denUCL[self.gibbsobj.missIndx] = self.missDenTable[:, 2] / 100.00
        denSE[self.gibbsobj.missIndx] = self.stdMissDen

        P.figure(figsize=(11, 13))
        for i in range(len(self.gibbsobj.denSites)):
            P.subplot(3,2, plotOrder[i])
            site_i = self.gibbsobj.denSites[i]
            mask = self.gibbsobj.denGrid == site_i
            

            validMaskSite = validMask[mask]
            invalidMaskSite = invalidMask[mask]

            denSite = self.denArr[mask] / 100.0
            den = denSite[validMaskSite]
            lclSite = denLCL[mask]
            lcl = den - lclSite[validMaskSite]
            uclSite = denUCL[mask] 
            ucl = uclSite[validMaskSite] - den 
#            se = denSE[validMaskSite] * 2.0 
            validDates = self.gibbsobj.uDates[validMaskSite]


####            den = self.denArr[mask] / 100.0 
####            lcl = den - denLCL[mask] 
####            ucl = denUCL[mask] - den 
####            se = denSE[mask] * 2.0 

            print('site_i', site_i, 'den', den, 'ucl', ucl)

#            maskPred = siteOrderFromList == site_i
#            predDen = meanMDen[maskPred]

            ax = P.gca()

            lns1 = ax.errorbar(validDates, den, yerr = [lcl, ucl], 
                ecolor = 'red', fmt = 'ro', ms = 4, ls = '-', mew = 2,
                markeredgecolor = 'red', capsize = 4, label = 'Mean and 95% CI')
###            P.plot(self.gibbsobj.uDates, den / 100., 'o-', color = 'r',
###                label = "Measured density")
            lns1 = ax.plot(self.missDateBySiteList[i], 
                np.array(self.missDenBySiteList[i]) / 100, 'ro', markersize=10, 
                fillstyle='none', mew = 2, label = "Missing data")

            print('########### i', i, 'miss den', self.missDenBySiteList[i])
            P.title(self.denSites[i])

            P.ylim(-.750, 25.)

#            ax = P.gca()
            if i == 0:
                ax.legend(loc = 'upper left', fontsize=13)


            if (i == 0) | (i == 2) | (i == 4):
                ax.set_ylabel('Density (rats ha$^{-1}$)', fontsize = 13)

                ax.tick_params(axis='y', labelsize=13)


            else:
                ax.set_yticklabels([])
            if (i == 2) | (i == 3):

#            if (i == 0) | (i == 3):
#                ax.set_ylabel('Density (rats ha$^{-1}$)', fontsize = 12)
#            else:
#                ax.set_yticklabels([])
#            if i > 2:
                ax.set_xticks(dateLabels)
                ax.set_xticklabels(reducedLabels, fontsize = 13, rotation=45)
                ax.set_xlabel("Date", fontsize = 13)

            else:
                ax.set_xticklabels([])
        P.savefig(self.params.rawDensityFname, format='eps', dpi = 600)


        P.show()


    def simulateDensity(self):
        nWk = len(self.gibbsobj.dateList)
        predQuants3D = np.zeros((self.gibbsobj.nDenSites, self.params.ngibbs, 
            self.gibbsobj.nDenSessions))
        self.rQuants3D = np.zeros((2, self.params.ngibbs, nWk))
        self.dPredict3D = np.zeros((6, self.params.ngibbs, nWk))

        plotOrder = [1,2,5,6,3,4]



        for h in range(self.params.ngibbs):
            cc = 0
            

            changeWk_h = self.gibbsobj.changeWkGibbs[h]
            diffWk_h = cc - self.gibbsobj.changeWkGibbs[h] 
            highMaxK_h = self.gibbsobj.maxKGibbs[h, 0]
            lowMaxK_h = self.gibbsobj.maxKGibbs[h, 1]
            gamm_h = self.gibbsobj.gammGibbs[h]

            immWeek_h = self.gibbsobj.immWeekGibbs[h]

            k_up1 = np.zeros(2)                 ## K FOR FIRST WEEK OF NEXT SESS            
            for i in range(self.gibbsobj.nDenSessions):
            
                ## MASK WEEKS FOR EXPONENTIAL DECAY
                wkMask = self.gibbsobj.weekMastList[i] >= changeWk_h
                ## WEEK OF EXPONENTIAL DECAY
                wkNegExp_hi = self.gibbsobj.weekMastList[i] - changeWk_h
                ## NEG EXP PRED AND K
                negExpHigh_hi = np.exp(-wkNegExp_hi * gamm_h[0])

                K_High_hi = np.where(~wkMask, highMaxK_h, (negExpHigh_hi * highMaxK_h)) 
                negExpLow_hi = np.exp(-wkNegExp_hi * gamm_h[1])
                K_Low_hi = np.where(~wkMask, lowMaxK_h, (negExpLow_hi * lowMaxK_h)) 

                ## K FOR FIRST WEEK OF NEXT SESS
                if i == 0:
                    k_up1[0] = highMaxK_h                    
                    k_up1[1] = lowMaxK_h

                if (i == 0):
                    bRatio_i = np.zeros((self.gibbsobj.diffWeeks[i], 4))
                    D = self.gibbsobj.D0Gibbs[h].copy()
                    for j in range(2):
                        D_mid = self.gibbsobj.D0Gibbs[h, (j + 4)]
                        k_mid = lowMaxK_h
                        bRatio_i[0, (j + 2)] = D_mid / k_mid 
                        ## HIGH SITE bRatio
                        D_High= self.gibbsobj.D0Gibbs[h, j]
                        k_High = highMaxK_h
                        bRatio_i[0, j] = D_High / k_High
                else:
                    D = self.dPredict3D[:, h, (cc -1)].copy()

                bRatio_Up1 = np.zeros(4)
                mArray_i = np.zeros(self.gibbsobj.nDenSites)
                r = self.gibbsobj.rGibbs[h]
                ## NUMBA LOOP THROUGH WEEKS AND SITES
                (mArray_i, bRatio_i,
                    bRatio_Up1, self.rQuants3D, 
                    self.dPredict3D, D, cc) = processWeeksSites(i,
                    self.gibbsobj.nDenSites, D, 
                    self.gibbsobj.diffWeeks,
                    r, mArray_i, K_High_hi, 
                    K_Low_hi, bRatio_i,
                    self.gibbsobj.nDenSessions, bRatio_Up1, 
                    h, self.rQuants3D, self.dPredict3D, 
                    immWeek_h, cc, self.params.immModel, k_up1, self.params.minBRatio)
                ## PUT THE B RATIO AND B MID TEST VALUES IN NEXT UP SESSION FOR TIME 0
                if (i < (self.gibbsobj.nDenSessions - 1)) & self.params.immModel:
                    bRatio_i = np.zeros((self.gibbsobj.diffWeeks[i + 1], 4))
                    bRatioMidTest_i  = np.zeros((self.gibbsobj.diffWeeks[i + 1], 2))
                    for j in range(self.gibbsobj.nDenSites):
                        if j < 2:
                            bRatio_i[0, j] = bRatio_Up1[j]
                        elif j > 3:
                            bRatio_i[0, (j - 2)] = bRatio_Up1[j-2]
                predQuants3D[:, h, i] = np.exp(mArray_i) / 100.0
                k_up1[0] = K_High_hi[-1]
                k_up1[1] = K_Low_hi[-1]

        P.figure(figsize=(11,13))
        dateLabels = self.gibbsobj.uDates[[0, 2, 4, 6]]
        reducedLabels = np.array(dateLabels).astype(str)
        print('dateLabels', dateLabels, 'reduced', reducedLabels)
        for ss in range(self.gibbsobj.nDenSites):
            quant_ss = np.zeros((3, self.gibbsobj.nDenSessions))
            quant_ss[0] = mquantiles(predQuants3D[ss], prob = 0.5, axis = 0)
#            quant_ss[0] = np.mean(predQuants3D[ss], axis = 0)
            quant_ss[1:] = mquantiles(predQuants3D[ss], prob = [0.025, 0.975], axis = 0)
            site_ss = self.gibbsobj.denSites[ss]
            mask = self.gibbsobj.denGrid == site_ss
            denObs = self.denArr[mask] / 100.
            P.subplot(3,2, plotOrder[ss])
            if ss < 2:
                P.plot(self.gibbsobj.dateList, self.quantsHigh[:, 0], color = 'b', 
                    linewidth = 1, linestyle = 'dashed', label = 'Carrying capacity')
            else:
                P.plot(self.gibbsobj.dateList, self.quantsLow[:, 0], color = 'b', 
                    linewidth = 1, linestyle = 'dashed')
            P.plot(self.gibbsobj.uDates, quant_ss[0], 'o-', color = 'k', linewidth = 3,
                label = 'Predicted density')
            P.plot(self.gibbsobj.uDates, quant_ss[1], color = 'k', linewidth = 1,
                label = '(95% CI)')
            P.plot(self.gibbsobj.uDates, quant_ss[2], color = 'k', linewidth = 1)
            P.plot(self.gibbsobj.uDates, denObs, 'o', color = 'r', 
                label = 'Observed density')
            missDensity = np.array(self.missDenBySiteList[ss]) / 100.
            P.plot(self.missDateBySiteList[ss], missDensity, 'r*',
                markersize=12, label = 'Data imputation')
            P.ylim(-.50, 25.)
            P.title(self.denSites[ss])
            ax = P.gca()
            if ss == 0:
                ax.legend(loc = 'upper left')


            if (ss == 0) | (ss == 2) | (ss == 4):
                ax.set_ylabel('Density (rats ha$^{-1}$)', fontsize = 12)
            else:
                ax.set_yticklabels([])
            if (ss == 2) | (ss == 3):
                ax.set_xticks(dateLabels)
                ax.set_xticklabels(reducedLabels, fontsize = 10, rotation=45)
                ax.set_xlabel("Date", fontsize = 12)
            else:
                ax.set_xticklabels([])
        P.tight_layout()
        P.savefig(self.params.predictedObservedFname, format='png', dpi = 600)
        P.show()
        
        ###########################################
        #####   IMMIGRATION PLOT
        P.figure(figsize = (11,9))
        ax = P.gca()
        wk0 = 0
        linecol = ['k', 'r']
        site = ['-A', '-B']
        for ab in range(2):
            meanR = np.mean(self.rQuants3D[ab], axis = 0)
            self.getMovingAverage(meanR)
            qR = mquantiles(self.rQuants3D[ab], prob = (0.05, 0.95), axis = 0)
            P.plot(self.gibbsobj.dateList, meanR / 100. , color = linecol[ab], 
                linewidth = 3, label = 'high' + site[ab])
        ax.set_xticks(dateLabels)
        ax.set_xticklabels(reducedLabels, fontsize = 15, rotation=45)
        ax.set_xlabel("Date", fontsize = 15)
        ax.set_ylabel("Rats immigrating per week (ha$^{-1}$)", fontsize = 15)
        ax.legend(loc = 'upper left', fontsize=15)

        ax.tick_params(axis='y', labelsize=15)
        P.tight_layout()

        P.savefig(self.params.rImmRatioFname, format='eps', dpi = 600)
        P.show()

        ##########################################
        ####    FULL SIM PLOT



        P.figure(figsize=(11,13))
        nWeeks = len(self.gibbsobj.dateList)
        for ss in range(self.gibbsobj.nDenSites):
            quant_ss = np.zeros((3, nWeeks))
            quant_ss[0] = mquantiles(self.dPredict3D[ss] / 100.0, prob = 0.5, axis = 0)
#            quant_ss[0] = np.mean(self.dPredict3D[ss] / 100.0, axis = 0)
            quant_ss[1:] = mquantiles(self.dPredict3D[ss] / 100.0, prob = [0.025, 0.975], 
                axis = 0)
            site_ss = self.gibbsobj.denSites[ss]

            print(site_ss, 'Max mean density =', np.round(np.max(quant_ss[0]), 2),
                'Max 95%CI den', np.round(np.max(quant_ss[2]), 2))

            mask = self.gibbsobj.denGrid == site_ss
            denObs = self.denArr[mask] / 100.0
            P.subplot(3,2, plotOrder[ss])
            if ss < 2:
                P.plot(self.gibbsobj.dateList, self.quantsHigh[:, 0], color = 'b', 
                    linewidth = 1, linestyle = 'dashed', label = 'Carrying capacity')
            else:
                P.plot(self.gibbsobj.dateList, self.quantsLow[:, 0], color = 'b', 
                    linewidth = 1, linestyle = 'dashed')

            P.plot(self.gibbsobj.dateList, quant_ss[0], color = 'k', linewidth = 3,
                label = 'Predicted density')
            P.plot(self.gibbsobj.dateList, quant_ss[1], color = 'k', linewidth = 1,
                label = '(95% CI)')
            P.plot(self.gibbsobj.dateList, quant_ss[2], color = 'k', linewidth = 1)
            P.plot(self.gibbsobj.uDates, denObs, 'o', color = 'r', 
                label = 'Observed density')
            missDensity = np.array(self.missDenBySiteList[ss]) / 100.
            P.plot(self.missDateBySiteList[ss], missDensity, 'r*',
                markersize=12, label = 'Data imputation')
            P.ylim(-.50, 25.)
            P.title(self.denSites[ss])
            ax = P.gca()
            if ss == 0:
                ax.legend(loc = 'upper left')
            if (ss == 0) | (ss == 2) | (ss == 4):
                ax.set_ylabel('Density (rats ha$^{-1}$)', fontsize = 12)
            else:
                ax.set_yticklabels([])
            if (ss == 2) | (ss == 3):
                ax.set_xticks(dateLabels)
                ax.set_xticklabels(reducedLabels, fontsize = 10, rotation=45)
                ax.set_xlabel("Date", fontsize = 12)
            else:
                ax.set_xticklabels([])
        P.tight_layout()
        P.savefig(self.params.fullSimFname, format='eps', dpi = 600)
        P.show()

    def highPlots(self, doImmig):
        ################################################################
        ####    HIGH SITES - FULL SIM PLOTS - EXPLORE IMMIGRATION EFFECT
        nWk = len(self.gibbsobj.dateList)
        predQuants3D = np.zeros((self.gibbsobj.nDenSites, self.params.ngibbs, self.gibbsobj.nDenSessions))
        rQuants3D = np.zeros((2, self.params.ngibbs, nWk))
        dPredict3D = np.zeros((self.gibbsobj.nDenSites, self.params.ngibbs, nWk))
        nSites = 2
        plotOrder = [1,2]
        print('do Immig', doImmig)
        dateLabels = self.gibbsobj.uDates[[0, 2, 4, 6]]
        reducedLabels = np.array(dateLabels).astype(str)

        for h in range(self.params.ngibbs):
            cc = 0
            changeWk_h = self.gibbsobj.changeWkGibbs[h]
            diffWk_h = cc - self.gibbsobj.changeWkGibbs[h] 
            highMaxK_h = self.gibbsobj.maxKGibbs[h, 0]
            lowMaxK_h = self.gibbsobj.maxKGibbs[h, 1]
            gamm_h = self.gibbsobj.gammGibbs[h]

            immWeek_h = self.gibbsobj.immWeekGibbs[h]

            k_up1 = np.zeros(2)                 ## K FOR FIRST WEEK OF NEXT SESS            
            for i in range(self.gibbsobj.nDenSessions):
            
                ## MASK WEEKS FOR EXPONENTIAL DECAY
                wkMask = self.gibbsobj.weekMastList[i] >= changeWk_h
                ## WEEK OF EXPONENTIAL DECAY
                wkNegExp_hi = self.gibbsobj.weekMastList[i] - changeWk_h
                ## NEG EXP PRED AND K
                negExpHigh_hi = np.exp(-wkNegExp_hi * gamm_h[0])

                K_High_hi = np.where(~wkMask, highMaxK_h, (negExpHigh_hi * highMaxK_h)) 
                negExpLow_hi = np.exp(-wkNegExp_hi * gamm_h[1])
                K_Low_hi = np.where(~wkMask, lowMaxK_h, (negExpLow_hi * lowMaxK_h)) 

                ## K FOR FIRST WEEK OF NEXT SESS
                if i == 0:
                    k_up1[0] = highMaxK_h                    
                    k_up1[1] = lowMaxK_h

                if (i == 0):
                    bRatio_i = np.zeros((self.gibbsobj.diffWeeks[i], 4))
                    D = self.gibbsobj.D0Gibbs[h].copy()
                    for j in range(2):
                        D_mid = self.gibbsobj.D0Gibbs[h, (j + 4)]
                        k_mid = lowMaxK_h
                        bRatio_i[0, (j + 2)] = D_mid / k_mid 
                        ## HIGH SITE bRatio
                        D_High= self.gibbsobj.D0Gibbs[h, j]
                        k_High = highMaxK_h
                        bRatio_i[0, j] = D_High / k_High
                else:
                    D = dPredict3D[:, h, (cc -1)].copy()

                bRatio_Up1 = np.zeros(4)
                mArray_i = np.zeros(self.gibbsobj.nDenSites)
                r = self.gibbsobj.rGibbs[h]
                ## NUMBA LOOP THROUGH WEEKS AND SITES
                (mArray_i, bRatio_i,
                    bRatio_Up1, rQuants3D, 
                    dPredict3D, D, cc) = processWeeksSites(i,
                    self.gibbsobj.nDenSites, D, 
                    self.gibbsobj.diffWeeks,
                    r, mArray_i, K_High_hi, 
                    K_Low_hi, bRatio_i,
                    self.gibbsobj.nDenSessions, bRatio_Up1, 
                    h, rQuants3D, dPredict3D, 
                    immWeek_h, cc, doImmig, k_up1, self.params.minBRatio)
                ## PUT THE B RATIO AND B MID TEST VALUES IN NEXT UP SESSION FOR TIME 0
                if (i < (self.gibbsobj.nDenSessions - 1)) & self.params.immModel:
                    bRatio_i = np.zeros((self.gibbsobj.diffWeeks[i + 1], 4))
                    bRatioMidTest_i  = np.zeros((self.gibbsobj.diffWeeks[i + 1], 2))
                    for j in range(self.gibbsobj.nDenSites):
                        if j < 2:
                            bRatio_i[0, j] = bRatio_Up1[j]
                        elif j > 3:
                            bRatio_i[0, (j - 2)] = bRatio_Up1[j-2]
                predQuants3D[:, h, i] = np.exp(mArray_i) / 100.0
                k_up1[0] = K_High_hi[-1]
                k_up1[1] = K_Low_hi[-1]

        P.figure(figsize=(10,5))
        nWeeks = len(self.gibbsobj.dateList)
        for ss in range(2):
            quant_ss = np.zeros((3, nWeeks))
            quant_ss[0] = mquantiles(dPredict3D[ss] / 100.0, prob = 0.5, axis = 0)
#            quant_ss[0] = np.mean(self.dPredict3D[ss] / 100.0, axis = 0)
            quant_ss[1:] = mquantiles(dPredict3D[ss] / 100.0, prob = [0.025, 0.975], 
                axis = 0)
            site_ss = self.denSites[ss]

            print(site_ss, 'Max mean density =', np.round(np.max(quant_ss[0]), 2),
                'Max 95%CI den', np.round(np.max(quant_ss[2]), 2))

            mask = self.gibbsobj.denGrid == self.gibbsobj.denSites[ss]
            denObs = self.denArr[mask] / 100.0
            P.subplot(1,2, plotOrder[ss])
            P.plot(self.gibbsobj.dateList, self.quantsHigh[:, 0], color = 'b', 
                linewidth = 1, linestyle = 'dashed', label = 'Carrying capacity')
            P.plot(self.gibbsobj.dateList, quant_ss[0], color = 'k', linewidth = 3,
                label = 'Predicted density')
            P.plot(self.gibbsobj.dateList, quant_ss[1], color = 'k', linewidth = 1,
                label = '(95% CI)')
            P.plot(self.gibbsobj.dateList, quant_ss[2], color = 'k', linewidth = 1)
            P.plot(self.gibbsobj.uDates, denObs, 'o', color = 'r', 
                label = 'Observed density')
            missDensity = np.array(self.missDenBySiteList[ss]) / 100.
            P.plot(self.missDateBySiteList[ss], missDensity, 'r*',
                markersize=12, label = 'Data imputation')
            P.ylim(-.50, 25.)
            P.title(site_ss)
            ax = P.gca()
            if ss == 0:
                ax.legend(loc = 'upper left')
            if (ss == 0):
                ax.set_ylabel('Density (rats ha$^{-1}$)', fontsize = 12)
            else:
                ax.set_yticklabels([])
            ax.set_xticks(dateLabels)
            ax.set_xticklabels(reducedLabels, fontsize = 10, rotation=45)
            ax.set_xlabel("Date", fontsize = 12)
        P.tight_layout()
        P.savefig(self.params.highPlotFname, format='eps', dpi = 600)
        P.show()





    def getMovingAverage(self, meanR):
        n = len(self.gibbsobj.dateList)
        self.maR = np.zeros(n)
        nSeq = np.arange(n)
        for wk in range(n):
            diffWk = np.abs(wk - nSeq)
            diffMask = diffWk <= 8
            rSel = meanR[diffMask]
            self.maR[wk] = np.mean(rSel)


    def calcDIC(self):
        twoMeanD = 2.0 * np.mean(self.gibbsobj.DICGibbs)
        ## GET D EVALUATED AT MEAN PARAMETERS
        changeWk = np.mean(self.gibbsobj.changeWkGibbs)
        highMaxK = np.mean(self.gibbsobj.maxKGibbs[:, 0])
        lowMaxK = np.mean(self.gibbsobj.maxKGibbs[:, 1])
        gamm = np.mean(self.gibbsobj.gammGibbs, axis = 0)
        D0 = np.mean(self.gibbsobj.D0Gibbs, axis = 0)
        missDensity = np.mean(self.gibbsobj.missDenGibbs, axis = 0)
        immWeek = np.mean(self.gibbsobj.immWeekGibbs, axis = 0)
        r = np.mean(self.gibbsobj.rGibbs, axis = 0)
        ## MEAN COVARIANCE MATRIX
        vDen = np.mean(self.gibbsobj.vDenGibbs)
        phiDen = np.mean(self.gibbsobj.phiDenGibbs)
        cor = np.exp(-phiDen * self.gibbsobj.denDistMat)
        cov = vDen * cor
        ## LOOP THRU ALL MISSING DATA TO POPULATE PROPOSED DENSITY
        D_list = deepcopy(self.gibbsobj.listDensity)
        lnD_list = deepcopy(self.gibbsobj.listLnDensity)
        for mm in range(self.params.nMissingDen):
            sess_mm = self.gibbsobj.listSessSiteMissDen[mm][0]
            site_mm = self.gibbsobj.listSessSiteMissDen[mm][1]
            D_list[sess_mm][site_mm] = missDensity[mm]
            lnD_list[sess_mm][site_mm] = np.log(missDensity[mm])
        K_List = []
        k_up1 = np.zeros(2)                 ## K FOR FIRST WEEK OF NEXT SESS
        week = 0
        ## SET UP EMPTY ARRAY LIST FOR BRATIO - LOOP SESSIONS (7)
        for i in range(self.gibbsobj.nDenSessions):
            ## MASK WEEKS FOR EXPONENTIAL DECAY
            wkMask = self.gibbsobj.weekMastList[i] >= changeWk
            ## WEEK OF EXPONENTIAL DECAY
            wkNegExp = self.gibbsobj.weekMastList[i] - changeWk
            ## NEG EXP PRED AND K
            negExpHigh = np.exp(-wkNegExp * gamm[0])
            K_High_i = np.where(~wkMask, highMaxK, (negExpHigh * highMaxK)) 
            negExpLow = np.exp(-wkNegExp * gamm[1])
            K_Low_i = np.where(~wkMask, lowMaxK, (negExpLow * lowMaxK)) 
            ## POPULATE K 2-D ARRAY
            K_2D = np.zeros((self.gibbsobj.diffWeeks[i], 2))
            K_2D[:, 0] = K_High_i
            K_2D[:, 1] = K_Low_i
            K_List.append(K_2D)
        bRatio_i = np.zeros(4)
        for j in range(2):
            D_mid = D0[j + 4]
            k_mid = lowMaxK
            bRatio_i[j + 2] = D_mid / k_mid 
            ## HIGH SITE bRatio
            D_High= D0[j]
            k_High = highMaxK
            bRatio_i[j] = D_High / k_High
        D = np.zeros(self.gibbsobj.nDenSites)
        llik = 0.0
        week = 0
        ## SET UP EMPTY ARRAY LIST FOR BRATIO - LOOP SESSIONS (7)
        for i in range(self.gibbsobj.nDenSessions):
            K_High_i = K_List[i][:, 0]
            K_Low_i = K_List[i][:, 1]
            mArray = np.zeros(self.gibbsobj.nDenSites)
            ## LOOP THROUGH WEEKS IN SESSION i (SIMULATION)
            for k in range(self.gibbsobj.diffWeeks[i]):
                ## LOOP THROUGH DEN SITES
                for j in range(self.gibbsobj.nDenSites):
                    if i == 0:
                        if k == 0:                              ## SESS 0, WEEK 0
                            D[j] = D0[j]                   ## USE D0
                            if j < 2:
                                K_ij = highMaxK
                            elif j >= 2:
                                K_ij = lowMaxK
                        elif k > 0:                             ## SESS 0, WEEK > 0
                            if j < 2:
                                K_ij = K_High_i[k - 1]
                            elif j >= 2:
                                K_ij = K_Low_i[k - 1]
                    elif i > 0:                                 
                        if k == 0:                              ## SESS > 0, WEEK 0
                            D[j] = D_list[(i - 1)][j] 
                            if j < 2:
                                K_ij = K_List[i - 1][-1, 0]
                            elif j >= 2:
                                K_ij = K_List[i - 1][-1, 1]
                        elif k > 0:                             ## SESS > 0, WEEK > 0
                            if j < 2:
                                K_ij = K_High_i[k-1]
                            elif j >= 2:
                                K_ij = K_Low_i[k-1]
                    if (j < 2) & self.params.immModel:
                        if (week >= immWeek[0]) & (week <= immWeek[1]):
                            ## RICKER UPDATE OF DENSITY WITH IMMIGRATION
                            a = (r[1] * (bRatio_i[j + 2] / bRatio_i[j]))
                            D[j] = (D[j] * np.exp(r[0] * (1.0  - (D[j] / K_ij))) +
                                (D[j + 4] * a))
                        ## HIGH ELEV BUT NOT IN IMMIGRATION PERIOD
                        else:
                            D[j] = D[j] * np.exp(r[0] * (1.0  - (D[j] / K_ij)))
                    else:
                        ## RICKER WITH NO IMMIGRATION
                        D[j] = D[j] * np.exp(r[0] * (1.0  - (D[j] / K_ij)))
                    ## IF NOT LAST WEEK IN SESSION, UPDATE B RATIO VALUES
                    if k < (self.gibbsobj.diffWeeks[i] - 1): 
                        if j < 2:
                            bRatio_i[j] = D[j] / K_High_i[k]
                        elif j > 3:
                            bRatio_i[j - 2] = D[j] / K_Low_i[k]
                    ##  IF LAST WEEK OF SESSION; POPULATE mArray_i
                    if k == (self.gibbsobj.diffWeeks[i] - 1):
                        mArray[j] = np.log(D[j])
                week += 1    
            llik += stats.multivariate_normal.logpdf(lnD_list[i], mArray, cov)
        D_meanTheta = -2.0 * llik
        DIC = twoMeanD - D_meanTheta
        PD = (twoMeanD / 2) - D_meanTheta
        print('#########################################')
        print('###  DIC =', DIC)
        print('###  PD =', PD)
        print('#########################################')
        print('#########################################')
                


    def getRMSE(self, lowTest, highTest):
        """
        ## GET RMSE FROM CROSSVALIDATION
        """
        nSE = highTest - lowTest + 1
        seArray = np.zeros(nSE)
        meanErrArray = np.zeros(nSE)
        for i in range(nSE):
            fname = os.path.join(self.params.outputDataPath, 
                'cvResultModel' + str(i) + '.csv')
            dat_i = np.genfromtxt(fname,  delimiter=',', names=True, dtype=['i8', 
                'f8', 'f8'])
            seArray[i] = dat_i['cvErrorSq']
            meanErrArray[i] = dat_i['cvMeanErr']

        rmse = np.sqrt(np.sum(seArray) / self.params.ngibbs / nSE)

        rSqPred = 1.0 - (np.sum(seArray) / np.sum(meanErrArray))
        print('#########################################')
        print('###  RMSE =', rmse)
        print('###')
        print('###  Square error =', seArray)
        print('###')
        print('###  rSq Predicted =', rSqPred)
        print('###')
        print('###  Mean Error =', meanErrArray)
        print('#########################################')
        print('#########################################')
                





