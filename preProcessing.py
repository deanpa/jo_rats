#!/usr/bin/env python

########################################
########################################
# This file is part of Possum g0 and sigma analysis
# Copyright (C) 2016 Dean Anderson 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

########################################
########################################

import os
import pickle
import numpy as np
import datetime
from scipy import stats
#from numba import jit
#from numba import njit
#from numba.typed import List
from scipy.special import gamma
from scipy.special import gammaln
import pylab as P
from copy import deepcopy

def decodeBytes(byteArray, nArray):
    """
    ## loop to decode byte to string
    """
    strArray = str(byteArray[0], 'utf-8')
    for i in range(1, nArray):
        strArr_i = str(byteArray[i], 'utf-8')
        strArray = np.append(strArray, strArr_i)
    return(strArray)


### Define global functions: ###
def logit(x):
    return np.log(x) - np.log(1 - x)


def inv_logit(x):
    return np.exp(x) / (1 + np.exp(x))

## GAMMA PDF
def gamma_pdf(xx, shape, scale):
    gampdf = 1.0 / (scale**shape) / gamma(shape) * xx**(shape - 1) * np.exp(-(xx/scale))
    return gampdf



def distFX(x1, y1, x2, y2):
    """
    calc distance matrix between 2 sets of points
    resulting matrix dimensions = (len(x1), len(x2))
    """
    deltaX = np.add.outer(x1, -x2)
    deltaX_sq = deltaX**2
    deltaY = np.add.outer(y1, -y2)
    deltaY_sq = deltaY**2
    dist_2D = np.sqrt(deltaX_sq + deltaY_sq)
    return dist_2D


def formatDate(inArray):
    n = len(inArray)
    outArray = np.empty(n, dtype = datetime.date)
    mondayArray = np.empty(n, dtype = datetime.date)
    for i in range(n):
        date_i = inArray[i]
        if date_i != '':
            di = datetime.datetime.strptime(date_i, '%d/%m/%Y').date()
            outArray[i] = di
            mondayArray[i] = (di - datetime.timedelta(days=di.weekday()))

###            outArray[i] = datetime.datetime.strptime(date_i, '%d/%m/%y').date()
        else:
            outArray[i] = datetime.date(1999, 1, 1).date()
#        print('date', di, 'monday', mondayArray[i])
    return(outArray, mondayArray)



def getWeek(inArray):
    n = len(inArray)
    outArray = np.empty(n, dtype = int)
    for i in range(n):
        date_i = inArray[i]
        outArray[i] = date_i.isocalendar()[1]
#        print('date', date_i, 'wk', outArray[i])    
    return(outArray)


def dwrpcauchy(th, mu, rho):
    """
    wrapped cauchy pdf: direction is mu, and focus is rho.
    mu is real, and rho > 0;  Wikipedia pdf equation
    """
    sinh_rho = np.sinh(rho)
    cosh_rho = np.cosh(rho)
    cos_mu_th = np.cos(th - mu)
    dwrpc = sinh_rho / 2 / np.pi / (cosh_rho - cos_mu_th)
    return dwrpc

def genGammaPDF(x, a, d, p):
    ## FROM WIKIPEDIA
    num = p / a**d
    dem = gamma(d/p)
    num2 = (x**(d-1)) * (np.exp(-(x / a)**p))
    ggPDF = num * num2 / dem
    return(ggPDF)


class BasicData(object):
    def __init__(self, params):
        """
        OBJECT TO READ DATA, IMPORT PARAMS, AND SET INITIAL VALUES
        """
        #############################################
        ############ RUN BASICDATA FUNCTIONS
        self.getParams(params)
        self.readDensity()


        self.readEnviron()
        self.readTrackingTunnel()
        self.readTraps()
        self.readCamera()
        self.getWeekIntervalDensity()
        self.getDenElevBySite()


        if self.params.crossValidation:
            self.getCVIndx()

        self.makeDenKrigeVariables()

        if self.params.crossValidation:
            self.cvModify()

        self.predictRicker()
        self.makeCovMatrix()
        self.makePredCovMat()


        ############## END RUNNING FUNCTIONS
        #############################################

    def getParams(self, params):
        """
        ## MOVE PARAMS INTO BASICDATA
        """
        self.params = params
        
        self.gamm = self.params.gamm
        self.nGamm = len(self.gamm)                  

        self.maxK = self.params.maxK
        self.changeWk = self.params.changeWk

        self.D0 = self.params.D0
        self.lnD0 = np.log(self.D0)
        self.missingDen = self.params.missingDen
        self.phiDen = self.params.phiDen
        self.vDen = self.params.vDen
        self.beta = self.params.beta
        self.sig0 = self.params.sig0
        self.missingSig = self.params.missingSig
#        self.phiSig = self.params.phiSig
        self.vSig = self.params.vSig
    
        ## RICKER MODEL PARAS
        
        self.r = self.params.r
#        self.wrpC = self.params.wrpC
#        self.immigDates = self.params.immigDates

####        self.bRatioThreshold = self.params.bRatioThreshold
        self.immWeek = self.params.immWeek



        ############################       

    def readDensity(self):
        """
        ## READ IN DENSITY DATA
        """
#        densityDat = np.genfromtxt(self.params.inputDensityFname,  delimiter=',', 
#            names=True, dtype=['S10', 'S10', 'f8', 'f8', 'f8'])

        densityDat = np.genfromtxt(self.params.inputDensityFname,  delimiter=',', 
            names=True, dtype=['S10', 'S10', 'S10', 'i8', 'S10', 'i8', 'S10', 'f8', 'f8', 
            'f8', 'f8', 'f8', 'f8', 'f8', 'f8','f8', 'f8', 'f8', 'f8'])


#        self.densitySeason = densityDat['Season']
#        self.densitySeason = decodeBytes(self.densitySeason, self.nDensity)
        self.denGrid = densityDat['Grid']
        self.nDensity = len(self.denGrid)
        self.denGrid = decodeBytes(self.denGrid, self.nDensity)
        self.uDenGrid = np.unique(self.denGrid)
        self.nUDenGrid = len(self.uDenGrid)
        self.denDate = densityDat['date']
        self.denDate = decodeBytes(self.denDate, self.nDensity)
#        self.denElevClass = densityDat['elevation']
#        self.denElevClass = decodeBytes(self.denElevClass, self.nDensity)

        ## FORMAT DENSITY DATES AND GET SIN, COS OF WEEK OF YEAR
        self.getDensityDates()
        self.rawD = densityDat['D'] * 100.0
        self.D = self.rawD.copy() 
        self.sigma = densityDat['sigma']
#        ## SESSION
#        self.denSession = densityDat['Session']
#        self.denSession = decodeBytes(self.denSession, self.nDensity)
        ## ELEVATION AND MONTHS SINCE MAST
        self.denElev = densityDat['Height']
#        self.monthsMast = densityDat['MonthsSinceMast']
        self.getLatentDensitySigma()
        self.denSites = np.array(['highA', 'highB', 'lowA', 'lowB', 'midA', 'midB'])
        self.nDenSites = len(self.denSites)


    def getLatentDensitySigma(self):
        ## GET LATENT DATA FOR T0, AND MISSING DENSITY AND SIGMA DATA


        ## CREATE MASK OF MISSING DENSITY
        maskMissD = np.isnan(self.D)
        ## INSERT LATENT DENSITIES
        self.D[maskMissD] = self.missingDen
        ## LOG OF D FOR UPDATING PARAMETERS (EQ. 21)
        self.lnD = np.log(self.D)


        ## ARRAYS FOR UPDATING MISSING SIGMA
        self.Den_s = self.D.copy()
        self.lnDen_s = self.lnD.copy() 
        ## CREATE MASK OF FIRST OBS BY SITE FOR LATENT D0
        date_0 = self.uDates[0]      #datetime.date(2019, 7, 15) # FIRST OBS AT EACH SITE
        self.maskFirstObs = self.denDate == date_0 

        ## CREATE MASK OF MISSING SIGMA
        self.maskMissSig = np.isnan(self.sigma)
        ## INSERT LATENT SIGMA
        self.sigma[self.maskMissSig] = self.missingSig
        self.sigma_s = self.sigma.copy()
        ## LOG OF SIGMA FOR UPDATING PARAMETERS 
        self.lnSigma = np.log(self.sigma)
        self.lnSigma_s = self.lnSigma.copy()
#        ## GET LATENT DENSITY AT T-1
#        self.getDensitySigmaT_1()
            
    def getDensityDates(self):
        ## GET WEEKS OF YEAR FOR DENSITY AT SECR
        (self.denDate, self.denMonday) = formatDate(self.denDate)
        self.denWeek = getWeek(self.denDate)
#        self.denWeekRadians = 2.0 * np.pi * self.denWeek / 52.0


#        self.denCosine = np.cos(self.denWeekRadians)
#        self.denSine = np.sin(self.denWeekRadians)
        ## UNIQUE DENSITY DATES
        self.uDates = np.unique(self.denMonday)
#        self.nDenSessions = len(self.denDate)
        self.nDenSessions = len(self.uDates)
        print('udates', self.uDates)

#        for i in range(self.nDensity):
#            print('wk', self.denWeek[i], 'rad', self.denWeekRadians[i], 
#                'cos', self.denCosine[i])



    def getDensitySigmaT_1(self):
        """
        ## GET DENSITY AND SIGMA AT PREVIOUS TIME STEP
        """
        ## LATENT DENSITY 
        self.D_T_1 = np.zeros(self.nDensity)
        self.D_T_1[1:] = self.D[:-1]
        ## POPULATE THE LATENT D0 OBS FOR FIRST DATE
        self.D_T_1[self.maskFirstObs] = self.D0
        ## LOG D_T-1 FOR UPDATING
        self.lnD_T_1 = np.log(self.D_T_1)

#        ## GET DENSITY T-1 MISSING MASK: MASK WHERE TO POPUL. MISSING T-1 DATA
#        self.getMissDenT_1_Indx()


        ## LATENT SIGMA
        self.sig_T_1 = np.zeros(self.nDensity)
        self.sig_T_1[1:] = self.sigma[:-1]
        ## POPULATE THE LATENT SIGMA 0 OBS FOR FIRST DATE
        self.sig_T_1[self.maskFirstObs] = self.sig0
        ## LOG SIG_T_1 FOR UPDATING PARAMETERS
        self.lnSig_T_1 = np.log(self.sig_T_1)
#        for i in range(self.nDensity):
#            print('sig', self.sigma[i], 'Dt-1', self.sig_T_1[i], 'mask', self.maskFirstObs[i],
#                'date', self.denDate[i], 'SESS', self.denSession[i])



    def getMissDenT_1_Indx(self):
        """
        ## ID THE MISSING DEN POINTS THAT NEED TO BE PUT IN THE T-1 DATA, AND
        ## WHERE TO PUT THEM.
        """
        self.indxMissDenT_1 = np.empty(0, dtype = int)
        self.subIndxMissDensity = np.empty(0, dtype = int)
        arrIndx = np.arange(self.nDensity)
        lastDate = self.uDates[-1]
        for i in range(self.nDensity):
            d_i = self.rawD[i]
            if np.isnan(d_i):
                date_i = self.denMonday[i]
#                date_i = self.denDate[i]
                if date_i == lastDate:
                    continue
                else:
                    ## TAKE INDX FROM HERE AND PUT MISS DEN IN LOCATION OF 
                    ## self.indxMissDenT_1
                    self.subIndxMissDensity = np.append(self.subIndxMissDensity, i)
                    ## THE FOLLOWING FINDS WHERE TO PUT THEM IN FULL DEN ARRAY
                    t_up1_dateIndx = list(self.uDates).index(date_i) + 1
                    dateT_up1 = self.uDates[t_up1_dateIndx]
                    bigDateMask = self.denMonday == dateT_up1
#                    bigDateMask = self.denDate == dateT_up1
                    grid_i = self.denGrid[i]
                    bigGridMask = self.denGrid == grid_i
                    gridDateMask = bigDateMask & bigGridMask
                    bigIndx = arrIndx[gridDateMask]
                    ## INDX WHERE TO PUT THE MISSING T-1 DENSITY IN FULL D ARRAY
                    self.indxMissDenT_1 = np.append(self.indxMissDenT_1, bigIndx)
        


    def readEnviron(self):
        ## READ IN ENVIRONMENTAL DATA 
        environDat = np.genfromtxt(self.params.inputEnvironFname,  delimiter=',', 
            names=True, dtype=['S10', 'f8', 'f8', 'f8', 'S10'])
        self.environDeviceID = environDat['DeviceID']
        self.nEnviron = len(self.environDeviceID)
        self.environDeviceID = decodeBytes(self.environDeviceID, self.nEnviron)
        self.envEast = environDat['Easting']
        self.envNorth = environDat['Northing']
        self.envElev = environDat['Elev_vect']
        self.envType = environDat['detectType']
        self.envType = decodeBytes(self.envType, self.nEnviron)


    def readTrackingTunnel(self):
        ## READ IN TRACKING TUNNEL DATA
        TTDat = np.genfromtxt(self.params.inputTTFname,  delimiter=',', 
            names=True, dtype=['S10', 'S10', 'S10', 'S10', 'S10', 'S10', 'S10', 
            'f8', 'i8', 'i8', 'i8', 'i8', 'i8', 'i8', 'S10'])
        self.TTID = TTDat['TunnelID']
        self.nTT = len(self.TTID)
        self.TTID = decodeBytes(self.TTID, self.nTT)

    def readTraps(self):
        ## READ IN TRAP DATA
        trapDat = np.genfromtxt(self.params.inputSnapFname,  delimiter=',', 
            names=True, dtype=['S10', 'S10', 'S10', 'S10', 'S10', 'i8', 'S10', 'S10', 
            'f8', 'i8', 'i8', 'i8', 'S10'])
        self.trapID = trapDat['TrapID']
        self.nTrapDat = len(self.trapID)
        self.trapID = decodeBytes(self.trapID, self.nTrapDat)

    def readCamera(self):
        ## READ IN CAMERA DATA
        cameraDat = np.genfromtxt(self.params.inputCameraFname,  delimiter=',', 
            names=True, dtype=['i8', 'S10', 'S10', 'S10', 'S10', 'i8', 'S10', 
            'f8', 'i8', 'i8'])
        self.cameraID = cameraDat['FullcamID']
        self.nCameraDat = len(self.cameraID)
        self.cameraID = decodeBytes(self.cameraID, self.nCameraDat)

    def getWeekIntervalDensity(self):
        self.diffWeeks = np.zeros(len(self.uDates), dtype = int)
        startMonday = (self.params.date0 - 
            datetime.timedelta(days=self.params.date0.weekday()))
        mastDate = datetime.date(2019,3,10)
        mastMonday = (mastDate - datetime.timedelta(days=mastDate.weekday()))
#        print('startMonday', startMonday)
        ## EMPTY LISTS TO POPULATE
#        self.weekSinCosList = []
###        self.weekWrpCList = []
        self.weekMastList = []
        self.dateList = []
        self.dateSessList = []
#        self.dateList.append(startMonday)
        for i in range(self.nDenSessions):

            date_i = self.uDates[i]
            year_i = date_i.year

            nWeeksYear = datetime.date(year_i, 12, 28).isocalendar()[1]
            endMonday = (date_i - datetime.timedelta(days=date_i.weekday()))

#            print('start', startMonday, 'end', endMonday, 'udate', self.uDates[i])

            self.diffWeeks[i] = (endMonday - startMonday).days // 7
            wkMastArray = np.zeros(self.diffWeeks[i])
            datesesslist = np.zeros(self.diffWeeks[i], dtype = datetime.date)
#            print('year_i', year_i, 'nWeeks', nWeeksYear)
            for j in range(self.diffWeeks[i]):
                mon_j = startMonday + datetime.timedelta(days=7)
                wkYear_j = mon_j.isocalendar()[1]
                year_j = mon_j.year
                j_weekInYear = datetime.date(year_j, 12, 28).isocalendar()[1]
                ## MAST WEEK
                wkMast_j = (mon_j - mastDate).days // 7
                wkMastArray[j] = wkMast_j

                startMonday = mon_j
#                if (i == (self.nDenSessions - 1)) & (j == (self.diffWeeks[i] - 1)):
#                    continue
                self.dateList.append(mon_j)

#                if (i == 0) & (j == 0):
#                    datesesslist[j] = self.params.date0
#                else:
                datesesslist[j] = mon_j

#                print('sess', i, 'mon', mon_j, 'wkyear', wkYear_j, 'wkMask', wkMast_j,
#                    'in year', j_weekInYear, 
#                    'year_j', year_j)

            self.weekMastList.append(wkMastArray)
            self.dateSessList.append(datesesslist)
#        print('Week differences', self.diffWeeks, 'wkMastList', self.weekMastList, 
#            'dateSessList', self.dateSessList, 'dateList', self.dateList)

#        P.figure()
#        sincos = np.concatenate(self.weekSinCosList)
#        nWk = len(sincos)
#        P.plot(np.arange(nWk), sincos)
#        P.show()



    def getDenElevBySite(self):
        """
        ## GET ELEVATION BY SITE FOR DENSITY ANALYSIS
        """
        self.denElevBySite = np.zeros(self.nDenSites)
        self.elevDummyVar = np.zeros(self.nDenSites)
        for i in range(self.nDenSites):
            site_i = self.denSites[i]
            mask = self.environDeviceID == site_i
            self.denElevBySite[i] = np.log(self.envElev[mask])
            if (self.envElev[mask] > 800.0):
                self.elevDummyVar[i] = 1.0
#            print('site', site_i, 'elev', self.denElevBySite[i], self.elevDummyVar[i])
###        nSpace = len(self.gamm[:-1])        
###        self.spaceCovar = np.ones((self.nDenSites, nSpace))

###        self.spaceCovar[:,1] = self.elevDummyVar
#        self.spaceCovar[:,1] = self.denElevBySite


    def getCVIndx(self):
        """
        ## CROSS VALIDATION - GET INDICES
        """
        cc = 0
        for d in range(self.nDensity):
#           while cc <= self.params.cvJobID:
            if np.isnan(self.rawD[d]):
                continue
            print('d', d, 'cc', cc)
            if cc == self.params.cvJobID:
                site_cc = self.denGrid[d]
                print('d', d, 'cc', cc, 'site_cc', site_cc)
                date_cc = self.denMonday[d]        
                self.sessCV = np.asarray(np.where(date_cc == self.uDates)).item()
                siteStr = np.where(self.denSites == site_cc)
#                print('siteStr', siteStr, 'sessCV', self.sessCV)
                self.siteCV = np.array(np.where(site_cc == self.denSites)).item()
#                print('siteCV', self.siteCV)
                self.den1D_IndxCV = d
                ## GET MEAN DENSITY FOR SESSION CV
                dateMask = self.denMonday == date_cc
                rawDen2 = self.rawD.copy()
                rawDen2[d] = np.nan
                nanMask = ~np.isnan(rawDen2)
                keepMask = nanMask & dateMask
                meanTestDat = np.mean(self.rawD[keepMask])
                self.cvMeanDensity = meanTestDat
                self.cvObsDensity = self.D[d]
                break
            cc += 1            
        print('siteCV', self.siteCV, 'sessCV', self.sessCV, 'obDen', self.cvObsDensity,
            'meanTestDat', meanTestDat)


    def makeDenKrigeVariables(self):
        """
        ## MAKE 2-D ARRAY OF COVARIATES FOR DENSITY KRIGING
        ## INTERCEPT, ELEV, TIME MAST, TIME MAST SQ, SIN, COS, DENSITY T-1
        """

        print('missDen', np.round(self.missingDen, 3))

        lastDate = self.uDates[-1]
        xDatFull = np.ones((self.nDensity, len(self.params.xdatDictionary)), 
            dtype = float)
        ## POPULATE ARRAY
        xDatFull[:, 2] = np.log(self.denElev)
        xDatFull[:, 3] = 111        #np.log(self.monthsMast)
        xDatFull[:, 4] = 222        #(np.log(self.monthsMast))**2
        xDatFull[:, 4] = 333    #self.denSine
        xDatFull[:, 5] = 444    #self.denCosine
#        ## MAKE REDUCED COVARIATE MATRIX FOR DENSITY FROM INDEX IN PARAMS
#        self.gammX = xDatFull[:, self.params.xDenIndx] 
        ## MAKE REDUCED COVARIATE MATRIX FOR SIGMA FROM INDEX IN PARAMS
        self.betaX = xDatFull[:, self.params.xSigIndx]
        ## MAKE LIST OF X AND Y DATA AND PREDICTION M AND MU BY DATE
#        self.listGammX = []
        self.listBetaX = []
        self.listLnDensity = []
        self.listDensity = []
#        self.listLnDensityT_1 = []
#        self.listDensityT_1 = []
#        self.listLnSigma = []
#        self.listRawD = []
#        self.listM = []
#        self.listMu =[]
        ## LIST OF INDICES FOR MISSING DENSITY AND SIGMA; AND T-1 DATA PLACEMENT
        self.listSessSiteMissDen = []   # WHERE PUT MISS DATA IN LIST DATE/SITE
#        self.originT_1_Indx = []        # INDX IN MISS DATA ARRAY FOR T-1 DATA TO MOVE
#        self.listSessSiteMissT_1 = []   # WHERE PUT MISS DATA IN T-1 LIST DATE/SITE
#        denBySite = np.zeros(self.nDenSites)
        cc = 0
        indxArr = np.arange(len(self.D), dtype=int)
        self.missIndx = []
        ## LOOP SESSIONS
        for i in range(self.nDenSessions):
            ## EMPTY ARRAY TO POPULATE WITH x DATA
#            dummyGammX = np.zeros((self.nDenSites, np.shape(self.gammX)[1]))
#            dummyBetaX = np.zeros((self.nDenSites, np.shape(self.betaX)[1]))
            dummyLnDensity = np.zeros(self.nDenSites)
            dummyDensity = np.zeros(self.nDenSites)
#            dummyLnSigma = np.zeros(self.nDenSites)
#            dummyDensityT_1 = np.zeros(self.nDenSites)
#            dummyLnDensityT_1 = np.zeros(self.nDenSites)

            ## MAKE SURE IN ORDER OF SITES IS self.denSites
            date_i = self.uDates[i]
            ## LOOP THROUGH SITES
            sessMask = (self.denMonday == date_i)
#            sessMask = (self.denDate == date_i)
            for j in range(self.nDenSites):
                site_j = self.denSites[j]
                siteMask = self.denGrid == site_j
                sessSiteMask = sessMask & siteMask

                dummyLnDensity[j] = self.lnD[sessSiteMask]
                dummyDensity[j] = self.D[sessSiteMask]

                D_ij = self.rawD[sessSiteMask]
                if np.isnan(D_ij):
                    iii = np.int(indxArr[sessSiteMask])
                    self.missIndx.append(iii)

                    ## TAKE INDX FROM HERE AND PUT MISS DEN IN LOCATION [sess,site] 
                    date_iIndx = list(self.uDates).index(date_i)
                    site_jIndx = list(self.denSites).index(site_j)               
                    self.listSessSiteMissDen.append([date_iIndx, site_jIndx])
#                    print('sess', i, 'site', j, 'dateID', date_iIndx, 'siteID', site_jIndx,
#                        'list', self.listSessSiteMissDen, 'indxArr', iii) 
                    ## UPDATE WITH MISSING DENSITY DATA
                    dummyDensity[j] = self.missingDen[cc]
                    dummyLnDensity[j] = np.log(self.missingDen[cc])
                    self.D[indxArr[sessSiteMask]] = self.missingDen[cc]
                    self.lnD[indxArr[sessSiteMask]] = np.log(self.missingDen[cc])

#                    print('sess', date_iIndx, 'site', site_jIndx, 'dumDen',
#                        np.round(dummyDensity,3))

                    cc += 1



            ## EQUATION 21
#            m_i = np.dot(dummyGammX, self.gamm)
#            self.listM.append(m_i)
#            self.listGammX.append(dummyGammX)            
            self.listLnDensity.append(dummyLnDensity)   
            self.listDensity.append(dummyDensity)   
#            self.listLnDensityT_1.append(dummyLnDensity)   
#            self.listDensityT_1.append(dummyDensity)   
            ## EQUATION 10 FOR PREDICTING SIGMA ON LOG SCALE
#            Mu_i = np.dot(dummyBetaX, self.beta) / np.sqrt(dummyDensity)                
#            self.listMu.append(Mu_i)
#            self.listBetaX.append(dummyBetaX)
#            self.listLnSigma.append(dummyLnSigma)         
            
        ## GET COLUMN NUMBER FOR DENSITY T-1
        allCovariateList = np.array(list(self.params.xdatDictionary.keys()))
        namesGamma = allCovariateList[self.params.xDenIndx]
#        self.denT_1Column = np.squeeze(np.where(namesGamma == 'DensityT_1'))
#        self.nMissT_1 = len(self.originT_1_Indx)
        ## MAKE COPIES FOR UPDATING MISSING VALUES
        self.listLnDen_s = deepcopy(self.listLnDensity)
        self.listDen_s = deepcopy(self.listDensity)

#        self.listLnDen_s[0][0] = 12.001

        print('den', np.round(self.listDensity,3)) 
#            'den_s', np.round(self.listLnDen_s,3))

#        print('self.missIndx', self.missIndx)
        print('self.listSessSiteMissDen', self.listSessSiteMissDen)


    def cvModify(self):
        ## STARTING VALUE FOR MISSING CV DENSITY TO UPDATE IN MCMC
        self.cvMissDen = np.random.normal(np.log(self.cvObsDensity), 0.1)
        ## SET PRIORS FOR MISSING CV VALUE
        self.cvMissPriorLow = self.cvMissDen - 250.0
        if self.cvMissPriorLow < self.params.missDenPrior[0]:
            self.cvMissPriorLow = self.params.missDenPrior[0] 
        self.cvMissPriorHigh = self.cvMissDen + 250.0
        if self.cvMissPriorHigh > 2400:
            self.cvMissPriorHigh = 2400.
        self.listDensity[self.sessCV][self.siteCV] = self.cvMissDen         
        self.listLnDensity[self.sessCV][self.siteCV] = np.log(self.cvMissDen)




    def predictRicker(self):
        """
        ## PREDICT M FOR DENSITY: LIST LEN N SESSIONS, EACH ARR WITH 6 SITES
        """ 
        ## SPACE PREDICTION FOR SIX SITES
###        self.spacePred = np.dot(self.spaceCovar, self.gamm[:-1])
        self.listM = []
        self.bRatioList = []
        self.bRatioMidTestList = []
        self.maskNegExpWk_List = []
        self.negExpWk_List = []
        self.negExpPred_HighList = []
        self.negExpPred_LowList = []
        self.K_List = []
        ## EMPTY PROPOSAL ARRAYS
        self.K_List_s = []
        self.bRatioList_s = []
        self.bRatioMidTestList_s = []
        self.listM_s = []

#        ## SET UP INITIAL B RATIOS ARRAYS IN LIST
        self.K_High0 = self.maxK[0] 
        self.K_Low0 = self.maxK[1]
        ## SET UP EMPTY ARRAY LIST FOR BRATIO - LOOP SESSIONS (7)
        for i in range(self.nDenSessions):
            self.bRatioList.append(np.zeros((self.diffWeeks[i], 4)))
            self.bRatioMidTestList.append(np.zeros((self.diffWeeks[i], 2)))
            ## MASK WEEKS FOR EXPONENTIAL DECAY
            wkMask = self.weekMastList[i] >= self.changeWk
            self.maskNegExpWk_List.append(wkMask)       # MASK OF WEEKS FOR EXP DEC
            ## WEEK OF EXPONENTIAL DECAY
            wkNegExp_i = self.weekMastList[i][wkMask] - self.changeWk
            self.negExpWk_List.append(wkNegExp_i)       # WEEK MULTIPLIER FOR DECAY
            ## NEG EXP PRED AND K
            K_High_i = np.repeat(self.maxK[0], self.diffWeeks[i])
            negExpHigh_i = np.exp(-wkNegExp_i * self.gamm[0])
            self.negExpPred_HighList.append(negExpHigh_i)
            K_High_i[wkMask] = self.maxK[0] * negExpHigh_i
            ## LOW AND MID SITES
            K_Low_i = np.repeat(self.maxK[1], self.diffWeeks[i])
            negExpLow_i = np.exp(-wkNegExp_i * self.gamm[1])
            self.negExpPred_LowList.append(negExpLow_i)
            K_Low_i[wkMask] = self.maxK[1] * negExpLow_i
            ## POPULATE K 2-D ARRAY
            K_2D = np.zeros((self.diffWeeks[i], 2))
            K_2D[:, 0] = K_High_i
            K_2D[:, 1] = K_Low_i
            self.K_List.append(K_2D)

        ## LOOP THROUGH FIRST TWO SITES
        for j in range(2):
            ## MID SITE D AND B (CARRYING CAPACITY) AND bRatio
            D_mid = self.D0[j + 4]
            K_mid = self.K_Low0
            self.bRatioList[0][0, (j + 2)] = D_mid / K_mid
            ## HIGH SITE bRatio
#            print('test len', len(D), 'D', D, 'K_ij')
            D_High= self.D0[j]
            K_High = self.K_High0
            self.bRatioList[0][0, j] = D_High / K_High 

        D = np.zeros(self.nDenSites)
        ## LOOP DEN SESSIONS AGAIN AND POPULATE ARRAYS WITH RICKER PROCESS WITH MIGRATION
        week = 0
        for i in range(self.nDenSessions):
            ## TIME SINCE MAST EFFECT
#            K_High_i = np.repeat(self.maxK[0], self.diffWeeks[i])
#            wkMask = self.maskNegExpWk_List[i]
#            K_High_i[wkMask] = self.maxK[0] * self.negExpPred_HighList[i]
            K_High_i = self.K_List[i][:, 0]

#            K_Low_i = np.repeat(self.maxK[1], self.diffWeeks[i])
#            wkMask = self.maskNegExpWk_List[i]
#            K_Low_i[wkMask] = self.maxK[1] * self.negExpPred_LowList[i]
            K_Low_i = self.K_List[i][:, 1]
            mArray_i = np.zeros(self.nDenSites)
            ## LOOP THROUGH WEEKS IN SESSION i (SIMULATION)
            for k in range(self.diffWeeks[i]):
                ## LOOP THROUGH DEN SITES
                for j in range(self.nDenSites):

                    ####################################################
                    ####### new code
                    if i == 0:
                        if k == 0:                              ## SESS 0, WEEK 0
                            D[j] = self.D0[j]                   ## USE D0
                            if j < 2:
                                K_ij = self.K_High0
                            elif j >= 2:
                                K_ij = self.K_Low0
                        elif k > 0:                             ## SESS 0, WEEK > 0
                            if j < 2:
                                K_ij = K_High_i[k - 1]
                            elif j >= 2:
                                K_ij = K_Low_i[k - 1]
                    elif i > 0:                                 
                        if k == 0:                              ## SESS > 0, WEEK 0
                            D[j] = self.listDensity[(i - 1)][j] 
                            if j < 2:
                                K_ij = self.K_List[i - 1][-1, 0]
                            elif j >= 2:
                                K_ij = self.K_List[i - 1][-1, 1]
                        elif k > 0:                             ## SESS > 0, WEEK > 0
                            if j < 2:
                                K_ij = K_High_i[k-1]
                            elif j >= 2:
                                K_ij = K_Low_i[k-1]

###                    if (k == 0) & (i == 0):
###                        D[j] = self.D0[j]

                    
#                        if j < 2:
#                            K_ij = K_High_i[k]
#                        else:
#                            K_ij = K_Low_i[k]


###                    elif (k == 0) & (i > 0):
###                        D[j] = self.listDensity[(i - 1)][j]
                    ## DENSITY DEPENDENCE FOR EACH SUB-WEEK (RICKER)
###                    if j < 2:
###                        K_ij = K_High_i[k]
###                    else:
###                        K_ij = K_Low_i[k]
                    ## UPDATE 'a' IF HAVE MIGRATION CONDITIONS
###                    a = self.r[0]
                    if (j < 2) & self.params.immModel:
#                    if (j < 2) & (np.in1d(self.params.modelID, self.params.immModels)):
                        if np.in1d(week, range(self.immWeek[0], self.immWeek[1])):
####                        if (self.bRatioMidTestList[i][k, j] >= self.params.bRatioThreshold):
                            ## RICKER UPDATE OF DENSITY WITH IMMIGRATION
                            if self.bRatioList[i][k, j] < self.params.minBRatio:
                                self.bRatioList[i][k, j] = self.params.minBRatio
                            a = (self.r[1] * (self.bRatioList[i][k, (j + 2)] / 
                                self.bRatioList[i][k, j]))
                            D[j] = (D[j] * np.exp(self.r[0] * (1.0  - (D[j] / K_ij))) +
                                (D[j + 4] * a))
                        else:
                            D[j] = D[j] * np.exp(self.r[0] * (1.0  - (D[j] / K_ij)))
                    else:
                        ## RICKER WITH NO IMMIGRATION
                        D[j] = D[j] * np.exp(self.r[0] * (1.0  - (D[j] / K_ij)))
                    ## IF NOT LAST WEEK IN SESSION, UPDATE B RATIO VALUES
                    if k < (self.diffWeeks[i] - 1): 
                        if j < 2:
                            self.bRatioList[i][(k+1), j] = D[j] / K_High_i[k]
                        elif j > 3:
                            self.bRatioList[i][(k+1), (j - 2)] = D[j] / K_Low_i[k]
                    ##  IF LAST WEEK OF SESSION; POPULATE mArray_i
                    if k == (self.diffWeeks[i] - 1):
                        mArray_i[j] = np.log(D[j])
                        if i < (self.nDenSessions - 1):
                            if j < 2:
                                self.bRatioList[(i + 1)][0, j] = D[j] / K_Low_i[k] 
                            elif j > 3:
                                self.bRatioList[(i + 1)][0, (j - 2)] = D[j] / K_High_i[k] 
                week += 1    
#                        print('i', i, 'j', j, 
#                            'D', np.round(self.listDensity[i][j], 2),
#                            'predM', np.round(D[j], 2))

#            print('POST bratio 5', np.round(self.bRatioList[i][:5], 2))
            self.listM.append(mArray_i)
            ## POPULATE PROPOSAL ARRAYS
            self.K_List_s.append(np.zeros((self.diffWeeks[i], 2)))
            self.bRatioList_s.append(np.zeros((self.diffWeeks[i], 4)))
            self.bRatioMidTestList_s.append(np.zeros((self.diffWeeks[i], 2)))
            self.listM_s.append(np.zeros(self.nDenSites))

        self.negExpPred_HighList_s = deepcopy(self.negExpPred_HighList)
        self.negExpPred_LowList_s = deepcopy(self.negExpPred_LowList)
        self.maskNegExpWk_List_s = deepcopy(self.maskNegExpWk_List)
        self.negExpWk_List_s = deepcopy(self.negExpWk_List)

        ## PREDICT MU FOR SIGMA
        self.mu = (np.exp(np.dot(self.betaX, self.beta)) /
                 np.sqrt(self.D / 100.0))
        self.lnMu = np.log(self.mu)   
        self.mu_s = self.mu.copy()


#        print('D list', np.round(self.listDensity, 2), 
#                'M list', np.round(np.exp(self.listM), 2))



    def makeCovMatrix(self):
        ## GET EAST AND NORTH FOR DENSITY DATA
        uGrid = np.unique(self.denGrid)
        self.denEast = np.zeros(self.nDenSites)
        self.denNorth = np.zeros(self.nDenSites)
        for i in range(self.nDenSites):
            grid_i = self.denSites[i]
            mask = self.denGrid == grid_i
            east_i = self.envEast[self.environDeviceID == grid_i]
            north_i = self.envNorth[self.environDeviceID == grid_i]
            self.denEast[i] = east_i
            self.denNorth[i] = north_i
        ## MAKE DISTANCE CORR MATRIX IN KM
        self.denDistMat = distFX(self.denEast,self.denNorth, 
            self.denEast,self.denNorth) / 1000.0
        self.gammCorMat = np.exp(-self.phiDen * self.denDistMat)
        ## EQUATION 22
        self.gammCovMat = self.vDen * self.gammCorMat
#        ## MAKE COR AND COV MATRISES FOR SIGMA (Beta)
#        self.betaCorMat = np.exp(-self.phiSig * self.denDistMat)
#        ## EQUATION 11
#        self.betaCovMat = self.vSig * self.betaCorMat 


    def makePredCovMat(self):
        """
        ## MAKE COVARIANCE MAT FOR C_gi, C_ii
        """
#        keepMask = np.in1d(self.environDeviceID, self.denSites)
        ## CAMERA COV 
        camMask = self.envType == 'CAM'
        xCam = self.envEast[camMask]        
        yCam = self.envNorth[camMask]        
        self.distCam_gi = distFX(self.denEast,self.denNorth, 
            xCam, yCam) / 1000.0 
        self.camCorMat = np.exp(-self.phiDen * self.distCam_gi)
        self.camCovMat = self.vDen * self.camCorMat
        ## ST COV 
        STMask = self.envType == 'ST'
        xST = self.envEast[STMask]        
        yST = self.envNorth[STMask]        
        self.distST_gi = distFX(self.denEast,self.denNorth, 
            xST, yST) / 1000.0 
        self.STCorMat = np.exp(-self.phiDen * self.distST_gi)
        self.STCovMat = self.vDen * self.STCorMat
        ## TT COV 
        TTMask = self.envType == 'TT'
        xTT = self.envEast[TTMask]        
        yTT = self.envNorth[TTMask]        
        self.distTT_gi = distFX(self.denEast,self.denNorth, 
            xTT, yTT) / 1000.0 
        self.TTCorMat = np.exp(-self.phiDen * self.distTT_gi)
        self.TTCovMat = self.vDen * self.TTCorMat
#        print('shp cov TT', np.shape(self.TTCovMat), 'CAM', np.shape(self.camCovMat),
#            'ST', np.shape(self.STCovMat)) 



########            Pickle results to directory
########

class CheckingData(object):
    def __init__(self, mcmcobj):
        self.gammGibbs = mcmcobj.gammGibbs
        self.maxKGibbs = mcmcobj.maxKGibbs
        self.changeWkGibbs = mcmcobj.changeWkGibbs
        self.betaGibbs = mcmcobj.betaGibbs
        self.vDenGibbs = mcmcobj.vDenGibbs
        self.vSigGibbs = mcmcobj.vSigGibbs
        self.phiDenGibbs = mcmcobj.phiDenGibbs
#        self.phiSigGibbs = mcmcobj.phiSigGibbs
        self.missDenGibbs = mcmcobj.missDenGibbs
        self.missSigGibbs = mcmcobj.missSigGibbs
        self.D0Gibbs = mcmcobj.D0Gibbs
        self.rGibbs = mcmcobj.rGibbs
        self.DICGibbs = mcmcobj.DICGibbs
####        self.bRatioThresGibbs = mcmcobj.bRatioThresGibbs
        self.meanMDen = mcmcobj.meanMDen
        self.meanMuSigma = mcmcobj.meanMuSigma
        self.startingStep = mcmcobj.startingStep
        self.cc = mcmcobj.cc



class GibbsResults(object):
    def __init__(self, modelparams, basicdata, mcmcobj):
        self.gammGibbs = mcmcobj.gammGibbs
        self.maxKGibbs = mcmcobj.maxKGibbs
        self.changeWkGibbs = mcmcobj.changeWkGibbs
        self.betaGibbs = mcmcobj.betaGibbs
        self.vDenGibbs = mcmcobj.vDenGibbs
        self.vSigGibbs = mcmcobj.vSigGibbs
        self.phiDenGibbs = mcmcobj.phiDenGibbs
#        self.phiSigGibbs = mcmcobj.phiSigGibbs
        self.missDenGibbs = mcmcobj.missDenGibbs
        self.missSigGibbs = mcmcobj.missSigGibbs
        self.D0Gibbs = mcmcobj.D0Gibbs
        self.rGibbs = mcmcobj.rGibbs
        self.DICGibbs = mcmcobj.DICGibbs
####        self.bRatioThresGibbs = mcmcobj.bRatioThresGibbs
        self.immWeekGibbs = mcmcobj.immWeekGibbs
        self.meanMDen = mcmcobj.meanMDen
        self.meanMuSigma = mcmcobj.meanMuSigma
        ## basicdata elements
        self.denDistMat = basicdata.denDistMat
        self.listSessSiteMissDen = basicdata.listSessSiteMissDen
        self.uDates = basicdata.uDates
        self.denSites = basicdata.denSites
        self.nDenSites = basicdata.nDenSites
        self.nUDenGrid = basicdata.nUDenGrid
        self.denGrid = basicdata.denGrid
#        self.weekSinCosList = basicdata.weekSinCosList
#        self.listTMPDF = basicdata.listTMPDF
        self.diffWeeks = basicdata.diffWeeks   
        self.nDenSessions = basicdata.nDenSessions
        self.nDensity = basicdata.nDensity
        self.denMonday = basicdata.denMonday
        self.denDate = basicdata.denDate
        self.dateList = basicdata.dateList
        self.date0 = modelparams.date0
        self.weekMastList = basicdata.weekMastList
#        self.weekWrpCList = basicdata.weekWrpCList
        self.mu = basicdata.mu
        self.listM = basicdata.listM
        self.listLnDensity = basicdata.listLnDensity
        self.listDensity = basicdata.listDensity
        self.D = basicdata.D
        self.sigma = basicdata.sigma
#        self.maskMissD = basicdata.maskMissD
#        self.spaceCovar = basicdata.spaceCovar
        self.missIndx = basicdata.missIndx
        self.maskMissSig = basicdata.maskMissSig


        self.bRatioList = basicdata.bRatioList
        self.bRatioMidTestList = basicdata.bRatioMidTestList
        self.K_List = basicdata.K_List

        self.maskNegExpWk_List = basicdata.maskNegExpWk_List
        self.negExpWk_List = basicdata.negExpWk_List
        self.negExpPred_HighList = basicdata.negExpPred_HighList
        self.negExpPred_LowList = basicdata.negExpPred_LowList
#        self.negExpPred_HighList_s = basicdata.negExpPred_HighList_s
#        self.negExpPred_LowList_s = basicdata.negExpPred_LowList_s
        self.K_High0 = basicdata.K_High0
        self.K_Low0 = basicdata.K_Low0
        self.maxK = basicdata.maxK
        self.changeWk = basicdata.changeWk
        ## EMPTY PROPOSAL ARRAYS
        self.K_List_s = basicdata.K_List_s
        self.bRatioList_s = basicdata.bRatioList_s
        self.bRatioMidTestList_s = basicdata.bRatioMidTestList_s
        self.listM_s = basicdata.listM_s
        self.listDen_s = basicdata.listDen_s




###### UN-USED FUNCTIONS


    def predictM(self):
        """
        ## PREDICT M FOR DENSITY: LIST LEN N SESSIONS, EACH ARR WITH 6 SITES
        """ 
        ## SPACE PREDICTION FOR SIX SITES
        self.spacePred = np.dot(self.spaceCovar, self.gamm[:-1])
        self.listM = []
        self.listTMPDF = []
        self.listPredTM = []
        self.bRatioList = []
        self.bRatioMidTestList = []
#        self.listSeasonEff = []
#        self.listWrpCPDF = []
        ## LOOP SESSIONS (7)
        for i in range(self.nDenSessions):
            ## TIME SINCE MAST EFFECT
            tmGamPDF_i = genGammaPDF(self.weekMastList[i], self.TM[0], 
                self.TM[1], self.TM[2])
#            tmGamPDF_i = gamma_pdf(self.weekMastList[i], self.tmShape, self.TM[1])
            tmPred_i = self.gamm[2] * tmGamPDF_i

##          MIGRATION EFFECT
            self.bRatioList.append(np.zeros((self.diffWeeks[i], 4)))
            self.bRatioMidTestList.append(np.zeros((self.diffWeeks[i], 2)))
            if i == 0:
                tmPred_t_1  = (genGammaPDF(0.5, self.TM[0], 
                    self.TM[1], self.TM[2]) * self.gamm[2])

#            ## SEASONAL WRAPPED CAUCHY EFFECT
#            wrpCPDF_i = dwrpcauchy(self.weekWrpCList[i], self.wrpC[0], self.wrpC[1])
#            seasonalEff_i = wrpCPDF_i * self.gamm[-1]
#            ## SEASONAL COSINE - SINE EFFECT
#            seasonalEff_i = np.dot(self.weekSinCosList[i], self.gamm[-2:])
#            ## TOTAL TEMPORAL EFFECT: TIME-MAST AND SEASON
#            totalTemporal_i = tmPred_i + seasonalEff_i
            mArray_i = np.zeros(self.nDenSites)
            ## LOOP DENSITY SITES (6)
            print('self.nDenSites', self.nDenSites)
            for j in range(self.nDenSites):
                ## DENSITY DEPENDENCE FOR EACH SUB-WEEK (RICKER)
                b_ij = np.exp(self.spacePred[j] + tmPred_i)

                if i == 0:                                  ## FIRST SESSION
                    D = self.D0[j]

                                        
                    ## FIRST TIME POINT     ###############################
                    ## GET MID SITE DATA FOR IMMIGRATION TO HIGH
                    if j < 2:
                        ## MID SITE D AND B (CARRYING CAPACITY) AND bRatio
                        D_mid = self.D0[j + 4]
                        b_mid = np.exp(self.spacePred[j + 4] + tmPred_t_1)
                        self.bRatioList[i][0, (j + 2)] = D_mid / b_mid * np.log(D_mid)
                        self.bRatioMidTestList[i][0, j] = D_mid / b_mid
                        ## HIGH SITE bRatio
#                        print('test len', len(D), 'D', D, 'b_ij')
                        b_i00 = np.exp(self.spacePred[j] + tmPred_t_1)
                        self.bRatioList[i][0, j] = D / b_i00 

                else:                                       ## SUBSEQUENT SESSIONS
                    D = self.listDensity[(i - 1)][j]
#                    ## GET MID SITE DATA FOR IMMIGRATION TO HIGH
#                    if j < 2:
#                        D_mid = self.listDensity[(i - 1)][midIndx_j]


                ## LOOP THROUGH WEEKS IN SESSION i (SIMULATION)
                print('diffweek', self.diffWeeks[i])
                for k in range(self.diffWeeks[i]):
                    ## GET MID SITE DATA FOR IMMIGRATION TO HIGH

                    print('i', i, 'j', j, 'k', k)
                    a = self.r[0]
                    ## UPDATE 'a' IF HAVE MIGRATION CONDITIONS
                    if (j < 2):
                        if (self.bRatioMidTestList[i][k, j] >= self.params.bRatioThreshold):
                            a = ((self.r[1] * (self.bRatioList[i][k, (j + 2)] / 
                                self.bRatioList[i][k, j])) + self.r[0])
                    D = D * np.exp(a * (1.0  - (D / b_ij[k])))

                    if k < (self.diffWeeks[i] - 1): 
                        if j < 2:
                            self.bRatioList[i][(k+1), j] = D / b_ij[k]
                        elif j > 3:
                            self.bRatioList[i][(k+1), (j - 2)] = D / b_ij[k] * np.log(D)
                            self.bRatioMidTestList[i][(k+1), (j-4)] = D / b_ij[k]



                mArray_i[j] = np.log(D)

#                print('lnDen', self.listLnDensity[i][j], 'mArray_i', logD)
            self.listM.append(mArray_i)
            self.listTMPDF.append(tmGamPDF_i)
            self.listPredTM.append(tmPred_i)
#            self.listWrpCPDF.append(wrpCPDF_i)
#            self.listSeasonEff.append(seasonalEff_i)
#            print('mArray_i', mArray_i, 'listM', self.listM)
        ## PREDICT MU FOR SIGMA
        self.mu = (np.exp(np.dot(self.betaX, self.beta)) /
                 np.sqrt(self.D))
        self.lnMu = np.log(self.mu)   
        self.mu_s = self.mu.copy()




    def predictMXXX(self):
        """
        ## PREDICT M FOR DENSITY: LIST LEN N SESSIONS, EACH ARR WITH 6 SITES
        """ 
        ## SPACE PREDICTION FOR SIX SITES
        self.spacePred = np.dot(self.spaceCovar, self.gamm[:2])
        self.listM = []
        self.listTMPDF = []
        self.listPredTM = []
#        self.listSeasonEff = []
#        self.listWrpCPDF = []
        ## LOOP SESSIONS (7)
        for i in range(self.nDenSessions):
            ## TIME SINCE MAST EFFECT
            tmGamPDF_i = genGammaPDF(self.weekMastList[i], self.TM[0], 
                self.TM[1], self.TM[2])
#            tmGamPDF_i = gamma_pdf(self.weekMastList[i], self.tmShape, self.TM[1])
            tmPred_i = self.gamm[2] * tmGamPDF_i
#            ## SEASONAL WRAPPED CAUCHY EFFECT
#            wrpCPDF_i = dwrpcauchy(self.weekWrpCList[i], self.wrpC[0], self.wrpC[1])
#            seasonalEff_i = wrpCPDF_i * self.gamm[-1]
#            ## SEASONAL COSINE - SINE EFFECT
#            seasonalEff_i = np.dot(self.weekSinCosList[i], self.gamm[-2:])
#            ## TOTAL TEMPORAL EFFECT: TIME-MAST AND SEASON
#            totalTemporal_i = tmPred_i + seasonalEff_i
            mArray_i = np.zeros(self.nDenSites)
            ## LOOP DENSITY SITES (6)
            for j in range(self.nDenSites):
                ## DENSITY DEPENDENCE FOR EACH SUB-WEEK
                b_ij = self.spacePred[j] + tmPred_i
#                b_ij = self.spacePred[j] + totalTemporal_i
                if i == 0:                                  ## FIRST SESSION
                    logD = np.log(self.D0[j])
                else:                                       ## SUBSEQUENT SESSIONS
                    logD = self.listLnDensity[(i - 1)][j]
                ## LOOP THROUGH WEEKS IN SESSION i
                for k in range(self.diffWeeks[i]):
                    logD = logD * (1.0 + b_ij[k]) + self.r 
                mArray_i[j] = logD
#                print('lnDen', self.listLnDensity[i][j], 'mArray_i', logD)
            self.listM.append(mArray_i)
            self.listTMPDF.append(tmGamPDF_i)
            self.listPredTM.append(tmPred_i)
#            self.listWrpCPDF.append(wrpCPDF_i)
#            self.listSeasonEff.append(seasonalEff_i)
#            print('mArray_i', mArray_i, 'listM', self.listM)
        ## PREDICT MU FOR SIGMA
        self.mu = (np.exp(np.dot(self.betaX, self.beta)) /
                 np.sqrt(self.D))
        self.lnMu = np.log(self.mu)   
        self.mu_s = self.mu.copy()



